figure
mydata=[17,9.8,16];

hold on
for i = 1:length(mydata)
    h=bar(i,mydata(i),0.6);
    if mydata(i) < 11
        set(h,'FaceColor','g');
    elseif mydata(i) > 16.5
        set(h,'FaceColor','b');
    else
        set(h,'FaceColor','r');
    end
end

se=[6,0.4,3];

er = errorbar(X,meristemlength,se,'k','LineWidth',1,'LineStyle','None'); 

 set(gca,'XTick',1:3);
 set(gca,'XTickLabel',{'Control','Drought','Cold'},'FontSize',8);
 xlim([0.5 3.5])
    
title('DZ length')%,'Interpreter','latex','FontSize',10)

ylabel('DZ length (mm)','Interpreter','latex','FontSize',10)
box off