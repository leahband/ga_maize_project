% Considering the data from the GA20oxOE line, 
% fits b-spline functions to the data of the five 'input functions' GA53,
% ZmGA20ox, AtGA20ox, GA3ox and GA2ox.

% Produces a .mat file containing a vector of the estimated b-spline
% co-efficients. Also plots figures showing the fits and
% the data. 

% Uses the fdaM package for creating and manipulating the b-spline
% functions.


clear
addpath('../semethod')

load ../data/GA20oxOE/data_GA20oxOE_GAconc_nM_GA20oxAdata.mat

ntime=length(data_GA20ox.pos_GAs);
ntime_enz=length(data_GA20ox.pos_enz);

addpath ('../fdaM')
initialt=1000;
tvec=[initialt;data.pos_GAs];
rng   = [initialt,max(tvec)];
norder=3;
nbasis=7;


basis = create_bspline_basis(rng, nbasis,norder);
params   = getbasispar(basis);
breaks   = [rng(1), params, rng(2)];
nparams=nbasis;

figure
plot(basis)

basisvals_meas_tp=eval_basis(data_GA20ox.pos_GAs,basis);
basisvals_meas_tp_enz=eval_basis(data_GA20ox.pos_enz,basis);


% For plotting solutions:
x_results=[initialt:100:max(tvec)];
basisvals_plot=eval_basis(x_results,basis);

nruns=10;
nparams=nbasis;
lb=-6*ones(nparams,1);
ub=4*ones(nparams,1);

theta0_01=lhsdesign(nruns,nparams);
 theta0=zeros(nruns,nparams);
 for j=1:nruns
     theta0(j,:)=theta0_01(j,:).*(ub-lb)'+lb';
 end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
% Fitting forcing function for GA20ox Maize for data_GA20ox.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
theta=zeros(nruns,nparams);
fval=zeros(1,nruns);

objfun=@(theta)objfun_fitinputscoefficients(theta,data_GA20ox.GA20ox_Maize,basisvals_meas_tp_enz);
options = optimset('Display', 'off');
for i=1:nruns
    [theta(i,:),fval(i)] = lsqnonlin(objfun,theta0(i,:),lb,ub,options);
end;
[f_best,I]=min(fval);
theta_GA20ox_Maize=theta(I,:);

ndata_GA20ox=length(data_GA20ox.pos_enz);
AICc=2*nparams*ndata_GA20ox/(ndata_GA20ox-nparams-1)+ndata_GA20ox*log(f_best/ndata_GA20ox)

coef=10.^theta_GA20ox_Maize';
GA20oxfit_Maize=basisvals_plot*coef;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
% Fitting forcing function for GA20ox Arabidopsis for data_GA20ox.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

theta=zeros(nruns,nparams);
fval=zeros(1,nruns);
objfun=@(theta)objfun_fitinputscoefficients(theta,data_GA20ox.GA20ox_Arab,basisvals_meas_tp_enz);
for i=1:nruns
    [theta(i,:),fval(i)] = lsqnonlin(objfun,theta0(i,:),lb,ub,options);
end;;
[f_best,I]=min(fval);
theta_GA20ox_Arab=theta(I,:);
ndata_GA20ox=length(data_GA20ox.pos_enz);
AICc=2*nparams*ndata_GA20ox/(ndata_GA20ox-nparams-1)+ndata_GA20ox*log(f_best/ndata_GA20ox)

coef=10.^theta_GA20ox_Arab';
GA20oxfit_Arab=basisvals_plot*coef;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fitting forcing function for GA3ox data_GA20ox.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
theta=zeros(nruns,nparams);
fval=zeros(1,nruns);
objfun=@(theta)objfun_fitinputscoefficients(theta,data_GA20ox.GA3ox2,basisvals_meas_tp_enz);
for i=1:nruns
    [theta(i,:),fval(i)] = lsqnonlin(objfun,theta0(i,:),lb,ub,options);
end;
[f_best,I]=min(fval);
theta_GA3ox=theta(I,:);
ndata_GA20ox=length(data_GA20ox.pos_enz);
AICc=2*nparams*ndata_GA20ox/(ndata_GA20ox-nparams-1)+ndata_GA20ox*log(f_best/ndata_GA20ox)

coef=10.^theta_GA3ox';
GA3oxfit=basisvals_plot*coef;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fitting forcing function for GA2ox data_GA20ox.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
theta=zeros(nruns,nparams);
fval=zeros(1,nruns);
objfun=@(theta)objfun_fitinputscoefficients(theta,data_GA20ox.GA2ox,basisvals_meas_tp_enz);
for i=1:nruns
    [theta(i,:),fval(i)] = lsqnonlin(objfun,theta0(i,:),lb,ub,options);
end;
[f_best,I]=min(fval)
theta_GA2ox=theta(I,:)

ndata_GA20ox=length(data_GA20ox.pos_enz)
AICc=2*nparams*ndata_GA20ox/(ndata_GA20ox-nparams-1)+ndata_GA20ox*log(f_best/ndata_GA20ox)

coef=10.^theta_GA2ox';
GA2oxfit=basisvals_plot*coef;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fitting forcing function for GA53 data_GA20ox, with data_GA20ox converted to
% cytoplasmic concentrations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

data_GA20ox.kappa=1;
data_GA20ox.initialt=200;
data_GA20ox=calculateequationexpressions_innuc(data_GA20ox,basis,data_GA20ox.initialt);

% Convert data_GA20ox to cytoplasmic concentrations, assuming GA in nucleus:
[cyt_conv,~]=convertdata_cytconc(data_GA20ox);
GA53_data_GA20ox_cytconc=cyt_conv.*data_GA20ox.GA53;


theta=zeros(nruns,nparams);
fval=zeros(1,nruns);

objfun=@(theta)objfun_fitinputscoefficients(theta,GA53_data_GA20ox_cytconc,basisvals_meas_tp);
options = optimset('Display', 'off');
for i=1:nruns
    [theta(i,:),fval(i)] = lsqnonlin(objfun,theta0(i,:),lb,ub,options);
end;

[f_best,I]=min(fval)
theta_GA53_cytconc=theta(I,:)

ndata_GA20ox=length(data_GA20ox.pos_GAs)
AICc=2*nparams*ndata_GA20ox/(ndata_GA20ox-nparams-1)+ndata_GA20ox*log(f_best/ndata_GA20ox)

% Plot:
coef=10.^theta_GA53_cytconc';
GA53fit_cytconc=basisvals_plot*coef;

figure
subplot(2,3,1)
hold on
plot(1/1000*data_GA20ox.pos_GAs,GA53_data_GA20ox_cytconc,'*r','LineWidth',1)
plot(1/1000*x_results,GA53fit_cytconc,'-')
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('Cyt conc (nM)','Interpreter','latex','FontSize',16)
title('GA53','Interpreter','latex','FontSize',16)
box off

subplot(2,3,2)
hold on
errorbar(1/1000*data_GA20ox.pos_enz,data_GA20ox.GA20ox_Maize,data_GA20ox.GA20ox_Maize_se,'*r','LineWidth',1)
plot(1/1000*x_results,GA20oxfit_Maize,'-')
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('Level','Interpreter','latex','FontSize',16)
title('ZmGA20ox','Interpreter','latex','FontSize',16)
box off

subplot(2,3,3)
hold on
errorbar(1/1000*data_GA20ox.pos_enz,data_GA20ox.GA20ox_Arab,data_GA20ox.GA20ox_Arab_se,'*r','LineWidth',1)
plot(1/1000*x_results,GA20oxfit_Arab,'-')
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('Level','Interpreter','latex','FontSize',16)
title('AtGA20ox','Interpreter','latex','FontSize',16)
box off

subplot(2,3,4)
hold on
errorbar(1/1000*data_GA20ox.pos_enz,data_GA20ox.GA3ox2,data_GA20ox.GA3ox2_se,'*r','LineWidth',1)
plot(1/1000*x_results,GA3oxfit,'-')
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('Level','Interpreter','latex','FontSize',16)
title('GA3ox2','Interpreter','latex','FontSize',16)
box off

subplot(2,3,5)
hold on
errorbar(1/1000*data_GA20ox.pos_enz,data_GA20ox.GA2ox,data_GA20ox.GA2ox_se,'*r','LineWidth',1)
plot(1/1000*x_results,GA2oxfit,'-')
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('Level','Interpreter','latex','FontSize',16)
title('GA2ox','Interpreter','latex','FontSize',16)
box off

theta_fittedinputs=[theta_GA53_cytconc,theta_GA20ox_Maize,theta_GA3ox,theta_GA2ox,theta_GA20ox_Arab]

%save fittedinputs_B104GA20ox_Arabdiff_GA53nM_nbasis7_vector_norder3_evenbasis_innuc_initialt1000_GA20oxOEAdata.mat theta_fittedinputs nbasis norder basis breaks rng

