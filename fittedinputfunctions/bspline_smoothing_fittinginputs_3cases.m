% Fits b-spline functions to the data of the four 'input functions' GA53, GA20ox,
% GA3ox and GA2ox, from the three conditions: control, drought and cold
% conditions.

% Produces a .mat file containing three vectors of the estimated b-spline
% co-efficients for each condition. Also plots figures showing the fits and
% the data. 

% Uses the fdaM package for creating and manipulating the b-spline
% functions.

function bspline_smoothing_fittinginputs_GA53nM_evenbasis_3cases

addpath('../semethod')

load ../data/drought_cold/data_PCRenz_GAconc_nM_threecases_final_Ainterp_allenzpts.mat

ntime=length(data.pos_GAs);
ntime_enz=length(data.pos_enz);


addpath ('../fdaM')
initialt=1000;
tvec=[initialt;data.pos_GAs];
rng   = [initialt,max(tvec)];
norder=3;
nbasis=7;

basis = create_bspline_basis(rng, nbasis,norder);
params   = getbasispar(basis);
breaks   = [rng(1), params, rng(2)];
nparams=nbasis;

% Replace three lines above with lines below are for an uneven basis.
%nb=nbasis-norder;
%breaks=[rng(1), 95000*fliplr(exp(-linspace(1,2.5,nb))), rng(2)]
%basis = create_bspline_basis(rng, [],norder,breaks);


figure
plot(basis)

basisvals_meas_tp=eval_basis(data.pos_GAs,basis);
basisvals_meas_tp_enz=eval_basis(data.pos_enz,basis);

% For plotting solutions:
x_results=[initialt:100:max(tvec)];
basisvals_plot=eval_basis(x_results,basis);

nruns=10;
nparams=nbasis;
lb=-6*ones(nparams,1);
ub=4*ones(nparams,1);

theta0_01=lhsdesign(nruns,nparams);
 theta0=zeros(nruns,nparams);
 for j=1:nruns
     theta0(j,:)=theta0_01(j,:).*(ub-lb)'+lb';
 end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
% Fitting forcing function for GA20ox data.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
theta=zeros(nruns,nparams);
fval=zeros(1,nruns);
objfun=@(theta)objfun_fitinputscoefficients(theta,data.GA20ox,basisvals_meas_tp_enz);
options = optimset('Display', 'off');
for i=1:nruns
    [theta(i,:),fval(i)] = lsqnonlin(objfun,theta0(i,:),lb,ub,options);
end;
[f_best,I]=min(fval);
theta_GA20ox=theta(I,:);
GA20oxfit=basisvals_plot*(10.^theta_GA20ox');

theta=zeros(nruns,nparams);
fval=zeros(1,nruns);
objfun=@(theta)objfun_fitinputscoefficients(theta,data_cold.GA20ox,basisvals_meas_tp_enz);
for i=1:nruns
    [theta(i,:),fval(i)] = lsqnonlin(objfun,theta0(i,:),lb,ub,options);
end;
[f_best_cold,I]=min(fval);
theta_cold_GA20ox=theta(I,:);
GA20oxfit_cold=basisvals_plot*(10.^theta_cold_GA20ox');

theta=zeros(nruns,nparams);
fval=zeros(1,nruns);
objfun=@(theta)objfun_fitinputscoefficients(theta,data_drought.GA20ox,basisvals_meas_tp_enz);
for i=1:nruns
    [theta(i,:),fval(i)] = lsqnonlin(objfun,theta0(i,:),lb,ub,options);
end;
[f_best_drought,I]=min(fval);
theta_drought_GA20ox=theta(I,:);
GA20oxfit_drought=basisvals_plot*(10.^theta_drought_GA20ox');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fitting forcing function for GA3ox data.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

theta=zeros(nruns,nparams);
fval=zeros(1,nruns);
objfun=@(theta)objfun_fitinputscoefficients(theta,data.GA3ox2,basisvals_meas_tp_enz);
for i=1:nruns
    [theta(i,:),fval(i)] = lsqnonlin(objfun,theta0(i,:),lb,ub,options);
end;
[f_best,I]=min(fval);
theta_GA3ox=theta(I,:);
GA3oxfit=basisvals_plot*(10.^theta_GA3ox');

theta=zeros(nruns,nparams);
fval=zeros(1,nruns);
objfun=@(theta)objfun_fitinputscoefficients(theta,data_cold.GA3ox2,basisvals_meas_tp_enz);
for i=1:nruns
    [theta(i,:),fval(i)] = lsqnonlin(objfun,theta0(i,:),lb,ub,options);
end;
[f_best_cold,I]=min(fval);
theta_cold_GA3ox=theta(I,:);
GA3oxfit_cold=basisvals_plot*(10.^theta_cold_GA3ox');

theta=zeros(nruns,nparams);
fval=zeros(1,nruns);
objfun=@(theta)objfun_fitinputscoefficients(theta,data_drought.GA3ox2,basisvals_meas_tp_enz);
for i=1:nruns
    [theta(i,:),fval(i)] = lsqnonlin(objfun,theta0(i,:),lb,ub,options);
end;
[f_best_drought,I]=min(fval);
theta_drought_GA3ox=theta(I,:);
GA3oxfit_drought=basisvals_plot*(10.^theta_drought_GA3ox');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fitting forcing function for GA2ox data.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
theta=zeros(nruns,nparams);
fval=zeros(1,nruns);
objfun=@(theta)objfun_fitinputscoefficients(theta,data.GA2ox,basisvals_meas_tp_enz);
for i=1:nruns
    [theta(i,:),fval(i)] = lsqnonlin(objfun,theta0(i,:),lb,ub,options);
end;

[f_best,I]=min(fval);
ndata=length(data.pos_enz);
AICc_GA20ox_control=2*nparams*ndata/(ndata-nparams-1)+ndata*log(f_best/ndata)

theta_GA2ox=theta(I,:);
GA2oxfit=basisvals_plot*(10.^theta_GA2ox');

theta=zeros(nruns,nparams);
fval=zeros(1,nruns);
objfun=@(theta)objfun_fitinputscoefficients(theta,data_cold.GA2ox,basisvals_meas_tp_enz);
for i=1:nruns
    [theta(i,:),fval(i)] = lsqnonlin(objfun,theta0(i,:),lb,ub,options);
end;
[f_best,I]=min(fval);
theta_cold_GA2ox=theta(I,:);
GA2oxfit_cold=basisvals_plot*(10.^theta_cold_GA2ox');

theta=zeros(nruns,nparams);
fval=zeros(1,nruns);
objfun=@(theta)objfun_fitinputscoefficients(theta,data_drought.GA2ox,basisvals_meas_tp_enz);
for i=1:nruns
    [theta(i,:),fval(i)] = lsqnonlin(objfun,theta0(i,:),lb,ub,options);
end;
[f_best,I]=min(fval);
theta_drought_GA2ox=theta(I,:);
GA2oxfit_drought=basisvals_plot*(10.^theta_drought_GA2ox');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fitting forcing function for GA53 data, with data converted to
% cytoplasmic concentrations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

data.kappa=1; 
data_cold.kappa=1;
data_drought.kappa=1;

data.initialt=initialt;
data_cold.initialt=initialt;
data_drought.initialt=initialt;

basisA = create_bspline_basis(rng, nbasis,norder);
data=calculateequationexpressions_innuc(data,basisA,data.initialt);
data_cold=calculateequationexpressions_innuc(data_cold,basisA,data_cold.initialt);
data_drought=calculateequationexpressions_innuc(data_drought,basisA,data_drought.initialt);

% Convert data to cytoplasmic concentrations, assuming GA in nucleus (see dataconversion_wcompartments for alternative assumptions):
[cyt_conv,~]=convertdata_cytconc(data);
GA53_data_cytconc=cyt_conv.*data.GA53;

[cyt_conv_cold,~]=convertdata_cytconc(data_cold);
GA53_data_cytconc_cold=cyt_conv_cold.*data_cold.GA53;

[cyt_conv_drought,~]=convertdata_cytconc(data_drought);
GA53_data_cytconc_drought=cyt_conv_drought.*data_drought.GA53;


theta=zeros(nruns,nparams);
fval=zeros(1,nruns);
objfun=@(theta)objfun_fitinputscoefficients(theta,GA53_data_cytconc,basisvals_meas_tp);
for i=1:nruns
    [theta(i,:),fval(i)] = lsqnonlin(objfun,theta0(i,:),lb,ub,options);
end;
[f_best,I]=min(fval);
theta_GA53_cytconc=theta(I,:);
GA53fit_cytconc=basisvals_plot*(10.^theta_GA53_cytconc');

theta=zeros(nruns,nparams);
fval=zeros(1,nruns);
objfun=@(theta)objfun_fitinputscoefficients(theta,GA53_data_cytconc_cold,basisvals_meas_tp);
for i=1:nruns
    [theta(i,:),fval(i)] = lsqnonlin(objfun,theta0(i,:),lb,ub,options);
end;
[f_best,I]=min(fval);
theta_cold_GA53_cytconc=theta(I,:);
GA53fit_cytconc_cold=basisvals_plot*(10.^theta_cold_GA53_cytconc');

theta=zeros(nruns,nparams);
fval=zeros(1,nruns);
objfun=@(theta)objfun_fitinputscoefficients(theta,GA53_data_cytconc_drought,basisvals_meas_tp);
for i=1:nruns
    [theta(i,:),fval(i)] = lsqnonlin(objfun,theta0(i,:),lb,ub,options);
end;
[f_best,I]=min(fval);
theta_drought_GA53_cytconc=theta(I,:);
GA53fit_cytconc_drought=basisvals_plot*(10.^theta_drought_GA53_cytconc');

plotfittedinputs(data,GA53_data_cytconc,x_results,GA53fit_cytconc,GA20oxfit,GA3oxfit,GA2oxfit)

plotfittedinputs(data_cold,GA53_data_cytconc_cold,x_results,GA53fit_cytconc_cold,GA20oxfit_cold,GA3oxfit_cold,GA2oxfit_cold)

plotfittedinputs(data_drought,GA53_data_cytconc_drought,x_results,GA53fit_cytconc_drought,GA20oxfit_drought,GA3oxfit_drought,GA2oxfit_drought)


theta_fittedinputs_wt=[theta_GA53_cytconc,theta_GA20ox,theta_GA3ox,theta_GA2ox];
theta_fittedinputs_cold=[theta_cold_GA53_cytconc,theta_cold_GA20ox,theta_cold_GA3ox,theta_cold_GA2ox];
theta_fittedinputs_drought=[theta_drought_GA53_cytconc,theta_drought_GA20ox,theta_drought_GA3ox,theta_drought_GA2ox];

%save fittedinputs_3cases_allenzpts_GA53nM_nbasis7_vector_norder3_evenbasis_lbm6_initialt1000.mat theta_fittedinputs_wt theta_fittedinputs_cold theta_fittedinputs_drought nbasis norder basis breaks rng

end

function plotfittedinputs(data,GA53data,x_results,GA53fit,GA20oxfit,GA3oxfit,GA2oxfit)

figure
subplot(2,2,1)
hold on
plot(1/1000*data.pos_GAs,GA53data,'*r','LineWidth',1)
plot(1/1000*x_results,GA53fit,'-')
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('Cyt conc (nM)','Interpreter','latex','FontSize',16)
title('GA53','Interpreter','latex','FontSize',16)
box off

subplot(2,2,2)
hold on
plot(1/1000*data.pos_enz,data.GA20ox,'*r','LineWidth',1)
plot(1/1000*x_results,GA20oxfit,'-')
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('Level','Interpreter','latex','FontSize',16)
title('GA20ox','Interpreter','latex','FontSize',16)
box off

subplot(2,2,3)
hold on
plot(1/1000*data.pos_enz,data.GA3ox2,'*r','LineWidth',1)
plot(1/1000*x_results,GA3oxfit,'-')
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('Level','Interpreter','latex','FontSize',16)
title('GA3ox2','Interpreter','latex','FontSize',16)
box off

subplot(2,2,4)
hold on
plot(1/1000*data.pos_enz,data.GA2ox,'*r','LineWidth',1)
plot(1/1000*x_results,GA2oxfit,'-')
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('Level','Interpreter','latex','FontSize',16)
title('GA2ox','Interpreter','latex','FontSize',16)
box off

end
