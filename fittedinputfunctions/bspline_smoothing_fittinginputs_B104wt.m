% Fits b-spline functions to the data of the four 'input functions' GA53,
% GA20ox, GA3ox and GA2ox, for a single line. Used for both B104 wild type,
% and for the GA20oxOE line where we assume GA20ox levels are the summation
% of AtGA20ox and ZmGA20ox. (Change line 18 to compute using this data set).

% Produces a .mat file containing a vector of the estimated b-spline
% co-efficients. Also plots figures showing the fits and
% the data. 

% Uses the fdaM package for creating and manipulating the b-spline
% functions.

clear
addpath('../semethod')

load ../data/GA20oxOE/data_GA20oxOE_GAconc_nM_GA20oxAdata.mat

% data=data_GA20ox; % Change to fit functions for GA20ox OE data - note in
% this data structure GA20ox is the summation of GA20ox_Maize and GA20ox_Arab.

ntime=length(data.pos_GAs);
ntime_enz=length(data.pos_enz);

addpath ('../fdaM')
initialt=1000;
tvec=[initialt;data.pos_GAs];
rng   = [initialt,max(tvec)];
norder=3;
nbasis=7;


basis = create_bspline_basis(rng, nbasis,norder);
params   = getbasispar(basis);
breaks   = [rng(1), params, rng(2)];
nparams=nbasis;

% Following three lines make an uneven basis:
%nb=nbasis-norder;
%breaks=[rng(1), 95000*fliplr(exp(-linspace(1,2,nb))), rng(2)]
%basis = create_bspline_basis(rng, [],norder,breaks);

figure
plot(basis)

basisvals_meas_tp=eval_basis(data.pos_GAs,basis);
basisvals_meas_tp_enz=eval_basis(data.pos_enz,basis);

% For plotting solutions:
x_results=[initialt:100:max(tvec)];
basisvals_plot=eval_basis(x_results,basis);

nruns=10;
nparams=nbasis;
lb=-6*ones(nparams,1);
ub=4*ones(nparams,1);

theta0_01=lhsdesign(nruns,nparams);
 theta0=zeros(nruns,nparams);
 for j=1:nruns
     theta0(j,:)=theta0_01(j,:).*(ub-lb)'+lb';
 end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
% Fitting forcing function for GA20ox data.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
theta=zeros(nruns,nparams);
fval=zeros(1,nruns);

objfun=@(theta)objfun_fitinputscoefficients(theta,data.GA20ox,basisvals_meas_tp_enz);
options = optimset('Display', 'off');
for i=1:nruns
[theta(i,:),fval(i)] = lsqnonlin(objfun,theta0(i,:),lb,ub,options);
end;

[f_best,I]=min(fval)
theta_GA20ox=theta(I,:)


ndata=length(data.pos_enz)
AICc=2*nparams*ndata/(ndata-nparams-1)+ndata*log(f_best/ndata)

% Plot:
coef=10.^theta_GA20ox';
GA20oxfit=basisvals_plot*coef;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fitting forcing function for GA3ox data.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
x=data.pos_enz;
y=data.GA3ox2;

theta=zeros(nruns,nparams);
fval=zeros(1,nruns);

objfun=@(theta)objfun_fitinputscoefficients(theta,y,basisvals_meas_tp_enz);
options = optimset('Display', 'off');
for i=1:nruns
    [theta(i,:),fval(i)] = lsqnonlin(objfun,theta0(i,:),lb,ub,options);
end;

[f_best,I]=min(fval)
theta_GA3ox=theta(I,:)

ndata=length(data.pos_enz)
AICc=2*nparams*ndata/(ndata-nparams-1)+ndata*log(f_best/ndata)

% Plot:
coef=10.^theta_GA3ox';
GA3oxfit=basisvals_plot*coef;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fitting forcing function for GA2ox data.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
x=data.pos_enz;
y=data.GA2ox;

theta=zeros(nruns,nparams);
fval=zeros(1,nruns);

objfun=@(theta)objfun_fitinputscoefficients(theta,y,basisvals_meas_tp_enz);
options = optimset('Display', 'off');
for i=1:nruns
[theta(i,:),fval(i)] = lsqnonlin(objfun,theta0(i,:),lb,ub,options);
end;

[f_best,I]=min(fval)
theta_GA2ox=theta(I,:)

ndata=length(data.pos_enz)
AICc=2*nparams*ndata/(ndata-nparams-1)+ndata*log(f_best/ndata)

% Plot:
coef=10.^theta_GA2ox';
GA2oxfit=basisvals_plot*coef;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fitting forcing function for GA53 data, with data converted to
% cytoplasmic concentrations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

data.kappa=1;
data.initialt=initialt;
basisA = create_bspline_basis(rng, nbasis,norder);

data=calculateequationexpressions_innuc(data,basisA,data.initialt);

% Convert data to cytoplasmic concentrations, assuming GA in nucleus (see dataconversion_wcompartments for alternative assumptions):
[cyt_conv,~]=convertdata_cytconc(data);
GA53_data_cytconc=cyt_conv.*data.GA53;

theta=zeros(nruns,nparams);
fval=zeros(1,nruns);

objfun=@(theta)objfun_fitinputscoefficients(theta,GA53_data_cytconc,basisvals_meas_tp);
options = optimset('Display', 'off');
for i=1:nruns
    [theta(i,:),fval(i)] = lsqnonlin(objfun,theta0(i,:),lb,ub,options);
end;

[f_best,I]=min(fval)
theta_GA53_cytconc=theta(I,:)

ndata=length(data.pos_GAs)
AICc=2*nparams*ndata/(ndata-nparams-1)+ndata*log(f_best/ndata)

% Plot:
coef=10.^theta_GA53_cytconc';
GA53fit_cytconc=basisvals_plot*coef;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
% Plot the bspline fitted solutions together with the data:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%555

figure
subplot(2,2,1)
hold on
plot(1/1000*data.pos_GAs,GA53_data_cytconc,'*r','LineWidth',1)
plot(1/1000*x_results,GA53fit_cytconc,'-')
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('Cyt conc (nM)','Interpreter','latex','FontSize',16)
title('GA53','Interpreter','latex','FontSize',16)
box off

subplot(2,2,2)
hold on
errorbar(1/1000*data.pos_enz,data.GA20ox,data.GA20ox_se,'*r','LineWidth',1)
plot(1/1000*x_results,GA20oxfit,'-')
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('Level','Interpreter','latex','FontSize',16)
title('GA20ox','Interpreter','latex','FontSize',16)
box off

subplot(2,2,3)
hold on
errorbar(1/1000*data.pos_enz,data.GA3ox2,data.GA3ox2_se,'*r','LineWidth',1)
plot(1/1000*x_results,GA3oxfit,'-')
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('Level','Interpreter','latex','FontSize',16)
title('GA3ox2','Interpreter','latex','FontSize',16)
box off

subplot(2,2,4)
hold on
errorbar(1/1000*data.pos_enz,data.GA2ox,data.GA2ox_se,'*r','LineWidth',1)
plot(1/1000*x_results,GA2oxfit,'-')
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('Level','Interpreter','latex','FontSize',16)
title('GA2ox','Interpreter','latex','FontSize',16)
box off

theta_fittedinputs=[theta_GA53_cytconc,theta_GA20ox,theta_GA3ox,theta_GA2ox];

%save fittedinputs_B104wt_GA53nM_nbasis7_vector_norder3_evenbasis_innuc_initialt1000_B104Adata.mat theta_fittedinputs nbasis norder basis breaks rng


