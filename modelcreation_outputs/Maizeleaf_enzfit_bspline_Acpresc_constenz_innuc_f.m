function out = Maizeleaf_enzfit_bspline_Acpresc_constenz_innuc_f(t,x,theta,g,z)  %#ok<*INUSD,*INUSL> 

out = zeros(6,1);

out(1,1) = -(x(1)*z(3) - z(2)*(g(1)*theta(1)*theta(9) - theta(2)*theta(9)*x(1)))/z(1);
out(2,1) = -(x(2)*z(3) - z(2)*(theta(2)*theta(9)*x(1) - theta(3)*theta(9)*x(2)))/z(1);
out(3,1) = -(x(3)*z(3) + z(2)*(theta(4)*theta(10)*x(3) - theta(3)*theta(9)*x(2) + theta(5)*theta(11)*x(3)))/z(1);
out(4,1) = -(x(4)*z(3) - z(2)*(theta(4)*theta(10)*x(3) - theta(6)*theta(11)*x(4)))/z(1);
out(5,1) = -(x(5)*z(3) - z(2)*(theta(5)*theta(11)*x(3) - theta(7)*theta(11)*x(5)))/z(1);
out(6,1) = -(x(6)*z(3) - z(2)*(theta(6)*theta(11)*x(4) - theta(8)*theta(11)*x(6)))/z(1);
