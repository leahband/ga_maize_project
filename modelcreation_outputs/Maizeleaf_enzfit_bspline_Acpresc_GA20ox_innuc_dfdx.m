function out = Maizeleaf_enzfit_bspline_Acpresc_GA20ox_innuc_dfdx(t,x,theta,g,z)  %#ok<*INUSD,*INUSL> 

out = zeros(6,6);

out(1,1) = -(z(3) + z(2)*(g(2)*theta(2) + g(5)*theta(10)))/z(1);
out(2,1) = (z(2)*(g(2)*theta(2) + g(5)*theta(10)))/z(1);
out(2,2) = -(z(3) + z(2)*(g(2)*theta(3) + g(5)*theta(11)))/z(1);
out(3,2) = (z(2)*(g(2)*theta(3) + g(5)*theta(11)))/z(1);
out(3,3) = -(z(3) + z(2)*(g(3)*theta(4) + g(4)*theta(5)))/z(1);
out(4,3) = (g(3)*theta(4)*z(2))/z(1);
out(5,3) = (g(4)*theta(5)*z(2))/z(1);
out(4,4) = -(z(3) + g(4)*theta(6)*z(2))/z(1);
out(6,4) = (g(4)*theta(6)*z(2))/z(1);
out(5,5) = -(z(3) + g(4)*theta(7)*z(2))/z(1);
out(6,6) = -(z(3) + g(4)*theta(8)*z(2))/z(1);
