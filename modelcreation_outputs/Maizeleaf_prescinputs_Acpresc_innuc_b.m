function out = Maizeleaf_prescinputs_Acpresc_innuc_b(t,x,theta,z)  %#ok<*INUSD,*INUSL> 

out = zeros(6,1);

out(1,1) = -(x(1)*z(3))/z(1);
out(2,1) = -(x(2)*z(3))/z(1);
out(3,1) = -(x(3)*z(3))/z(1);
out(4,1) = -(x(4)*z(3))/z(1);
out(5,1) = -(x(5)*z(3))/z(1);
out(6,1) = -(x(6)*z(3))/z(1);
