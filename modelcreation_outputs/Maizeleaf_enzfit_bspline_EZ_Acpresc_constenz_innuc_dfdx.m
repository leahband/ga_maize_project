function out = Maizeleaf_enzfit_bspline_EZ_Acpresc_constenz_innuc_dfdx(t,x,theta,g,z)  %#ok<*INUSD,*INUSL> 

out = zeros(6,6);

out(1,1) = -(z(6) + theta(2)*theta(9)*z(5))/(z(1)*z(4));
out(2,1) = (theta(2)*theta(9)*z(5))/(z(1)*z(4));
out(2,2) = -(z(6) + theta(3)*theta(9)*z(5))/(z(1)*z(4));
out(3,2) = (theta(3)*theta(9)*z(5))/(z(1)*z(4));
out(3,3) = -(z(6) + z(5)*(theta(4)*theta(10) + theta(5)*theta(11)))/(z(1)*z(4));
out(4,3) = (theta(4)*theta(10)*z(5))/(z(1)*z(4));
out(5,3) = (theta(5)*theta(11)*z(5))/(z(1)*z(4));
out(4,4) = -(z(6) + theta(6)*theta(11)*z(5))/(z(1)*z(4));
out(6,4) = (theta(6)*theta(11)*z(5))/(z(1)*z(4));
out(5,5) = -(z(6) + theta(7)*theta(11)*z(5))/(z(1)*z(4));
out(6,6) = -(z(6) + theta(8)*theta(11)*z(5))/(z(1)*z(4));
