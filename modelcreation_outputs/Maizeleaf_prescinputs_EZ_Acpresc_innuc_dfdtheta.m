function out = Maizeleaf_prescinputs_EZ_Acpresc_innuc_dfdtheta(t,x,theta,z)  %#ok<*INUSD,*INUSL> 

out = zeros(6,8);

out(1,1) = (z(5)*z(7)*z(8))/(z(1)*z(4));
out(1,2) = -(x(1)*z(5)*z(8))/(z(1)*z(4));
out(2,2) = (x(1)*z(5)*z(8))/(z(1)*z(4));
out(2,3) = -(x(2)*z(5)*z(8))/(z(1)*z(4));
out(3,3) = (x(2)*z(5)*z(8))/(z(1)*z(4));
out(3,4) = -(x(3)*z(5)*z(9))/(z(1)*z(4));
out(4,4) = (x(3)*z(5)*z(9))/(z(1)*z(4));
out(3,5) = -(x(3)*z(5)*z(10))/(z(1)*z(4));
out(5,5) = (x(3)*z(5)*z(10))/(z(1)*z(4));
out(4,6) = -(x(4)*z(5)*z(10))/(z(1)*z(4));
out(6,6) = (x(4)*z(5)*z(10))/(z(1)*z(4));
out(5,7) = -(x(5)*z(5)*z(10))/(z(1)*z(4));
out(6,8) = -(x(6)*z(5)*z(10))/(z(1)*z(4));
