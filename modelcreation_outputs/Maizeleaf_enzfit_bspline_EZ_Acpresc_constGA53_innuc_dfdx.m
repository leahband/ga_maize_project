function out = Maizeleaf_enzfit_bspline_EZ_Acpresc_constGA53_innuc_dfdx(t,x,theta,g,z)  %#ok<*INUSD,*INUSL> 

out = zeros(6,6);

out(1,1) = -(z(6) + g(1)*theta(2)*z(5))/(z(1)*z(4));
out(2,1) = (g(1)*theta(2)*z(5))/(z(1)*z(4));
out(2,2) = -(z(6) + g(1)*theta(3)*z(5))/(z(1)*z(4));
out(3,2) = (g(1)*theta(3)*z(5))/(z(1)*z(4));
out(3,3) = -(z(6) + z(5)*(g(2)*theta(4) + g(3)*theta(5)))/(z(1)*z(4));
out(4,3) = (g(2)*theta(4)*z(5))/(z(1)*z(4));
out(5,3) = (g(3)*theta(5)*z(5))/(z(1)*z(4));
out(4,4) = -(z(6) + g(3)*theta(6)*z(5))/(z(1)*z(4));
out(6,4) = (g(3)*theta(6)*z(5))/(z(1)*z(4));
out(5,5) = -(z(6) + g(3)*theta(7)*z(5))/(z(1)*z(4));
out(6,6) = -(z(6) + g(3)*theta(8)*z(5))/(z(1)*z(4));
