function out = Maizeleaf_enzfit_bspline_EZ_Acpresc_GA20ox_innuc_dfdg(t,x,theta,g,z)  %#ok<*INUSD,*INUSL> 

out = zeros(6,5);

out(1,1) = (z(5)*(g(2)*theta(1) + g(5)*theta(9)))/(z(1)*z(4));
out(1,2) = (z(5)*(g(1)*theta(1) - theta(2)*x(1)))/(z(1)*z(4));
out(2,2) = (z(5)*(theta(2)*x(1) - theta(3)*x(2)))/(z(1)*z(4));
out(3,2) = (theta(3)*x(2)*z(5))/(z(1)*z(4));
out(3,3) = -(theta(4)*x(3)*z(5))/(z(1)*z(4));
out(4,3) = (theta(4)*x(3)*z(5))/(z(1)*z(4));
out(3,4) = -(theta(5)*x(3)*z(5))/(z(1)*z(4));
out(4,4) = -(theta(6)*x(4)*z(5))/(z(1)*z(4));
out(5,4) = (z(5)*(theta(5)*x(3) - theta(7)*x(5)))/(z(1)*z(4));
out(6,4) = (z(5)*(theta(6)*x(4) - theta(8)*x(6)))/(z(1)*z(4));
out(1,5) = (z(5)*(g(1)*theta(9) - theta(10)*x(1)))/(z(1)*z(4));
out(2,5) = (z(5)*(theta(10)*x(1) - theta(11)*x(2)))/(z(1)*z(4));
out(3,5) = (theta(11)*x(2)*z(5))/(z(1)*z(4));
