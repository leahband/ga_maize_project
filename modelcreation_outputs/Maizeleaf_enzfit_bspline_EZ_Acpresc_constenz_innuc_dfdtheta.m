function out = Maizeleaf_enzfit_bspline_EZ_Acpresc_constenz_innuc_dfdtheta(t,x,theta,g,z)  %#ok<*INUSD,*INUSL> 

out = zeros(6,18);

out(1,1) = (g(1)*theta(9)*z(5))/(z(1)*z(4));
out(1,2) = -(theta(9)*x(1)*z(5))/(z(1)*z(4));
out(2,2) = (theta(9)*x(1)*z(5))/(z(1)*z(4));
out(2,3) = -(theta(9)*x(2)*z(5))/(z(1)*z(4));
out(3,3) = (theta(9)*x(2)*z(5))/(z(1)*z(4));
out(3,4) = -(theta(10)*x(3)*z(5))/(z(1)*z(4));
out(4,4) = (theta(10)*x(3)*z(5))/(z(1)*z(4));
out(3,5) = -(theta(11)*x(3)*z(5))/(z(1)*z(4));
out(5,5) = (theta(11)*x(3)*z(5))/(z(1)*z(4));
out(4,6) = -(theta(11)*x(4)*z(5))/(z(1)*z(4));
out(6,6) = (theta(11)*x(4)*z(5))/(z(1)*z(4));
out(5,7) = -(theta(11)*x(5)*z(5))/(z(1)*z(4));
out(6,8) = -(theta(11)*x(6)*z(5))/(z(1)*z(4));
out(1,9) = (z(5)*(g(1)*theta(1) - theta(2)*x(1)))/(z(1)*z(4));
out(2,9) = (z(5)*(theta(2)*x(1) - theta(3)*x(2)))/(z(1)*z(4));
out(3,9) = (theta(3)*x(2)*z(5))/(z(1)*z(4));
out(3,10) = -(theta(4)*x(3)*z(5))/(z(1)*z(4));
out(4,10) = (theta(4)*x(3)*z(5))/(z(1)*z(4));
out(3,11) = -(theta(5)*x(3)*z(5))/(z(1)*z(4));
out(4,11) = -(theta(6)*x(4)*z(5))/(z(1)*z(4));
out(5,11) = (z(5)*(theta(5)*x(3) - theta(7)*x(5)))/(z(1)*z(4));
out(6,11) = (z(5)*(theta(6)*x(4) - theta(8)*x(6)))/(z(1)*z(4));
