function out = Maizeleaf_enzfit_bspline_EZ_Acpresc_constGA53_innuc_dfdtheta(t,x,theta,g,z)  %#ok<*INUSD,*INUSL> 

out = zeros(6,30);

out(1,1) = (g(1)*theta(9)*z(5))/(z(1)*z(4));
out(1,2) = -(g(1)*x(1)*z(5))/(z(1)*z(4));
out(2,2) = (g(1)*x(1)*z(5))/(z(1)*z(4));
out(2,3) = -(g(1)*x(2)*z(5))/(z(1)*z(4));
out(3,3) = (g(1)*x(2)*z(5))/(z(1)*z(4));
out(3,4) = -(g(2)*x(3)*z(5))/(z(1)*z(4));
out(4,4) = (g(2)*x(3)*z(5))/(z(1)*z(4));
out(3,5) = -(g(3)*x(3)*z(5))/(z(1)*z(4));
out(5,5) = (g(3)*x(3)*z(5))/(z(1)*z(4));
out(4,6) = -(g(3)*x(4)*z(5))/(z(1)*z(4));
out(6,6) = (g(3)*x(4)*z(5))/(z(1)*z(4));
out(5,7) = -(g(3)*x(5)*z(5))/(z(1)*z(4));
out(6,8) = -(g(3)*x(6)*z(5))/(z(1)*z(4));
out(1,9) = (g(1)*theta(1)*z(5))/(z(1)*z(4));
