function out = Maizeleaf_prescinputs_EZ_Acpresc_innuc_dfdx(t,x,theta,z)  %#ok<*INUSD,*INUSL> 

out = zeros(6,6);

out(1,1) = -(z(6) + theta(2)*z(5)*z(8))/(z(1)*z(4));
out(2,1) = (theta(2)*z(5)*z(8))/(z(1)*z(4));
out(2,2) = -(z(6) + theta(3)*z(5)*z(8))/(z(1)*z(4));
out(3,2) = (theta(3)*z(5)*z(8))/(z(1)*z(4));
out(3,3) = -(z(6) + z(5)*(theta(4)*z(9) + theta(5)*z(10)))/(z(1)*z(4));
out(4,3) = (theta(4)*z(5)*z(9))/(z(1)*z(4));
out(5,3) = (theta(5)*z(5)*z(10))/(z(1)*z(4));
out(4,4) = -(z(6) + theta(6)*z(5)*z(10))/(z(1)*z(4));
out(6,4) = (theta(6)*z(5)*z(10))/(z(1)*z(4));
out(5,5) = -(z(6) + theta(7)*z(5)*z(10))/(z(1)*z(4));
out(6,6) = -(z(6) + theta(8)*z(5)*z(10))/(z(1)*z(4));
