function out = Maizeleaf_enzfit_bspline_Acpresc_GA20ox_innuc_dfdtheta(t,x,theta,g,z)  %#ok<*INUSD,*INUSL> 

out = zeros(6,46);

out(1,1) = (g(1)*g(2)*z(2))/z(1);
out(1,2) = -(g(2)*x(1)*z(2))/z(1);
out(2,2) = (g(2)*x(1)*z(2))/z(1);
out(2,3) = -(g(2)*x(2)*z(2))/z(1);
out(3,3) = (g(2)*x(2)*z(2))/z(1);
out(3,4) = -(g(3)*x(3)*z(2))/z(1);
out(4,4) = (g(3)*x(3)*z(2))/z(1);
out(3,5) = -(g(4)*x(3)*z(2))/z(1);
out(5,5) = (g(4)*x(3)*z(2))/z(1);
out(4,6) = -(g(4)*x(4)*z(2))/z(1);
out(6,6) = (g(4)*x(4)*z(2))/z(1);
out(5,7) = -(g(4)*x(5)*z(2))/z(1);
out(6,8) = -(g(4)*x(6)*z(2))/z(1);
out(1,9) = (g(1)*g(5)*z(2))/z(1);
out(1,10) = -(g(5)*x(1)*z(2))/z(1);
out(2,10) = (g(5)*x(1)*z(2))/z(1);
out(2,11) = -(g(5)*x(2)*z(2))/z(1);
out(3,11) = (g(5)*x(2)*z(2))/z(1);
