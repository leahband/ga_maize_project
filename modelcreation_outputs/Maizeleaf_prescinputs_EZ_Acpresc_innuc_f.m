function out = Maizeleaf_prescinputs_EZ_Acpresc_innuc_f(t,x,theta,z)  %#ok<*INUSD,*INUSL> 

out = zeros(6,1);

out(1,1) = -(x(1)*z(6) + z(5)*(theta(2)*x(1)*z(8) - theta(1)*z(7)*z(8)))/(z(1)*z(4));
out(2,1) = -(x(2)*z(6) - z(5)*(theta(2)*x(1)*z(8) - theta(3)*x(2)*z(8)))/(z(1)*z(4));
out(3,1) = -(x(3)*z(6) + z(5)*(theta(4)*x(3)*z(9) - theta(3)*x(2)*z(8) + theta(5)*x(3)*z(10)))/(z(1)*z(4));
out(4,1) = -(x(4)*z(6) - z(5)*(theta(4)*x(3)*z(9) - theta(6)*x(4)*z(10)))/(z(1)*z(4));
out(5,1) = -(x(5)*z(6) - z(5)*(theta(5)*x(3)*z(10) - theta(7)*x(5)*z(10)))/(z(1)*z(4));
out(6,1) = -(x(6)*z(6) - z(5)*(theta(6)*x(4)*z(10) - theta(8)*x(6)*z(10)))/(z(1)*z(4));
