function out = Maizeleaf_enzfit_bspline_Acpresc_constGA53_innuc_dfdg(t,x,theta,g,z)  %#ok<*INUSD,*INUSL> 

out = zeros(6,3);

out(1,1) = (z(2)*(theta(1)*theta(9) - theta(2)*x(1)))/z(1);
out(2,1) = (z(2)*(theta(2)*x(1) - theta(3)*x(2)))/z(1);
out(3,1) = (theta(3)*x(2)*z(2))/z(1);
out(3,2) = -(theta(4)*x(3)*z(2))/z(1);
out(4,2) = (theta(4)*x(3)*z(2))/z(1);
out(3,3) = -(theta(5)*x(3)*z(2))/z(1);
out(4,3) = -(theta(6)*x(4)*z(2))/z(1);
out(5,3) = (z(2)*(theta(5)*x(3) - theta(7)*x(5)))/z(1);
out(6,3) = (z(2)*(theta(6)*x(4) - theta(8)*x(6)))/z(1);
