function out = Maizeleaf_prescinputs_Acpresc_innuc_dfdtheta(t,x,theta,z)  %#ok<*INUSD,*INUSL> 

out = zeros(6,8);

out(1,1) = (z(2)*z(7)*z(8))/z(1);
out(1,2) = -(x(1)*z(2)*z(8))/z(1);
out(2,2) = (x(1)*z(2)*z(8))/z(1);
out(2,3) = -(x(2)*z(2)*z(8))/z(1);
out(3,3) = (x(2)*z(2)*z(8))/z(1);
out(3,4) = -(x(3)*z(2)*z(9))/z(1);
out(4,4) = (x(3)*z(2)*z(9))/z(1);
out(3,5) = -(x(3)*z(2)*z(10))/z(1);
out(5,5) = (x(3)*z(2)*z(10))/z(1);
out(4,6) = -(x(4)*z(2)*z(10))/z(1);
out(6,6) = (x(4)*z(2)*z(10))/z(1);
out(5,7) = -(x(5)*z(2)*z(10))/z(1);
out(6,8) = -(x(6)*z(2)*z(10))/z(1);
