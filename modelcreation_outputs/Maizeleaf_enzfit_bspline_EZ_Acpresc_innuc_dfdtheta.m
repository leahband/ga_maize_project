function out = Maizeleaf_enzfit_bspline_EZ_Acpresc_innuc_dfdtheta(t,x,theta,g,z)  %#ok<*INUSD,*INUSL> 

out = zeros(6,36);

out(1,1) = (g(1)*g(2)*z(5))/(z(1)*z(4));
out(1,2) = -(g(2)*x(1)*z(5))/(z(1)*z(4));
out(2,2) = (g(2)*x(1)*z(5))/(z(1)*z(4));
out(2,3) = -(g(2)*x(2)*z(5))/(z(1)*z(4));
out(3,3) = (g(2)*x(2)*z(5))/(z(1)*z(4));
out(3,4) = -(g(3)*x(3)*z(5))/(z(1)*z(4));
out(4,4) = (g(3)*x(3)*z(5))/(z(1)*z(4));
out(3,5) = -(g(4)*x(3)*z(5))/(z(1)*z(4));
out(5,5) = (g(4)*x(3)*z(5))/(z(1)*z(4));
out(4,6) = -(g(4)*x(4)*z(5))/(z(1)*z(4));
out(6,6) = (g(4)*x(4)*z(5))/(z(1)*z(4));
out(5,7) = -(g(4)*x(5)*z(5))/(z(1)*z(4));
out(6,8) = -(g(4)*x(6)*z(5))/(z(1)*z(4));
