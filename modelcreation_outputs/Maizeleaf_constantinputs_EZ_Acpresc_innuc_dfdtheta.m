function out = Maizeleaf_constantinputs_EZ_Acpresc_innuc_dfdtheta(t,x,theta,z)  %#ok<*INUSD,*INUSL> 

out = zeros(6,12);

out(1,1) = (theta(9)*theta(10)*z(5))/(z(1)*z(4));
out(1,2) = -(theta(10)*x(1)*z(5))/(z(1)*z(4));
out(2,2) = (theta(10)*x(1)*z(5))/(z(1)*z(4));
out(2,3) = -(theta(10)*x(2)*z(5))/(z(1)*z(4));
out(3,3) = (theta(10)*x(2)*z(5))/(z(1)*z(4));
out(3,4) = -(theta(11)*x(3)*z(5))/(z(1)*z(4));
out(4,4) = (theta(11)*x(3)*z(5))/(z(1)*z(4));
out(3,5) = -(theta(12)*x(3)*z(5))/(z(1)*z(4));
out(5,5) = (theta(12)*x(3)*z(5))/(z(1)*z(4));
out(4,6) = -(theta(12)*x(4)*z(5))/(z(1)*z(4));
out(6,6) = (theta(12)*x(4)*z(5))/(z(1)*z(4));
out(5,7) = -(theta(12)*x(5)*z(5))/(z(1)*z(4));
out(6,8) = -(theta(12)*x(6)*z(5))/(z(1)*z(4));
out(1,9) = (theta(1)*theta(10)*z(5))/(z(1)*z(4));
out(1,10) = (z(5)*(theta(1)*theta(9) - theta(2)*x(1)))/(z(1)*z(4));
out(2,10) = (z(5)*(theta(2)*x(1) - theta(3)*x(2)))/(z(1)*z(4));
out(3,10) = (theta(3)*x(2)*z(5))/(z(1)*z(4));
out(3,11) = -(theta(4)*x(3)*z(5))/(z(1)*z(4));
out(4,11) = (theta(4)*x(3)*z(5))/(z(1)*z(4));
out(3,12) = -(theta(5)*x(3)*z(5))/(z(1)*z(4));
out(4,12) = -(theta(6)*x(4)*z(5))/(z(1)*z(4));
out(5,12) = (z(5)*(theta(5)*x(3) - theta(7)*x(5)))/(z(1)*z(4));
out(6,12) = (z(5)*(theta(6)*x(4) - theta(8)*x(6)))/(z(1)*z(4));
