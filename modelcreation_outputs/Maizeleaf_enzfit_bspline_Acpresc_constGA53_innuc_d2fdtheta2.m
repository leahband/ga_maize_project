function out = Maizeleaf_enzfit_bspline_Acpresc_constGA53_innuc_d2fdtheta2(t,x,theta,g,z)  %#ok<*INUSD,*INUSL> 

out = zeros(6,30,30);

out(1,9,1) = (g(1)*z(2))/z(1);
out(1,1,9) = (g(1)*z(2))/z(1);
