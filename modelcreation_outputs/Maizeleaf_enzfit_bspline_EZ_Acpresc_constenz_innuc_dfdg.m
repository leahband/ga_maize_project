function out = Maizeleaf_enzfit_bspline_EZ_Acpresc_constenz_innuc_dfdg(t,x,theta,g,z)  %#ok<*INUSD,*INUSL> 

out = zeros(6,1);

out(1,1) = (theta(1)*theta(9)*z(5))/(z(1)*z(4));
