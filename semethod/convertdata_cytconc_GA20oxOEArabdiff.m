function [cyt_conv,par_data]=convertdata_cytconc_GA20oxOEArabdiff(data)

ntime=length(data.pos_GAs);
ntime_DZ=length(data.pos_GAs(data.pos_GAs<data.xm));

% Convert data to cytoplasmic concentrations, assuming GA in nucleus (see dataconversion_wcompartments for alternative assumptions):
Vdata=zeros(1,ntime);
for j=1:ntime
    bin=(data.pos_GAs(j)-data.initialt)/50+1;  % Assumes time points for GA data multiples of 50.
    Vdata(j)=data.cell_length(bin).*data.A_smooth(bin);
end
VdataDZ=Vdata(data.pos_GAs<data.xm)';
VdataEZ=Vdata(data.xm<data.pos_GAs)';

cyt_conv=[ones(ntime_DZ,1);VdataEZ./(data.Vmf+data.kappa*(VdataEZ-data.Vmf))];

par_data=[cyt_conv.*data.GA44;cyt_conv.*data.GA19;cyt_conv.*data.GA20;cyt_conv.*data.GA1;cyt_conv.*data.GA29;cyt_conv.*data.GA8;cyt_conv.*data.GA53;...
    data.GA20ox_Maize;data.GA3ox2;data.GA2ox;data.GA20ox_Arab];