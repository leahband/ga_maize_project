
function [f,J,sol_m,sol_EZ,sol_enz_vec]=objfun_Acpresc_nM_cyt(thetahat,par_data,mdl_m,mdl_EZ,pars_m,pars_EZ,ntime,ntime_enz,nparams,nvars,u,nkinparams,nbasis,basisvals_meas_tp,basisvals_meas_tp_enz,J_inputs,recipmaxvec,data,breaks,norder)

pars_m.theta=10.^thetahat;
pars_EZ.theta=pars_m.theta;

th_inputfunc_reshape=reshape(pars_m.theta(nkinparams+1:nparams),nbasis,u);
%pars_m.x0=[0;0;0;0;0;0];
pars_m.x0=v0_initialconditions(pars_m.theta,th_inputfunc_reshape,data.initialt,breaks,norder,data);
% Solve for x and dxdtheta using sensitivity equations:
[sol_m,~,dxdtheta_m,~] = mdl_m.solveWithSensEqns_bsplines(pars_m,data,breaks,norder);
pars_EZ.x0=sol_m.x(:,end);
[sol_EZ,~,dxdtheta_EZ,~] = mdl_EZ.solveWithSensEqns_bsplines(pars_EZ,data,breaks,norder);

sol_input_GA=basisvals_meas_tp*th_inputfunc_reshape(:,1);
sol_input_enz=basisvals_meas_tp_enz*th_inputfunc_reshape(:,2:4);

sol_GAs_nM=[sol_m.x(:,2:end-1),sol_EZ.x(:,2:end);sol_input_GA']; % Note now skip the first time point entry, as this in the initial condition.

ntGAs=(nvars+1)*ntime;
sol_GAs_vec=reshape(sol_GAs_nM',ntGAs,1); 
sol_enz_vec=reshape(sol_input_enz,3*ntime_enz,1);

% Calculate the objective function value, f: 
f=recipmaxvec.*([sol_GAs_vec;sol_enz_vec]-par_data); %The function whose sum of squares is minimized.

% Calculate the Jacobian:
% Form dxdtheta to include both regions and input functions:
dxdtheta=cat(3,dxdtheta_m(:,:,2:end-1),dxdtheta_EZ(:,:,2:end));
% rearranges the elements of dxdtheta to form J
z=permute(dxdtheta,[3 1 2]);
J_vars=reshape(z,[nvars*ntime,nparams]);
%Extend to include inputs, using matrix calculated in run program:
J=[J_vars;J_inputs];

% Scale J due to model predicting objective function
% scaling with max of each dataset:
overallscale=repmat(recipmaxvec,1,nparams);  
J=overallscale.*J;

% Scale J as working in log parameter space:
multmat=log(10)*repmat(pars_m.theta,[(ntGAs+ntime_enz*3) 1]);  % pars_m.theta is a horizontal vector.
J=multmat.*J;





    









