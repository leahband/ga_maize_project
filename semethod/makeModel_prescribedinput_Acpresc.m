function [mdl,pars,obsData] = makeModel_prescribedinput_Acpresc(modelName,symbCalcFlag)


% z(1) : v(x)


% With Ac prescribed:
%z(11)= A(x)
% z(12)= dA/dx
% z(13)= Amf

% For cytoplasmic expansion case, introduce
% z(11): gamma=alpha/X in EZ cytoplasmic expansion case

% bslpine input functions:
% g(1) : GA53
% g(2) : GA20ox
% g(3) : GA3ox
% g(4) : GA2ox


% theta(1):  lambda_53
% theta(2): lambda_44
% theta(3):  lambda_19
% theta(4):  lambda_20
% theta(5)  gamma_20
% theta(6):   gamma_1
% theta(7):  gamma_29
% theta (8):   gamma_8

switch modelName
        
        case 'Maizeleaf_prescinputs_Acpresc_innuc'
        p=8;
        v=6;
        w=10;  % No of input funcs prescribed from data
        
        f{1}=['[1/z(1)*(z(2)*(theta(1)*z(7)*z(8)-theta(2)*x(1)*z(8))-x(1)*z(3));'];
        f{2}=['1/z(1)*(z(2)*(theta(2)*x(1)*z(8)-theta(3)*x(2)*z(8))-x(2)*z(3));'];
        f{3}=['1/z(1)*(z(2)*(theta(3)*x(2)*z(8)-theta(4)*x(3)*z(9)-theta(5)*x(3)*z(10))-x(3)*z(3));'];
        f{4}=['1/z(1)*(z(2)*(theta(4)*x(3)*z(9)-theta(6)*x(4)*z(10))-x(4)*z(3));'];
        f{5}=['1/z(1)*(z(2)*(theta(5)*x(3)*z(10)-theta(7)*x(5)*z(10))-x(5)*z(3));'];
        f{6}=['1/z(1)*(z(2)*(theta(6)*x(4)*z(10)-theta(8)*x(6)*z(10))-x(6)*z(3))]'];
        
        f = strcat(f{:})
        theta = ones(1,p); % Arbitrary
        x0 = ones(1,v);
        t =[2500,7500,12500,17500,25000,35000,45000,55000,65000,75000,85000,95000]
        
        
    case 'Maizeleaf_prescinputs_EZ_Acpresc_innuc'
        p=8;
        v=6;
        w=10;  % No of input funcs prescribed from data
                
        f{1}=['[1/(z(1)*z(4))*(z(5)*(theta(1)*z(7)*z(8)-theta(2)*x(1)*z(8))-z(6)*x(1));'];
        f{2}=['1/(z(1)*z(4))*(z(5)*(theta(2)*x(1)*z(8)-theta(3)*x(2)*z(8))-z(6)*x(2));'];
        f{3}=['1/(z(1)*z(4))*(z(5)*(theta(3)*x(2)*z(8)-theta(4)*x(3)*z(9)-theta(5)*x(3)*z(10))-z(6)*x(3));'];
        f{4}=['1/(z(1)*z(4))*(z(5)*(theta(4)*x(3)*z(9)-theta(6)*x(4)*z(10))-z(6)*x(4));'];
        f{5}=['1/(z(1)*z(4))*(z(5)*(theta(5)*x(3)*z(10)-theta(7)*x(5)*z(10))-z(6)*x(5));'];
        f{6}=['1/(z(1)*z(4))*(z(5)*(theta(6)*x(4)*z(10)-theta(8)*x(6)*z(10))-z(6)*x(6))]'];
        
        f = strcat(f{:})
        theta = ones(1,p); % Arbitrary
        x0 = ones(1,v);
        t =[2500,7500,12500,17500,25000,35000,45000,55000,65000,75000,85000,95000]

 case 'Maizeleaf_constantinputs_Acpresc_innuc'
        p=12; % 8 kinetic parameters and 4 constant input functions
        v=6;
        w=6;  % No of input funcs prescribed from data
        
        f{1}=['[1/z(1)*(z(2)*(theta(1)*theta(9)*theta(10)-theta(2)*x(1)*theta(10))-x(1)*z(3));'];
        f{2}=['1/z(1)*(z(2)*(theta(2)*x(1)*theta(10)-theta(3)*x(2)*theta(10))-x(2)*z(3));'];
        f{3}=['1/z(1)*(z(2)*(theta(3)*x(2)*theta(10)-theta(4)*x(3)*theta(11)-theta(5)*x(3)*theta(12))-x(3)*z(3));'];
        f{4}=['1/z(1)*(z(2)*(theta(4)*x(3)*theta(11)-theta(6)*x(4)*theta(12))-x(4)*z(3));'];
        f{5}=['1/z(1)*(z(2)*(theta(5)*x(3)*theta(12)-theta(7)*x(5)*theta(12))-x(5)*z(3));'];
        f{6}=['1/z(1)*(z(2)*(theta(6)*x(4)*theta(12)-theta(8)*x(6)*theta(12))-x(6)*z(3))]'];
        
        f = strcat(f{:})
        theta = ones(1,p); % Arbitrary
        x0 = ones(1,v);
        t =[2500,7500,12500,17500,25000,35000,45000,55000,65000,75000,85000,95000]
        
        
    case 'Maizeleaf_constantinputs_EZ_Acpresc_innuc'
        p=12;
        v=6;
        w=6;  % No of input funcs prescribed from data
                
        f{1}=['[1/(z(1)*z(4))*(z(5)*(theta(1)*theta(9)*theta(10)-theta(2)*x(1)*theta(10))-z(6)*x(1));'];
        f{2}=['1/(z(1)*z(4))*(z(5)*(theta(2)*x(1)*theta(10)-theta(3)*x(2)*theta(10))-z(6)*x(2));'];
        f{3}=['1/(z(1)*z(4))*(z(5)*(theta(3)*x(2)*theta(10)-theta(4)*x(3)*theta(11)-theta(5)*x(3)*theta(12))-z(6)*x(3));'];
        f{4}=['1/(z(1)*z(4))*(z(5)*(theta(4)*x(3)*theta(11)-theta(6)*x(4)*theta(12))-z(6)*x(4));'];
        f{5}=['1/(z(1)*z(4))*(z(5)*(theta(5)*x(3)*theta(12)-theta(7)*x(5)*theta(12))-z(6)*x(5));'];
        f{6}=['1/(z(1)*z(4))*(z(5)*(theta(6)*x(4)*theta(12)-theta(8)*x(6)*theta(12))-z(6)*x(6))]'];
        
        f = strcat(f{:})
        theta = ones(1,p); % Arbitrary
        x0 = ones(1,v);
        t =[2500,7500,12500,17500,25000,35000,45000,55000,65000,75000,85000,95000]
        
        
    case 'Arabroot_prescinputs_meri'
        p=6;
        v=1;
        w=3;  % No of input funcs prescribed from data
        
        f = '[1/z(1)*(0.5*(z(2)-theta(1)*x(1))-theta(2)*x(1))]'
        theta = ones(1,p); % Arbitrary
        x0 = 0;
        t =linspace(50,1300,26)
        
    case 'Arabroot_prescinputs_EZ'
        p=6;
        v=1;
        w=3;  % No of input funcs prescribed from data
        
        f = '[1/(z(1)*(theta(4)+theta(5)*(z(3)-theta(4))))*(0.5*(z(2)-theta(6)*x(1))-theta(5)*theta(3)*z(3)*x(1))]'
        theta = ones(1,p); % Arbitrary
        x0 = 0;
        t =linspace(50,1300,26)
        
              
              
              
              
end

mdl = modl_prescribedinput(modelName,p,v,w);
%mdl = modl_prescribedinput_Arabnobsplines(modelName,p,v,w);

if nargout > 1; pars = params(mdl, theta, x0, t); end
if nargin > 1 && symbCalcFlag, doSymbolicCalcs_prescribedinput(modelName,f,p,v,w); end
if exist('xObs','var'), obsData = ser(t,xObs); else obsData = []; end
