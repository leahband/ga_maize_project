classdef modl
   %MODL  Class for ODE models
   
   properties
      name
      p
      v
   end
   
   methods
      
      function out = modl(varargin)  % generator
         defVals = cell(1,3);
         [defVals{1:length(varargin)}] = varargin{:};
         out.name = defVals{1};
         out.p = defVals{2};
         out.v = defVals{3};
      end
      
      % ----------------------------------------------------
      
      function out = solve(mdl,params)

         theta = params.theta;
         x0 = params.x0;
         fh = str2func([mdl.name '_f']);
         [t x]= ode15s(@(t,x) fh(t,x,theta),params.t,x0);
         
         if numel(params.t) == 2
            t = [t(1); t(end)];
            x = [x(1,:); x(end,:)];
         end
         
         % failure of numerical integration can make x wrongly dimensioned; 
         % if this happens then set x to be a array of correct
         % dimension filled with larges values (i.e., as a penalty)
         if size(x,1) ~= length(params.t)
            t = params.t';
            x = 1e99*ones(length(params.t),mdl.v);
         end
         
         out = ser(t',x');
         
      end
      
      % ----------------------------------------------------
      
      function [out,S,dxdtheta,dxdx0] = solveWithSensEqns(mdl,params)
         t = params.t;
         
         theta = params.theta;
         x0 = params.x0;
         fh = {str2func([mdl.name '_f']) str2func([mdl.name '_dfdtheta']) str2func([mdl.name '_dfdx'])};
         
         z0 = [x0; zeros(mdl.v*mdl.p,1); vec(eye(mdl.v))];
         n = length(t);
         
         [t z]= ode15s(@(t,z) func(t,z,theta,mdl,fh),t,z0);
         
         if numel(params.t) == 2
            t = [t(1); t(end)];
            z = [z(1,:); z(end,:)];
         end
         
         x = z(:,1:mdl.v);
         dxdtheta = reshape(z(:,mdl.v+1:mdl.v*(1+mdl.p))',[mdl.v,mdl.p,n]);
         dxdx0 = reshape(z(:,mdl.v*(1+mdl.p)+1:end)',[mdl.v,mdl.v,n]);
         
         % S = reshape(reshape(dxdtheta,[mdl.v, mdl.p*n])',[mdl.p, mdl.v*n])';
         
         dxdTheta = cat(2,dxdtheta,dxdx0);
         S = reshape(permute(dxdTheta,[1 3 2]),[mdl.v*n (mdl.p+mdl.v)]);
         
         out = ser(t',x');
         
         function out = func(t,z,theta,mdl,fh)
            % (underscores are on some variables to distiguish them from versions outside the function - otherwise not distinguished by the scoping rules)
            x_ = z(1:mdl.v);
            dxdtheta_ = reshape(z(mdl.v+1:mdl.v*(1+mdl.p)),[mdl.v,mdl.p]);
            dxdx0_ = reshape(z(mdl.v*(1+mdl.p)+1:end),[mdl.v,mdl.v]);
            f = fh{1}(t,x_,theta);  dfdtheta = fh{2}(t,x_,theta);  dfdx = fh{3}(t,x_,theta);
            foo1 = dfdtheta + dfdx*dxdtheta_;
            foo2 = dfdx*dxdx0_;
            out = [f; foo1(:); foo2(:)];
         end
         
  
         
      end
  
      
      % ----------------------------------------------------
      
      function out = solvePiecewise(model,t,theta,intervals,sm)
         % solves 'model' piecewise by dividing the domain specified in t
         % into d pieces.
         
         if numel(t) == 2, t = linspace(t(1),t(end),200); end
         
         if numel(intervals) == 1  % check that the value of d is valid
            d = intervals;
            if d ~= floor(d) || d < 1 || d >= numel(t)
               error('if input d is scalar it must be an integer satisfying 1 < d < n');
            end
            u = linspace(t(1),t(end),d+1);
         else
            u = intervals;
         end
         x_at_u = sm.eval(u,1);
         x = zeros(model.v,length(t));
         x(:,1) = x_at_u(:,1);
         for d_ = 1:d
            inds = (t>u(d_)&t<=u(d_+1));  % which indices are in the interval
            tPiece = [u(d_) t(inds)];
            x0Piece = x_at_u(:,d_);
            parPiece = params(model,theta,x0Piece,tPiece);
            solPiece = model.solve(parPiece);
            x(:,inds) = solPiece.x(:,2:end);
         end
         foo = ser(t,x);
         out = serpw(foo,u);
      end
      
%      % ----------------------------------------------------
%      
%      function out = evalRHS(model,ser)
%         % evaluates the right-hand side of ODE model, e.g. as needed by 
%         % Varah's gradient-matching approach
%         
%         theta = params.theta;
%         x0 = params.x0;
%         fh = str2func([mdl.name '_dfdtheta']);
%     
%         RHS = NaN(length(ser.t),1);
%         
%         
%         for k=1:length(ser.t)
%         % THIS IS STILL TO BE DONE!
%         end
%         
%         [t x]= ode15s(@(t,x) fh(t,x,theta),params.t,x0);
%         
%      end
      
   end
   
end

