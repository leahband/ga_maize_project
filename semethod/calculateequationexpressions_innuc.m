
function [data]=calculateequationexpressions_innuc(data,basis,initialt)

fdobj = smooth_basis(data.pos_Ac, data.Ac,basis);
%figure
%plotfit_fd(data.Ac,data.pos_Ac, fdobj) 
initpos=initialt/50+1;
finalpos=max(data.pos_GAs)/50+1;

data.position=data.position(initpos:finalpos);
data.RER=data.RER(initpos:finalpos);
data.cell_length=data.cell_length(initpos:finalpos);
data.vel=data.vel(initpos:finalpos);

data.A_smooth = eval_fd(data.position, fdobj);
data.dAdx_smooth=eval_fd(data.position, fdobj,1);

data.A_smooth=data.A_smooth';
data.dAdx_smooth=data.dAdx_smooth';

data.Vnuc=0.5*data.A_smooth(1)*data.cell_length(1);
data.Vmf=data.cell_length(data.xm/50+2-initpos)*data.A_smooth(data.xm/50+2-initpos);

data.z2=(data.A_smooth.*data.cell_length-data.Vnuc)./(data.A_smooth.*data.cell_length);
data.z3=data.RER+data.vel.*data.dAdx_smooth./data.A_smooth;

data.z4=data.Vmf+data.kappa*(data.A_smooth.*data.cell_length-data.Vmf);
data.z5=data.Vmf-data.Vnuc;
data.z6=data.kappa*data.cell_length.*(data.vel.*data.dAdx_smooth+data.A_smooth.*data.RER);



