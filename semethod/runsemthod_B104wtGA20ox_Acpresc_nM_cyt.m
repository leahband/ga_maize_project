% Estimates model parameters for the B104 wildtype and GA20oxOE data, 

% Assumes same kinetic rate constants for Maize and Arabidopsis GA20ox; in
% the mutant, we fit the summation of the two GA20oxidase (thus the mutant
% has the same number of bsplines as wildtype).

% Parameter estimates are used in plotting Figure S17.


function runsemthod_B104wtGA20ox_Acpresc_nM_cyt

[mdl_m,pars_m] = makeModel_bsplinesandprescribedinput_Acpresc('Maizeleaf_enzfit_bspline_Acpresc_innuc',1);
[mdl_EZ,pars_EZ] = makeModel_bsplinesandprescribedinput_Acpresc('Maizeleaf_enzfit_bspline_EZ_Acpresc_innuc',1);

addpath ('../modelcreation_outputs')

nruns=10;

load ../data/GA20oxOE/data_GA20oxOE_GAconc_nM_GA20oxAdata.mat

data_c1=data;
data_c2=data_GA20ox;

data_c1.initialt=1000;
data_c2.initialt=1000;
tvec=[data_c1.initialt;data_c1.pos_GAs];
mt_c1=[tvec(tvec<data_c1.xm);data_c1.xm];
EZt_c1=[data_c1.xm;tvec(tvec>data_c1.xm)];
mt_c2=[tvec(tvec<data_c2.xm);data_c2.xm];
EZt_c2=[data_c2.xm;tvec(tvec>data_c2.xm)];

ntime=length(data_c1.pos_GAs);
ntime_enz=length(data_c1.pos_enz);
pos_enz=data_c1.pos_enz;
u=mdl_m.u;
nenz=3;
nvars=mdl_m.v;
nkinparams=mdl_m.nkin;

addpath ('../fdaM')

load ../fittedinputfunctions/fittedinputs_B104wt_GA53nM_nbasis7_vector_norder3_evenbasis_innuc_initialt1000_B104Adata.mat theta_fittedinputs nbasis norder basis breaks rng
theta_fittedinputs_wt=theta_fittedinputs;

load ../fittedinputfunctions/fittedinputs_B104GA20oxOE_GA53nM_nbasis7_vector_norder3_evenbasis_innuc_initialt1000_B104Adata.mat  theta_fittedinputs
theta_fittedinputs_GA20ox=theta_fittedinputs;

nparams=nkinparams+2*nbasis*u;

if mdl_m.nbasis ~= nbasis
    warning('nbasis inconsistent')
end

basisvals_meas_tp=eval_basis(data_c1.pos_GAs,basis);
basisvals_meas_tp_enz=eval_basis(pos_enz,basis);

nbasisA=6;
data_c1.kappa=1;
data_c2.kappa=1;
basisA = create_bspline_basis(rng, nbasisA,norder);
data_c1=calculateequationexpressions_innuc(data_c1,basisA,data_c1.initialt);
data_c2=calculateequationexpressions_innuc(data_c2,basisA,data_c2.initialt);


% Convert data to cytoplasmic concentrations:
[cyt_conv_c1,par_data_c1]=convertdata_cytconc(data_c1);
[cyt_conv_c2,par_data_c2]=convertdata_cytconc(data_c2);
par_data=[par_data_c1;par_data_c2];

plotGAs_data_twocases(data.pos_GAs,par_data_c1,par_data_c2)

% Calculate Jacobian matrix for the inputs:
J_GA53_c1=[zeros(ntime,nkinparams),full(basisvals_meas_tp),zeros(ntime,nbasis*(u-1)),zeros(ntime,nbasis*u)];
J_enz_c1=zeros(nenz*ntime_enz,nparams);
for i=1:nenz
    J_enz_c1(ntime_enz*(i-1)+1:ntime_enz*i,:)=[zeros(ntime_enz,nkinparams),zeros(ntime_enz,nbasis*i),full(basisvals_meas_tp_enz),zeros(ntime_enz,nbasis*(u-1-i)),zeros(ntime_enz,nbasis*u)];
end
J_inputs_c1=[J_GA53_c1;J_enz_c1];

J_GA53_c2=[zeros(ntime,nkinparams),zeros(ntime,nbasis*u),full(basisvals_meas_tp),zeros(ntime,nbasis*(u-1))];
J_enz_c2=zeros(nenz*ntime_enz,nparams);
for i=1:nenz
    J_enz_c2(ntime_enz*(i-1)+1:ntime_enz*i,:)=[zeros(ntime_enz,nkinparams),zeros(ntime_enz,nbasis*u),zeros(ntime_enz,nbasis*i),full(basisvals_meas_tp_enz),zeros(ntime_enz,nbasis*(u-1-i))];
end
J_inputs_c2=[J_GA53_c2;J_enz_c2];


% Calculate vector of maximum values for scaling:
maxvec_GAs_c1=zeros((mdl_m.v+1)*ntime,1);
maxvec_GAs_c2=zeros((mdl_m.v+1)*ntime,1);

for i=1:mdl_m.v+1
    maxvec_GAs_c1((i-1)*ntime+1:i*ntime)=max(par_data_c1((i-1)*ntime+1:i*ntime))*ones(ntime,1);
    maxvec_GAs_c2((i-1)*ntime+1:i*ntime)=max(par_data_c2((i-1)*ntime+1:i*ntime))*ones(ntime,1);
end
maxvec_enz_c1=zeros(nenz*ntime_enz,1);
maxvec_enz_c2=zeros(nenz*ntime_enz,1);
for i=1:nenz
    maxvec_enz_c1((i-1)*ntime_enz+1:i*ntime_enz)=max(par_data_c1((mdl_m.v+1)*ntime+(i-1)*ntime_enz+1:(mdl_m.v+1)*ntime+i*ntime_enz))*ones(ntime_enz,1);
    maxvec_enz_c2((i-1)*ntime_enz+1:i*ntime_enz)=max(par_data_c2((mdl_m.v+1)*ntime+(i-1)*ntime_enz+1:(mdl_m.v+1)*ntime+i*ntime_enz))*ones(ntime_enz,1);
end
maxvec=[maxvec_GAs_c1;maxvec_enz_c1;maxvec_GAs_c2;maxvec_enz_c2];

recipmaxvec=1./maxvec;


ndata=2*(7*ntime+3*ntime_enz);

objfun=@(thetahat)objfun_Acpresc_nM_cyt_2cases(thetahat,par_data,mdl_m,mdl_EZ,pars_m,pars_EZ,ntime,ntime_enz,nparams,nvars,u,nkinparams,nbasis,...
    basisvals_meas_tp,basisvals_meas_tp_enz,J_inputs_c1,J_inputs_c2,recipmaxvec,data_c1,data_c2,breaks,...
    norder,mt_c1,EZt_c1,mt_c2,EZt_c2);

options = optimset('Jacobian','on','Display', 'off');
%parpool('AttachedFiles',{'bsplineM.m'})

lb=[-6*ones(nparams,1)];
ub=[3*ones(nkinparams,1);4*ones(2*nbasis*u,1)];

theta0_01=lhsdesign(nruns,nkinparams);
theta0=zeros(nruns,nparams);
for j=1:nruns
    theta0(j,1:nkinparams)=theta0_01(j,:).*(ub(1:nkinparams)-lb(1:nkinparams))'+lb(1:nkinparams)';
    theta0(j,nkinparams+1:mdl_m.p)=theta_fittedinputs_wt;
    theta0(j,mdl_m.p+1:end)=theta_fittedinputs_GA20ox;
end

theta=zeros(nruns,nparams);
fval=zeros(1,nruns);
exitflag=zeros(1,nruns);
telapsed_eachrun=zeros(1,nruns);


tstart = tic;
for i=1:nruns
    i
    [theta(i,:),fval(i),residual,exitflag(i),outputname] = lsqnonlin(objfun,theta0(i,:),lb,ub,options);
    telapsed_eachrun(i) = toc(tstart);
end;

%delete(gcp('nocreate'))
telapsed = toc(tstart);

[f_best,I]=min(fval);
theta_best=theta(I,:);
 
AIC=2*nparams+ndata*log(f_best/ndata)
AICc=2*nparams*ndata/(ndata-nparams-1)+ndata*log(f_best/ndata)

time_for_run(1)=telapsed_eachrun(1);
for j=2:nruns;
    time_for_run(j)=telapsed_eachrun(j)-telapsed_eachrun(j-1);
end

save ../parameter_estimates/semethod_B104wtandGA20oxOE_nM_cyt_evenbasis_nbas7_Acpresc_lbm6_innuc_init1000_B104Adata_test.mat
