
function [f,J]=objfun_Acpresc_nM_cyt_2cases_Arabdiff(thetahat,par_data,mdl_wt_m,mdl_wt_EZ,mdl_GA20ox_m,mdl_GA20ox_EZ,pars_wt_m,pars_wt_EZ,pars_GA20ox_m,pars_GA20ox_EZ,ntime,ntime_enz,nparams,nvars,u_wt,u_GA20ox,nkin_wt,nkin_GA20ox,nbasis,...
    basisvals_meas_tp,basisvals_meas_tp_enz,J_inputs_case1,J_inputs_case2,recipmaxvec,data_case1,data_case2,breaks,norder,mt_case1,EZt_case1,mt_case2,EZt_case2)
%global data

% Solutions for case one, where inputs are theta(1:36):
pars_wt_m.theta=10.^[thetahat(1:nkin_wt),thetahat(nkin_GA20ox+1:nkin_GA20ox+nbasis*u_wt)];
pars_wt_EZ.theta=pars_wt_m.theta;

pars_wt_m.t=mt_case1;
pars_wt_EZ.t=EZt_case1;
data=data_case1;
th_inputfunc_reshape=reshape(pars_wt_m.theta(nkin_wt+1:end),nbasis,u_wt);
pars_wt_m.x0=v0_initialconditions(pars_wt_m.theta,th_inputfunc_reshape,data.initialt,breaks,norder,data);
[sol_m_case1,~,dxdtheta_m_case1,~] = mdl_wt_m.solveWithSensEqns_bsplines(pars_wt_m,data,breaks,norder);
pars_wt_EZ.x0=sol_m_case1.x(:,end);
[sol_EZ_case1,~,dxdtheta_EZ_case1,~] = mdl_wt_EZ.solveWithSensEqns_bsplines(pars_wt_EZ,data,breaks,norder);

sol_input_GA=basisvals_meas_tp*th_inputfunc_reshape(:,1);
sol_input_enz=basisvals_meas_tp_enz*th_inputfunc_reshape(:,2:u_wt);

sol_GAs_nM=[sol_m_case1.x(:,2:end-1),sol_EZ_case1.x(:,2:end);sol_input_GA']; % Note now skip the first time point entry, as this in the initial condition.

ntGAs=(nvars+1)*ntime;
sol_GAs_vec_case1=reshape(sol_GAs_nM',ntGAs,1); 
sol_enz_vec_case1=reshape(sol_input_enz,(u_wt-1)*ntime_enz,1);

% Solutions for case two:
pars_GA20ox_m.theta=10.^thetahat([1:nkin_GA20ox,(nkin_GA20ox+nbasis*u_wt)+1:end]);
pars_GA20ox_EZ.theta=pars_GA20ox_m.theta;
pars_GA20ox_m.t=mt_case2;
pars_GA20ox_EZ.t=EZt_case2;
data=data_case2;
th_inputfunc_reshape=reshape(pars_GA20ox_m.theta(nkin_GA20ox+1:end),nbasis,u_GA20ox);
pars_GA20ox_m.x0=v0_initialconditions(pars_wt_m.theta,th_inputfunc_reshape,data.initialt,breaks,norder,data);
[sol_m_case2,~,dxdtheta_m_case2,~] = mdl_GA20ox_m.solveWithSensEqns_bsplines(pars_GA20ox_m,data,breaks,norder);
pars_GA20ox_EZ.x0=sol_m_case2.x(:,end);
[sol_EZ_case2,~,dxdtheta_EZ_case2,~] = mdl_GA20ox_EZ.solveWithSensEqns_bsplines(pars_GA20ox_EZ,data,breaks,norder);

sol_input_GA=basisvals_meas_tp*th_inputfunc_reshape(:,1);
sol_input_enz=basisvals_meas_tp_enz*th_inputfunc_reshape(:,2:u_GA20ox);

sol_GAs_nM=[sol_m_case2.x(:,2:end-1),sol_EZ_case2.x(:,2:end);sol_input_GA']; % Note now skip the first time point entry, as this in the initial condition.

sol_GAs_vec_case2=reshape(sol_GAs_nM',ntGAs,1); 
sol_enz_vec_case2=reshape(sol_input_enz,(u_GA20ox-1)*ntime_enz,1);

% Calculate the objective function value, f: 
f=recipmaxvec.*([sol_GAs_vec_case1;sol_enz_vec_case1;sol_GAs_vec_case2;sol_enz_vec_case2]-par_data); %The function whose sum of squares is minimized.

% Form dxdtheta to include both regions and input functions:
dxdtheta_case1_case1param=cat(3,dxdtheta_m_case1(:,:,2:end-1),dxdtheta_EZ_case1(:,:,2:end));
dxdtheta_case1=cat(2,dxdtheta_case1_case1param(:,1:nkin_wt,:),zeros(nvars,nkin_GA20ox-nkin_wt,ntime),dxdtheta_case1_case1param(:,nkin_wt+1:end,:),zeros(nvars,nbasis*u_GA20ox,ntime));
z_case1=permute(dxdtheta_case1,[3 1 2]);
J_vars_case1=reshape(z_case1,[nvars*ntime,nparams]);
J_case1=[J_vars_case1;J_inputs_case1];

dxdtheta_case2_case2param=cat(3,dxdtheta_m_case2(:,:,2:end-1),dxdtheta_EZ_case2(:,:,2:end));
dxdtheta_case2=cat(2,dxdtheta_case2_case2param(:,1:nkin_GA20ox,:),zeros(nvars,nbasis*u_wt,ntime),dxdtheta_case2_case2param(:,nkin_GA20ox+1:end,:));
z_case2=permute(dxdtheta_case2,[3 1 2]);
J_vars_case2=reshape(z_case2,[nvars*ntime,nparams]);
J_case2=[J_vars_case2;J_inputs_case2];

J=[J_case1;J_case2];

% Scale J due to objective function scaling with max of each dataset:
overallscale=repmat(recipmaxvec,1,nparams);  
J=overallscale.*J;

% Scale J as working in log parameter space:
multmat=log(10)*repmat(10.^thetahat,[2*ntGAs+ntime_enz*7 1]);
J=multmat.*J;





    









