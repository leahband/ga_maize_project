function x0=v0_initialconditions(theta,th_inputfunc_reshape,initialt,breaks,norder,data)

lambda53=theta(1);lambda44=theta(2);lambda19=theta(3);lambda20=theta(4);gamma20=theta(5);gamma1=theta(6);mu29=theta(7);mu8=theta(8);

basisvals = bsplineM(initialt, breaks, norder,0,0);
sol_input=basisvals*th_inputfunc_reshape;
GA53init=sol_input(1);
GA20oxmRNA=sol_input(2);
GA3oxmRNA=sol_input(3);
GA2oxmRNA=sol_input(4);

dilution=data.A_smooth(1)*data.cell_length(1)*data.RER(1)/(data.A_smooth(1)*data.cell_length(1)-data.Vnuc);
% RER nonzero solution:
 GA44init=lambda53*GA20oxmRNA*GA53init/(lambda44*GA20oxmRNA+dilution);
 GA19init=lambda44*GA20oxmRNA*GA44init/(lambda19*GA20oxmRNA+dilution);
 GA20init=lambda19*GA19init*GA20oxmRNA/(lambda20*GA3oxmRNA+gamma20*GA2oxmRNA+dilution);
 GA1init=lambda20*GA20init*GA3oxmRNA/(gamma1*GA2oxmRNA+dilution);
 GA29init=gamma20*GA20init*GA2oxmRNA/(mu29*GA2oxmRNA+dilution);
 GA8init=gamma1*GA1init*GA2oxmRNA/(mu8*GA2oxmRNA+dilution);
 
 x0=[GA44init;GA19init;GA20init;GA1init;GA29init;GA8init];