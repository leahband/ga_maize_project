% Estimates model parameters for the control, drought and cold
% datasets, all with B73 wildtype.
% Parameter estimates are used in plotting Figure 5H-Q.



function runsemthod_wt_Acpresc_nM_3cases

[mdl_m,pars_m] = makeModel_bsplinesandprescribedinput_Acpresc('Maizeleaf_enzfit_bspline_Acpresc_innuc',1);
[mdl_EZ,pars_EZ] = makeModel_bsplinesandprescribedinput_Acpresc('Maizeleaf_enzfit_bspline_EZ_Acpresc_innuc',1);

addpath ('../modelcreation_outputs')

nruns=10;

nkinparams=mdl_m.p-mdl_m.nbasis*mdl_m.u; % mdl_m.p is number of parameters in the model for one case. 

ninputparam=mdl_m.nbasis*mdl_m.u

% Specify which of the 16 cases to consider: 
%
% 'case1' means the same
% 'case2' means GA20ox different
% 'case3' means GA3ox different
% 'case4' means GA2ox different

coldparam='case4'
droughtparam='case2'


th_pos_c1=[1:mdl_m.p];

if coldparam=='case1'
    th_pos_c2=[1:nkinparams,mdl_m.p+1:mdl_m.p+ninputparam];
    if droughtparam=='case1'
        ndiffparams=0
        th_pos_c3=[1:nkinparams,mdl_m.p+ninputparam+1:mdl_m.p+2*ninputparam];
    elseif droughtparam=='case2'
        ndiffparams=3
        th_pos_c3=[mdl_m.p+2*ninputparam+1:mdl_m.p+2*ninputparam+ndiffparams,4:8,mdl_m.p+ninputparam+1:mdl_m.p+2*ninputparam];
    elseif droughtparam=='case3'
        ndiffparams=1
        th_pos_c3=[1:3,mdl_m.p+2*ninputparam+1,5:8,mdl_m.p+ninputparam+1:mdl_m.p+2*ninputparam];
    elseif droughtparam=='case4'
        ndiffparams=4
        th_pos_c3=[1:4,mdl_m.p+2*ninputparam+1:mdl_m.p+2*ninputparam+ndiffparams,mdl_m.p+ninputparam+1:mdl_m.p+2*ninputparam];
    end
    
elseif coldparam=='case2'    
    th_pos_c2=[mdl_m.p+2*ninputparam+1:mdl_m.p+2*ninputparam+3,4:8,mdl_m.p+1:mdl_m.p+ninputparam];
    if droughtparam=='case1'
        ndiffparams=3
        th_pos_c3=[1:nkinparams,mdl_m.p+ninputparam+1:mdl_m.p+2*ninputparam];
    elseif droughtparam=='case2'
        ndiffparams=6
        th_pos_c3=[mdl_m.p+2*ninputparam+4:mdl_m.p+2*ninputparam+ndiffparams,4:8,mdl_m.p+ninputparam+1:mdl_m.p+2*ninputparam];
    elseif droughtparam=='case3'
        ndiffparams=4
        th_pos_c3=[1:3,mdl_m.p+2*ninputparam+4,5:8,mdl_m.p+ninputparam+1:mdl_m.p+2*ninputparam];
    elseif droughtparam=='case4'
        ndiffparams=7
        th_pos_c3=[1:4,mdl_m.p+2*ninputparam+4:mdl_m.p+2*ninputparam+ndiffparams,mdl_m.p+ninputparam+1:mdl_m.p+2*ninputparam];
    end
    
elseif coldparam=='case3'    
    th_pos_c2=[1:3,mdl_m.p+2*ninputparam+1,5:8,mdl_m.p+1:mdl_m.p+ninputparam];
    if droughtparam=='case1'
        ndiffparams=1
        th_pos_c3=[1:nkinparams,mdl_m.p+ninputparam+1:mdl_m.p+2*ninputparam];
    elseif droughtparam=='case2'
        ndiffparams=4
        th_pos_c3=[mdl_m.p+2*ninputparam+2:mdl_m.p+2*ninputparam+4,4:8,mdl_m.p+ninputparam+1:mdl_m.p+2*ninputparam];
    elseif droughtparam=='case3'
        ndiffparams=2
        th_pos_c3=[1:3,mdl_m.p+2*ninputparam+2,5:8,mdl_m.p+ninputparam+1:mdl_m.p+2*ninputparam];
    elseif droughtparam=='case4'
        ndiffparams=5
        th_pos_c3=[1:4,mdl_m.p+2*ninputparam+2:mdl_m.p+2*ninputparam+5,mdl_m.p+ninputparam+1:mdl_m.p+2*ninputparam];
    end
    
elseif coldparam=='case4'    
    th_pos_c2=[1:4,mdl_m.p+2*ninputparam+1:mdl_m.p+2*ninputparam+4,mdl_m.p+1:mdl_m.p+ninputparam];
    if droughtparam=='case1'
        ndiffparams=4
        th_pos_c3=[1:nkinparams,mdl_m.p+ninputparam+1:mdl_m.p+2*ninputparam];
    elseif droughtparam=='case2'
        ndiffparams=7
        th_pos_c3=[mdl_m.p+2*ninputparam+5:mdl_m.p+2*ninputparam+7,4:8,mdl_m.p+ninputparam+1:mdl_m.p+2*ninputparam];
    elseif droughtparam=='case3'
        ndiffparams=5
        th_pos_c3=[1:3,mdl_m.p+2*ninputparam+5,5:8,mdl_m.p+ninputparam+1:mdl_m.p+2*ninputparam];
    elseif droughtparam=='case4'
        ndiffparams=8
        th_pos_c3=[1:4,mdl_m.p+2*ninputparam+5:mdl_m.p+2*ninputparam+8,mdl_m.p+ninputparam+1:mdl_m.p+2*ninputparam];
    end
    
        
end
nparams=nkinparams+3*ninputparam+ndiffparams;


%load data:
load ../data/drought_cold/data_PCRenz_GAconc_nM_threecases_final_Ainterp_allenzpts.mat


data_c1=data;
data_c2=data_cold;
data_c3=data_drought;

initialt=1000;
data_c1.initialt=initialt;
data_c2.initialt=initialt;
data_c3.initialt=initialt;
tvec=[initialt;data.pos_GAs];
ntime=length(data.pos_GAs);
ntime_enz=length(data.pos_enz);
nenz=3;
mt_c1=[tvec(tvec<data_c1.xm);data_c1.xm];
EZt_c1=[data_c1.xm;tvec(tvec>data_c1.xm)];

mt_c2=[tvec(tvec<data_c2.xm);data_c2.xm];
EZt_c2=[data_c2.xm;tvec(tvec>data_c2.xm)];

mt_c3=[tvec(tvec<data_c3.xm);data_c3.xm];
EZt_c3=[data_c3.xm;tvec(tvec>data_c3.xm)];

addpath ('../fdaM')

% load initial parameter estimates for b-spline representation of the input functions.
load ../fittedinputfunctions/fittedinputs_3cases_allenzpts_GA53nM_nbasis7_vector_norder3_evenbasis_lbm6_initialt1000.mat theta_fittedinputs_wt theta_fittedinputs_cold theta_fittedinputs_drought nbasis norder basis breaks rng
theta_fittedinputs=[theta_fittedinputs_wt,theta_fittedinputs_cold,theta_fittedinputs_drought];
    
if mdl_m.nbasis ~= nbasis
    warning('nbasis inconsistent')
end

basisvals_meas_tp=eval_basis(data.pos_GAs,basis);
basisvals_meas_tp_enz=eval_basis(data.pos_enz,basis);


nbasisA=6;
data_c1.kappa=1;
data_c2.kappa=1;
data_c3.kappa=1;
basisA = create_bspline_basis(rng, nbasisA,norder);

data_c1=calculateequationexpressions_innuc(data_c1,basisA,data_c1.initialt);
[cyt_conv_c1,par_data_c1]=convertdata_cytconc(data_c1);
plotGAs_data(data_c1.pos_GAs,data_c1.pos_enz,par_data_c1)


data_c2=calculateequationexpressions_innuc(data_c2,basisA,data_c2.initialt);
[cyt_conv_c2,par_data_c2]=convertdata_cytconc(data_c2);
plotGAs_data(data_c2.pos_GAs,data_c2.pos_enz,par_data_c2)

data_c3=calculateequationexpressions_innuc(data_c3,basisA,data_c3.initialt);
[cyt_conv_c3,par_data_c3]=convertdata_cytconc(data_c3);
plotGAs_data(data_c3.pos_GAs,data_c3.pos_enz,par_data_c3)

par_data=[par_data_c1;par_data_c2;par_data_c3];

% Calculate Jacobian matrix for the inputs:
J_GA53_c1=[zeros(ntime,nkinparams),full(basisvals_meas_tp),zeros(ntime,nbasis*nenz),zeros(ntime,2*ninputparam+ndiffparams)];
J_enz_c1=zeros(nenz*ntime_enz,nparams);
for i=1:nenz
    J_enz_c1(ntime_enz*(i-1)+1:ntime_enz*i,:)=[zeros(ntime_enz,nkinparams),zeros(ntime_enz,nbasis*i),full(basisvals_meas_tp_enz),zeros(ntime_enz,nbasis*(nenz-i)),zeros(ntime_enz,2*ninputparam+ndiffparams)];
end
Jinputs_c1=[J_GA53_c1;J_enz_c1];

J_GA53_c2=[zeros(ntime,nkinparams+ninputparam),full(basisvals_meas_tp),zeros(ntime,nbasis*nenz),zeros(ntime,ninputparam+ndiffparams)];
J_enz_c2=zeros(nenz*ntime_enz,nparams);
for i=1:nenz
    J_enz_c2(ntime_enz*(i-1)+1:ntime_enz*i,:)=[zeros(ntime_enz,nkinparams+ninputparam),zeros(ntime_enz,nbasis*i),full(basisvals_meas_tp_enz),zeros(ntime_enz,nbasis*(nenz-i)),zeros(ntime_enz,ninputparam+ndiffparams)];
end
Jinputs_c2=[J_GA53_c2;J_enz_c2];

J_GA53_c3=[zeros(ntime,nkinparams+2*ninputparam),full(basisvals_meas_tp),zeros(ntime,nbasis*nenz),zeros(ntime,ndiffparams)];
J_enz_c3=zeros(nenz*ntime_enz,nparams);
for i=1:nenz
    J_enz_c3(ntime_enz*(i-1)+1:ntime_enz*i,:)=[zeros(ntime_enz,nkinparams+2*ninputparam),zeros(ntime_enz,nbasis*i),full(basisvals_meas_tp_enz),zeros(ntime_enz,nbasis*(nenz-i)),zeros(ntime_enz,ndiffparams)];
end
Jinputs_c3=[J_GA53_c3;J_enz_c3];

% Calculate vector of maximum values for scaling:
maxvec_GAs_c1=zeros((mdl_m.v+1)*ntime,1);
maxvec_GAs_c2=zeros((mdl_m.v+1)*ntime,1);
maxvec_GAs_c3=zeros((mdl_m.v+1)*ntime,1);
for i=1:mdl_m.v+1
    maxvec_GAs_c1((i-1)*ntime+1:i*ntime)=max(par_data_c1((i-1)*ntime+1:i*ntime))*ones(ntime,1);
    maxvec_GAs_c2((i-1)*ntime+1:i*ntime)=max(par_data_c2((i-1)*ntime+1:i*ntime))*ones(ntime,1);
    maxvec_GAs_c3((i-1)*ntime+1:i*ntime)=max(par_data_c3((i-1)*ntime+1:i*ntime))*ones(ntime,1);
end
maxvec_enz_c1=zeros(nenz*ntime_enz,1);
maxvec_enz_c2=zeros(nenz*ntime_enz,1);
maxvec_enz_c3=zeros(nenz*ntime_enz,1);
for i=1:nenz
    maxvec_enz_c1((i-1)*ntime_enz+1:i*ntime_enz)=max(par_data_c1((mdl_m.v+1)*ntime+(i-1)*ntime_enz+1:(mdl_m.v+1)*ntime+i*ntime_enz))*ones(ntime_enz,1);
    maxvec_enz_c2((i-1)*ntime_enz+1:i*ntime_enz)=max(par_data_c2((mdl_m.v+1)*ntime+(i-1)*ntime_enz+1:(mdl_m.v+1)*ntime+i*ntime_enz))*ones(ntime_enz,1);
    maxvec_enz_c3((i-1)*ntime_enz+1:i*ntime_enz)=max(par_data_c3((mdl_m.v+1)*ntime+(i-1)*ntime_enz+1:(mdl_m.v+1)*ntime+i*ntime_enz))*ones(ntime_enz,1);
end
maxvec=[maxvec_GAs_c1;maxvec_enz_c1;maxvec_GAs_c2;maxvec_enz_c2;maxvec_GAs_c3;maxvec_enz_c3];
recipmaxvec=1./maxvec;

ndata=3*(7*(length(data.pos_GAs)+3*length(data.pos_enz)));

lb_kin=-6;
ub_kin=3;
lb_inputs=-6;
ub_inputs=4;
lb=[lb_kin*ones(nkinparams,1);lb_inputs*ones(3*ninputparam,1);lb_kin*ones(ndiffparams,1)];
ub=[ub_kin*ones(nkinparams,1);ub_inputs*ones(3*ninputparam,1);ub_kin*ones(ndiffparams,1)];

theta0_01=lhsdesign(nruns,nkinparams+ndiffparams);
theta0=zeros(nruns,nparams);
for j=1:nruns
     theta0(j,1:nkinparams)=(ub_kin-lb_kin)*theta0_01(j,1:nkinparams)+lb_kin;
     theta0(j,nkinparams+1:nkinparams+3*ninputparam)=theta_fittedinputs;
     if ndiffparams ~= 0
        theta0(j,nkinparams+3*ninputparam+1:end)=(ub_kin-lb_kin)*theta0_01(j,nkinparams+1:end)+lb_kin;
     end
end


theta=zeros(nruns,nparams);
fval=zeros(1,nruns);
exitflag=zeros(1,nruns);
telapsed_eachrun=zeros(1,nruns);

objfun=@(thetahat)objfun_Acpresc_nM_cyt_3cases(thetahat,par_data,mdl_m,mdl_EZ,pars_m,pars_EZ,ntime,ntime_enz,nparams,nkinparams,basisvals_meas_tp,basisvals_meas_tp_enz,Jinputs_c1,Jinputs_c2,Jinputs_c3,...
    recipmaxvec,mt_c1,mt_c2,mt_c3,EZt_c1,EZt_c2,EZt_c3,data_c1,data_c2,data_c3,initialt,breaks,norder,th_pos_c1,th_pos_c2,th_pos_c3);


options = optimset('Jacobian','on','Display', 'off');
%parpool('AttachedFiles',{'bsplineM.m'})

tstart = tic;
for i=1:nruns
    i
    [theta(i,:),fval(i),~,exitflag(i)] = lsqnonlin(objfun,theta0(i,:),lb,ub,options);
    telapsed_eachrun(i) = toc(tstart);
end;
telapsed = toc(tstart);
%delete(gcp('nocreate'))


[f_best,I]=min(fval);
theta_best=theta(I,:);

AIC=2*nparams+ndata*log(f_best/ndata)
AICc=2*nparams*ndata/(ndata-nparams-1)+ndata*log(f_best/ndata)

time_for_run(1)=telapsed_eachrun(1);
for j=2:nruns;
    time_for_run(j)=telapsed_eachrun(j)-telapsed_eachrun(j-1);
end


save ../parameter_estimates/semethod_3cases_coldGA2oxdroughtGA20oxdiff_nM_allenzpts_cyt_evenbasis_nbas7_Acpresc_lbm6_initialt1000_innuc_test.mat 
