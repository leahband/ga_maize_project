% Estimates model parameters for the B73 wildtype, control data, 
% assuming that all the inputs, GA53, GA20ox, GA3ox and GA2ox are constants.


function runsemthod_wt_Acpresc_nM_cyt_constantinputs

[mdl_m,pars_m] = makeModel_prescribedinput_Acpresc('Maizeleaf_constantinputs_Acpresc_innuc',1);
[mdl_EZ,pars_EZ] = makeModel_prescribedinput_Acpresc('Maizeleaf_constantinputs_EZ_Acpresc_innuc',1);

addpath ('../modelcreation_outputs')

nruns=10;

load ../data/drought_cold/data_PCRenz_GAconc_nM_threecases_final_Ainterp_allenzpts.mat

data.initialt=1000;
tvec=[data.initialt;data.pos_GAs];
pars_m.t=[tvec(tvec<data.xm);data.xm];
pars_EZ.t=[data.xm;tvec(tvec>data.xm)];

ntime=length(data.pos_GAs);
ntime_enz=length(data.pos_enz);
nbasis=6;
u=4;
nenz=3;
nparams=mdl_m.p;
nvars=mdl_m.v;
nkinparams=8;

addpath ('../fdaM')
rng   = [data.initialt,max(tvec)];
norder=3;
basisA = create_bspline_basis(rng, nbasis,norder);
data.kappa=1;
data=calculateequationexpressions_innuc(data,basisA,data.initialt);

[cyt_conv,par_data]=convertdata_cytconc(data);


dinputsdthetainputs=[[ones(ntime,1),zeros(ntime,3)];[zeros(nenz*ntime_enz,1),kron(eye(nenz),ones(ntime_enz,1))]];
J_inputs=[zeros(ntime+nenz*ntime_enz,nkinparams),dinputsdthetainputs];

% calculate vector of maximum values for scaling:
maxvec_GAs=zeros((mdl_m.v+1)*ntime,1);
for i=1:mdl_m.v+1
    maxvec_GAs((i-1)*ntime+1:i*ntime)=max(par_data((i-1)*ntime+1:i*ntime))*ones(ntime,1);
end
maxvec_enz=zeros(nenz*ntime_enz,1);
for i=1:nenz
    maxvec_enz((i-1)*ntime_enz+1:i*ntime_enz)=max(par_data(7*ntime+(i-1)*ntime_enz+1:(mdl_m.v+1)*ntime+i*ntime_enz))*ones(ntime_enz,1);
end
maxvec=[maxvec_GAs;maxvec_enz];
recipmaxvec=1./maxvec;

ndata=7*ntime+3*ntime_enz;

lb=[-6*ones(nkinparams,1);-1*ones(u,1)];
ub=[3*ones(nkinparams,1);4*ones(u,1)];

objfun=@(thetahat)objfun_Apres_nM_cyt_constantinputs(thetahat,par_data,mdl_m,mdl_EZ,pars_m,pars_EZ,ntime,ntime_enz,nparams,nvars,J_inputs,recipmaxvec,data);


options = optimset('Jacobian','on','Display', 'off','MaxFunEvals',200);
%options = optimset(options, 'UseParallel','Always');
%matlabpool open local 4

%parpool('AttachedFiles',{'bsplineM.m'})

theta0_01=lhsdesign(nruns,nkinparams);
theta0=zeros(nruns,nparams);
for j=1:nruns
     theta0(j,1:nkinparams)=theta0_01(j,:).*(ub(1:nkinparams)-lb(1:nkinparams))'+lb(1:nkinparams)';
     theta0(j,nkinparams+1:end)=[mean(cyt_conv.*data.GA53),mean(data.GA20ox),mean(data.GA3ox2),mean(data.GA2ox)];
end
theta0

theta=zeros(nruns,nparams);
fval=zeros(1,nruns);
exitflag=zeros(1,nruns);
telapsed_eachrun=zeros(1,nruns);

tstart = tic;
for i=1:nruns
    i
    [theta(i,:),fval(i),~,exitflag(i)] = lsqnonlin(objfun,theta0(i,:),lb,ub,options);
    telapsed_eachrun(i) = toc(tstart);
 end;
 telapsed = toc(tstart)
 

%delete(gcp('nocreate'))
[f_best,I]=min(fval)
theta_best=theta(I,:);
 
AIC=2*nparams+ndata*log(f_best/ndata)
AICc=2*nparams*ndata/(ndata-nparams-1)+ndata*log(f_best/ndata)

time_for_run(1)=telapsed_eachrun(1);
for j=2:nruns
    time_for_run(j)=telapsed_eachrun(j)-telapsed_eachrun(j-1);
end

save ../parameter_estimates/semethod_wt_nM_allenzpts_cyt_evenbasis_Acpresc_constantinputsfitted_lbm6_init1000_innuc_test.mat


