function mdl = doSymbolicCalcs_bsplines(modelName,f,p,v,u,w)

theta = sym(zeros(p,1));
x = sym(zeros(v,1));
t = sym('t');  %#ok<NASGU>

for z_=1:w
     syms(['z_' num2str(z_)])
     try
         z(z_) = eval(['z_' num2str(z_)]);
     catch  %#ok<*CTCH> % needed to account for maple syntax issue: see note (*)
         if z==1, theta = eval(['z_' num2str(z_)]); else error; end
     end
end

%g=[sym('g_1'),sym('g_2'),sym('g_3'),sym('g_4')];

for u_=1:u
     syms(['g_' num2str(u_)])
     try
         g(u_) = eval(['g_' num2str(u_)]);
     catch  %#ok<*CTCH> % needed to account for maple syntax issue: see note (*)
         if u==1, theta = eval(['g_' num2str(u_)]); else error; end
     end
end

for p_ = 1:p
    syms (['theta_' num2str(p_)])
    try
        theta(p_) = eval(['theta_' num2str(p_)]);
    catch  %#ok<*CTCH> % needed to account for maple syntax issue: see note (*)
        if p==1, theta = eval(['theta_' num2str(p_)]); else error; end
    end
end

for v_ = 1:v
    syms (['x_' num2str(v_)])
    try
        x(v_) = eval(['x_' num2str(v_)]);
    catch  % needed to account for maple syntax issue: see note (*)
        if v==1, x = eval(['x_' num2str(v_)]); else error; end
    end
end

% note (*): changes to the symbolic/Maple toolbox mean that assignment to
% theta(1) when theta is a scalar now returns an error - hence the need
% for the try/catch statements

f = eval(f);
outputToFile(f,'f',modelName,p,v);

% calculate dfdx
dfdx = sym(zeros(v,v));
for v1_ = 1:v
    for v2_ = 1:v
        try
            dfdx(v1_,v2_) = diff(f(v1_),x(v2_));
        catch
            if v==1, dfdx = diff(f(v1_),x(v2_)); else error; end
        end
    end
end
outputToFile(dfdx,'dfdx',modelName,p,v);

% calculate dfdg
dfdg = sym(zeros(v,u));
for v_ = 1:v
    for u_ = 1:u
        try
            dfdg(v_,u_) = diff(f(v_),g(u_));
        catch
            if v==1 && u==1, dfdg = diff(f(v_),x(u_)); else error; end
        end
    end
end
outputToFile(dfdg,'dfdg',modelName,p,v);

% calculate dfdtheta
dfdtheta = sym(zeros(v,p));
for v_ = 1:v
    for p_ = 1:p
        dfdtheta(v_,p_) = diff(f(v_),theta(p_));
    end
end
outputToFile(dfdtheta,'dfdtheta',modelName,p,v);

% calculate d2fdtheta2
d2fdtheta2 = sym(zeros(v,p,p));
%size(d2fdtheta2)
for v_ = 1:v
    for p1_ = 1:p
        for p2_ = 1:p
            d2fdtheta2(v_,p1_,p2_) = diff(dfdtheta(v_,p1_),theta(p2_));
        end
    end
end
outputToFile(d2fdtheta2,'d2fdtheta2',modelName,p,v);

% if all elements of d2fdtheta2 are zero it means dfdtheta = const w.r.t
% theta and thus that model is linear in theta (i.e. f = dfdtheta * theta + b);
% if so then calculate the vector b
if all(d2fdtheta2(:) == 0)
    b = sym(zeros(v,1));
    b = simplify(f - dfdtheta*theta);
    outputToFile(b,'b',modelName,p,v)
end

mdl = modl(modelName,p,v);

% ---------------------------------------------------------------------

function [] = outputToFile(in,funcName,modelName,p,v,filepath)

if nargin<6,
    filepath = '../modelcreation_outputs/';
    if ~exist(filepath)
        filepath = '../modelcreation_outputs/';
        if ~exist(filepath)
            error('Please hard-code into ''doSymbolicCalcs.m'' the path where model functions should be written')
        end
    end
end

filename = [modelName '_' funcName '.m'];
fid = fopen([filepath modelName '_' funcName '.m'], 'wt');

siz = size(in);

%if strcmp(funcName,'f'); % only calculate g in f file.
%    fprintf(fid,['function out = ' modelName '_' funcName '(t,x,theta,mdl)  %%#ok<*INUSD,*INUSL> \n\n']);
%else
%     fprintf(fid,['function out = ' modelName '_' funcName '(t,x,theta)  %%#ok<*INUSD,*INUSL> \n\n']);
%end
fprintf(fid,['function out = ' modelName '_' funcName '(t,x,theta,g,z)  %%#ok<*INUSD,*INUSL> \n\n']);


%fprintf(fid,'z=Maizegrowth_wildtype(t);');
%fprintf(fid,'global g;');
%if strcmp(funcName,'f'); % only calculate g in f file.
%    fprintf(fid,'g=inputfuncs_bsplines(t,theta,mdl);');
%end

dim = length(siz);

switch dim
    case 2
        fprintf(fid,['out = zeros(' num2str(siz(1)) ',' num2str(siz(2)) ');\n\n']);
        for i = find(~(in(:)==0))'
            [a,b] = ind2sub(siz,i);
            fprintf(fid,['out(' num2str(a) ',' num2str(b) ') = ' char(in(i)) ';\n']);
        end
    case 3
        fprintf(fid,['out = zeros(' num2str(siz(1)) ',' num2str(siz(2)) ',' num2str(siz(3)) ');\n\n']);
        for i = find(~(in(:)==0))'
            [a,b,c] = ind2sub(siz,i);
            fprintf(fid,['out(' num2str(a) ',' num2str(b) ',' num2str(c) ') = ' char(in(i)) ';\n']);
        end
    case 4
        fprintf(fid,['out = zeros(' num2str(siz(1)) ',' num2str(siz(2)) ',' num2str(siz(3)) ',' num2str(siz(4)) ');\n\n']);
        for i = find(~(in(:)==0))'
            [a,b,c,d] = ind2sub(siz,i);
            fprintf(fid,['out(' num2str(a) ',' num2str(b) ',' num2str(c) ',' num2str(d) ') = ' char(in(i)) ';\n']);
        end
end

% alter the file so that the indexing is in appropriate notation,
% i.e. change from x_1 to x(1), etc.

% original linux version:
%for k = 1:max(p,v)
%    eval(['!sed -i ''s/_' num2str(k) '/(' num2str(k) ')/g'' ' filepath filename]);
%end

% windows (or linux) version
fid = fopen([filepath filename],'r'); % open with reading permission
C = textscan(fid, '%s','delimiter','\n');
C = C{1,1};

for k = fliplr(1:max(p,v))  % the fliplr is needed to avoid problems when suffices have multiple digits; otherwise, e.g., _13 is converted into (1)3
    C = strrep(C,['_' num2str(k)],['(' num2str(k) ')']);
end
%C = strrep(C,'z(','z(t,'); % Makes data input functions time dependent (assuming number of data types < max (p,v)).

fid = fopen([filepath filename],'w+'); % open with writing permission, discarding previous contents


for k = 1:length(C)
    fprintf(fid,'%s\n',C{k});
end


fclose(fid);



