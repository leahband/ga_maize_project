
function [f,J]=objfun_Acpresc_nM_cyt_2cases(thetahat,par_data,mdl_m,mdl_EZ,pars_m,pars_EZ,ntime,ntime_enz,nparams,nvars,u,nkinparams,nbasis,...
    basisvals_meas_tp,basisvals_meas_tp_enz,J_inputs_case1,J_inputs_case2,recipmaxvec,data_case1,data_case2,breaks,...
    norder,mt_case1,EZt_case1,mt_case2,EZt_case2)


% Solutions for case one, where inputs are theta(1:36):
pars_m.theta=10.^thetahat(1:mdl_m.p);
pars_EZ.theta=pars_m.theta;

pars_m.t=mt_case1;
pars_EZ.t=EZt_case1;
data=data_case1;
th_inputfunc_reshape=reshape(pars_m.theta(nkinparams+1:end),nbasis,u);
pars_m.x0=v0_initialconditions(pars_m.theta,th_inputfunc_reshape,data.initialt,breaks,norder,data);
[sol_m_case1,~,dxdtheta_m_case1,~] = mdl_m.solveWithSensEqns_bsplines(pars_m,data,breaks,norder);
pars_EZ.x0=sol_m_case1.x(:,end);
[sol_EZ_case1,~,dxdtheta_EZ_case1,~] = mdl_EZ.solveWithSensEqns_bsplines(pars_EZ,data,breaks,norder);

sol_input_GA=basisvals_meas_tp*th_inputfunc_reshape(:,1);
sol_input_enz=basisvals_meas_tp_enz*th_inputfunc_reshape(:,2:4);

sol_GAs_nM=[sol_m_case1.x(:,2:end-1),sol_EZ_case1.x(:,2:end);sol_input_GA']; % Note now skip the first time point entry, as this in the initial condition.

ntGAs=(nvars+1)*ntime;
sol_GAs_vec_case1=reshape(sol_GAs_nM',ntGAs,1); 
sol_enz_vec_case1=reshape(sol_input_enz,3*ntime_enz,1);

% Solutions for case two:
pars_m.theta=10.^thetahat([1:nkinparams,mdl_m.p+1:end]);
pars_EZ.theta=pars_m.theta;
pars_m.t=mt_case2;
pars_EZ.t=EZt_case2;
data=data_case2;
th_inputfunc_reshape=reshape(pars_m.theta(nkinparams+1:end),nbasis,u);
pars_m.x0=v0_initialconditions(pars_m.theta,th_inputfunc_reshape,data.initialt,breaks,norder,data);
[sol_m_case2,~,dxdtheta_m_case2,~] = mdl_m.solveWithSensEqns_bsplines(pars_m,data,breaks,norder);
pars_EZ.x0=sol_m_case2.x(:,end);
[sol_EZ_case2,~,dxdtheta_EZ_case2,~] = mdl_EZ.solveWithSensEqns_bsplines(pars_EZ,data,breaks,norder);

sol_input_GA=basisvals_meas_tp*th_inputfunc_reshape(:,1);
sol_input_enz=basisvals_meas_tp_enz*th_inputfunc_reshape(:,2:4);

sol_GAs_nM=[sol_m_case2.x(:,2:end-1),sol_EZ_case2.x(:,2:end);sol_input_GA']; % Note now skip the first time point entry, as this in the initial condition.

sol_GAs_vec_case2=reshape(sol_GAs_nM',ntGAs,1); 
sol_enz_vec_case2=reshape(sol_input_enz,3*ntime_enz,1);

% Calculate the objective function value, f: 
f=recipmaxvec.*([sol_GAs_vec_case1;sol_enz_vec_case1;sol_GAs_vec_case2;sol_enz_vec_case2]-par_data); %The function whose sum of squares is minimized.

% Form dxdtheta to include both regions and input functions:
dxdtheta_case1_case1param=cat(3,dxdtheta_m_case1(:,:,2:end-1),dxdtheta_EZ_case1(:,:,2:end));
dxdtheta_case1=cat(2,dxdtheta_case1_case1param,zeros(mdl_m.v,nbasis*u,ntime));
z_case1=permute(dxdtheta_case1,[3 1 2]);
J_vars_case1=reshape(z_case1,[nvars*ntime,nparams]);
J_case1=[J_vars_case1;J_inputs_case1];

dxdtheta_case2_case2param=cat(3,dxdtheta_m_case2(:,:,2:end-1),dxdtheta_EZ_case2(:,:,2:end));
dxdtheta_case2=cat(2,dxdtheta_case2_case2param(:,1:nkinparams,:),zeros(mdl_m.v,nbasis*u,ntime),dxdtheta_case2_case2param(:,nkinparams+1:end,:));
z_case2=permute(dxdtheta_case2,[3 1 2]);
J_vars_case2=reshape(z_case2,[nvars*ntime,nparams]);
J_case2=[J_vars_case2;J_inputs_case2];

J=[J_case1;J_case2];


% Scale J due to model predicting cytoplasmic conc and objective function
% scaling with max of each dataset:
overallscale=repmat(recipmaxvec,1,nparams);  
J=overallscale.*J;

% Scale J as working in log parameter space:
multmat=log(10)*repmat(10.^thetahat,[2*(ntGAs+ntime_enz*3) 1]);
J=multmat.*J;





    









