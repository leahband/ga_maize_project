classdef params
    %PARAMS  Class of parameters for ODE models defined via class "model"
    %   Contains the model parameters, initial conditions, and times to
    %   evaluate the solution.
    
    properties
        theta
        x0
        t
    end
    
    methods
        function out = params(model,theta,x0,t)  % generator
            if nargin == 0;
                out.theta = [];
                out.x0 = [];
                out.t = [];
            else
                theta = colvec(theta);
                x0 = colvec(x0);
                if length(theta) ~= model.p, error('Need theta to have length p'); end
                if length(x0) ~= model.v, error('Wrong x0 to have length v'); end
                out.theta = theta;
                out.x0 = x0;
                if numel(t) == 1  % allow user to specify vector t or just its end value
                    out.t = linspace(0,t,100);
                else
                    out.t = t;
                end
            end
        end
    end
    
end

