% Estimates model parameters for the B73 wildtype, control data, 
% with input functions prescribed using pre-fitted b-spline functions.


% Parameter estimates are used in plotting Figure S9.



function runsemthod_wt_Acpresc_nM_cyt_prescribedinputs

[mdl_m,pars_m] = makeModel_prescribedinput_Acpresc('Maizeleaf_prescinputs_Acpresc_innuc',1);

[mdl_EZ,pars_EZ] = makeModel_prescribedinput_Acpresc('Maizeleaf_prescinputs_EZ_Acpresc_innuc',1);

addpath ('../modelcreation_outputs')

nruns=10;

%load data:
load ../data/drought_cold/data_PCRenz_GAconc_nM_threecases_final_Ainterp_allenzpts.mat


data.initialt=1000;
tvec=[data.initialt;data.pos_GAs];
pars_m.t=[tvec(tvec<data.xm);data.xm];
pars_EZ.t=[data.xm;tvec(tvec>data.xm)];

ntime=length(data.pos_GAs);
ntime_enz=length(data.pos_enz);
u=4;
nparams=mdl_m.p;
nvars=mdl_m.v;
nkinparams=mdl_m.p;

addpath ('../fdaM')

load ../fittedinputfunctions/fittedinputs_3cases_allenzpts_GA53nM_nbasis7_vector_norder3_evenbasis_lbm6_initialt1000.mat theta_fittedinputs_wt nbasis norder basis breaks rng
theta_fittedinputs_reshape=reshape(theta_fittedinputs_wt,nbasis,u);

nbasisA=6;
data.kappa=1;
basisA = create_bspline_basis(rng, nbasisA,norder);
data=calculateequationexpressions_innuc(data,basisA,data.initialt);


% Convert data to cytoplasmic concentrations
[cyt_conv,~]=convertdata_cytconc(data);
par_data=[cyt_conv.*data.GA44;cyt_conv.*data.GA19;cyt_conv.*data.GA20;cyt_conv.*data.GA1;cyt_conv.*data.GA29;cyt_conv.*data.GA8];

% calculate vector of maximum values for scaling:
maxvec_GAs=zeros(mdl_m.v*ntime,1);
for i=1:mdl_m.v
    maxvec_GAs((i-1)*ntime+1:i*ntime)=max(par_data((i-1)*ntime+1:i*ntime))*ones(ntime,1);
end
recipmaxvec=1./maxvec_GAs;

ndata=7*ntime+3*ntime_enz;

lb=[-6*ones(nkinparams,1)];
ub=[3*ones(nkinparams,1)];



objfun=@(thetahat)objfun_Apres_nM_cyt_prescribedinputs(thetahat,par_data,mdl_m,mdl_EZ,pars_m,pars_EZ,ntime,nparams,nvars,recipmaxvec,data,breaks,norder,theta_fittedinputs_reshape);


options = optimset('Jacobian','on','Display', 'off','MaxFunEvals',200);%'DerivativeCheck','on');%,'Diagnostics','on');
%options = optimset(options, 'UseParallel','Always');
%matlabpool open local 4

%parpool('AttachedFiles',{'bsplineM.m'})

theta0_01=lhsdesign(nruns,nkinparams);
theta0=zeros(nruns,nkinparams);
for j=1:nruns
     theta0(j,:)=theta0_01(j,:).*(ub-lb)'+lb';
end
theta0

theta=zeros(nruns,nkinparams);
fval=zeros(1,nruns);
exitflag=zeros(1,nruns);
telapsed_eachrun=zeros(1,nruns);
tstart = tic;
for i=1:nruns
    i
    [theta(i,:),fval(i),~,exitflag(i)] = lsqnonlin(objfun,theta0(i,:),lb,ub,options);
    telapsed_eachrun(i) = toc(tstart);
end
telapsed = toc(tstart)
%delete(gcp('nocreate'))

[f_best,I]=min(fval)
theta_best=theta(I,:);
 
% Calculate contribution to f from input functions, and recalculate AICc:
GA53coeffs=10.^theta_fittedinputs_reshape(:,1);
GA20oxcoeffs=10.^theta_fittedinputs_reshape(:,2);
GA3oxcoeffs=10.^theta_fittedinputs_reshape(:,3);
GA2oxcoeffs=10.^theta_fittedinputs_reshape(:,4);

basisvals_posGAs=eval_basis(data.pos_GAs,basis);
basisvals_posenz=eval_basis(data.pos_enz,basis);

GA53_fit=basisvals_posGAs*GA53coeffs;
GA20oxmRNA_fit=basisvals_posenz*GA20oxcoeffs;
GA3oxmRNA_fit=basisvals_posenz*GA3oxcoeffs;
GA2oxmRNA_fit=basisvals_posenz*GA2oxcoeffs;

f_vec_inputs=[1/max(data.GA53).*[GA53_fit-cyt_conv.*data.GA53];
                1/max(data.GA20ox).*[GA20oxmRNA_fit-data.GA20ox];
                1/max(data.GA3ox2).*[GA3oxmRNA_fit-data.GA3ox2];
                1/max(data.GA2ox).*[GA2oxmRNA_fit-data.GA2ox]];
      
f_inputs=sum(f_vec_inputs.^2)

f_total=f_best+f_inputs

nparams_tot=nparams+u*nbasis

AIC=2*nparams_tot+ndata*log(f_total/ndata)
AICc=2*nparams_tot*ndata/(ndata-nparams_tot-1)+ndata*log(f_total/ndata)



time_for_run(1)=telapsed_eachrun(1);
for j=2:nruns
    time_for_run(j)=telapsed_eachrun(j)-telapsed_eachrun(j-1);
end

save ../parameter_estimates/semethod_wt_nM_allenzpts_cyt_evenbasis_nbas7_Acpresc_prescinputs_lbm6_init1000_innuc_test.mat

