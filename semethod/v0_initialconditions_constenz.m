function x0=v0_initialconditions_constenz(theta,th_inputfunc,initialt,breaks,norder,data)

lambda53=theta(1);lambda44=theta(2);lambda19=theta(3);lambda20=theta(4);gamma20=theta(5);gamma1=theta(6);mu29=theta(7);mu8=theta(8);

basisvals = bsplineM(initialt, breaks, norder,0,0);
GA53init=basisvals*th_inputfunc;

GA20oxmRNA=theta(9);
GA3oxmRNA=theta(10);
GA2oxmRNA=theta(11);
 
% dilution=0 solution:
%  GA44init=lambda53/lambda44*GA53init;
%  GA19init=lambda53/lambda19*GA53init;
%  GA20init=(lambda19*GA19init.*GA20oxmRNA./(lambda20*GA3oxmRNA+gamma20*GA2oxmRNA));
%  GA1init=(lambda20*GA20init.*GA3oxmRNA./(gamma1*GA2oxmRNA));
%  GA29init=(gamma20/mu29*GA20init);
%  GA8init=(gamma1/mu8*GA1init);
%  

dilution=data.A_smooth(1)*data.cell_length(1)*data.RER(1)/(data.A_smooth(1)*data.cell_length(1)-data.Vnuc);
% dilution nonzero solution:
 GA44init=lambda53*GA20oxmRNA*GA53init/(lambda44*GA20oxmRNA+dilution);
 GA19init=lambda44*GA20oxmRNA*GA44init/(lambda19*GA20oxmRNA+dilution);
 GA20init=lambda19*GA19init*GA20oxmRNA/(lambda20*GA3oxmRNA+gamma20*GA2oxmRNA+dilution);
 GA1init=lambda20*GA20init*GA3oxmRNA/(gamma1*GA2oxmRNA+dilution);
 GA29init=gamma20*GA20init*GA2oxmRNA/(mu29*GA2oxmRNA+dilution);
 GA8init=gamma1*GA1init*GA2oxmRNA/(mu8*GA2oxmRNA+dilution);
 
 x0=[GA44init;GA19init;GA20init;GA1init;GA29init;GA8init];