% Estimates model parameters for the B104 wildtype, control data.
% Parameter estimates are used in plotting Figure S7.


function runsemthod_wt_Acpresc_nM_cyt_B104wt

% Construct model:
[mdl_m,pars_m] = makeModel_bsplinesandprescribedinput_Acpresc('Maizeleaf_enzfit_bspline_Acpresc_innuc',1);
[mdl_EZ,pars_EZ] = makeModel_bsplinesandprescribedinput_Acpresc('Maizeleaf_enzfit_bspline_EZ_Acpresc_innuc',1);

addpath ('../modelcreation_outputs')

nruns=10;

% Load data:
load ../data/GA20oxOE/data_GA20oxOE_GAconc_nM_GA20oxAdata.mat

data.initialt=1000;
tvec=[data.initialt;data.pos_GAs];
pars_m.t=[tvec(tvec<data.xm);data.xm];
pars_EZ.t=[data.xm;tvec(tvec>data.xm)];

ntime=length(data.pos_GAs);
ntime_enz=length(data.pos_enz);
pos_enz=data.pos_enz;
u=mdl_m.u;
nenz=3;
nvars=mdl_m.v;
nkinparams=mdl_m.nkin;
nparams=mdl_m.p;

addpath ('../fdaM')

% load initial parameter estimates for b-spline representation of the input functions.
load ../fittedinputfunctions/fittedinputs_B104wt_GA53nM_nbasis7_vector_norder3_evenbasis_innuc_initialt1000_B104Adata.mat theta_fittedinputs nbasis norder basis breaks rng

basisvals_meas_tp=eval_basis(data.pos_GAs,basis);
basisvals_meas_tp_enz=eval_basis(pos_enz,basis);

nbasisA=6;
data.kappa=1;
basisA = create_bspline_basis(rng, nbasisA,norder);
data=calculateequationexpressions_innuc(data,basisA,data.initialt);

% Convert data to cytoplasmic concentrations, assuming GA in nucleus (see dataconversion_wcompartments for alternative assumptions):
[cyt_conv,par_data]=convertdata_cytconc(data);
plotGAs_data(data.pos_GAs,data.pos_enz,par_data)

% Calculate Jacobian matrix for the inputs:
J_GA53=[zeros(ntime,nkinparams),full(basisvals_meas_tp),zeros(ntime,nbasis*(u-1))];
J_enz=zeros(nenz*ntime_enz,nparams);
for i=1:nenz
    J_enz(ntime_enz*(i-1)+1:ntime_enz*i,:)=[zeros(ntime_enz,nkinparams),zeros(ntime_enz,nbasis*i),full(basisvals_meas_tp_enz),zeros(ntime_enz,nbasis*(u-1-i))];
end
J_inputs=[J_GA53;J_enz];

% calculate vector of maximum values for scaling:
maxvec_GAs=zeros((mdl_m.v+1)*ntime,1);
for i=1:mdl_m.v+1
    maxvec_GAs((i-1)*ntime+1:i*ntime)=max(par_data((i-1)*ntime+1:i*ntime))*ones(ntime,1);
end
maxvec_enz=zeros(nenz*ntime_enz,1);
for i=1:nenz
    maxvec_enz((i-1)*ntime_enz+1:i*ntime_enz)=max(par_data(7*ntime+(i-1)*ntime_enz+1:(mdl_m.v+1)*ntime+i*ntime_enz))*ones(ntime_enz,1);
end
maxvec=[maxvec_GAs;maxvec_enz];
recipmaxvec=1./maxvec;

ndata=7*ntime+3*ntime_enz;


lb=[-6*ones(nparams,1)];
ub=[3*ones(nkinparams,1);4*ones(nbasis*u,1)];

objfun=@(thetahat)objfun_Acpresc_nM_cyt(thetahat,par_data,mdl_m,mdl_EZ,pars_m,pars_EZ,ntime,ntime_enz,nparams,nvars,u,nkinparams,nbasis,basisvals_meas_tp,basisvals_meas_tp_enz,J_inputs,recipmaxvec,data,breaks,norder);

options = optimset('Jacobian','on','Display', 'off');
%options = optimset(options, 'UseParallel','Always');
%matlabpool open local 4

%parpool('AttachedFiles',{'bsplineM.m'})

theta0_01=lhsdesign(nruns,nkinparams);
theta0=zeros(nruns,nparams);
for j=1:nruns
     theta0(j,1:nkinparams)=theta0_01(j,:).*(ub(1:nkinparams)-lb(1:nkinparams))'+lb(1:nkinparams)';
     theta0(j,nkinparams+1:end)=theta_fittedinputs;
end


theta=zeros(nruns,nparams);
fval=zeros(1,nruns);
exitflag=zeros(1,nruns);
telapsed_eachrun=zeros(1,nruns);

tstart = tic;
for i=1:nruns
    i
    [theta(i,:),fval(i),~,exitflag(i)] = lsqnonlin(objfun,theta0(i,:),lb,ub,options);
    telapsed_eachrun(i) = toc(tstart);
end
telapsed = toc(tstart);
%delete(gcp('nocreate'))

[f_best,I]=min(fval)
theta_best=theta(I,:);

AIC=2*nparams+ndata*log(f_best/ndata)
AICc=2*nparams*ndata/(ndata-nparams-1)+ndata*log(f_best/ndata)

time_for_run(1)=telapsed_eachrun(1);
for j=2:nruns
    time_for_run(j)=telapsed_eachrun(j)-telapsed_eachrun(j-1);
end

save ../parameter_estimates/semethod_B104wt_nM_cyt_evenbasis_nbas7_Acpresc_lbm6_innuc_init1000_B104wtAdata_test.mat

