function plotGAs_data_twocases(xvec_GAs,par_data1,par_data2)
ntime=length(xvec_GAs);

GA44data1=par_data1(1:ntime);
GA19data1=par_data1(ntime+1:2*ntime);
GA20data1=par_data1(2*ntime+1:3*ntime);
GA1data1=par_data1(3*ntime+1:4*ntime);
GA29data1=par_data1(4*ntime+1:5*ntime);
GA8data1=par_data1(5*ntime+1:6*ntime);
GA53data1=par_data1(6*ntime+1:7*ntime);


GA44data2=par_data2(1:ntime);
GA19data2=par_data2(ntime+1:2*ntime);
GA20data2=par_data2(2*ntime+1:3*ntime);
GA1data2=par_data2(3*ntime+1:4*ntime);
GA29data2=par_data2(4*ntime+1:5*ntime);
GA8data2=par_data2(5*ntime+1:6*ntime);
GA53data2=par_data2(6*ntime+1:7*ntime);


figure

subplot(2,4,1)
hold on
plot(1/1000*xvec_GAs(1:end),GA53data1(1:end),':x','LineWidth',2)
plot(1/1000*xvec_GAs(1:end),GA53data2(1:end),':xr','LineWidth',2)
title('GA53')
xlabel('Dist (mm)','Interpreter','latex','FontSize',16)
ylabel('Conc (nM)','Interpreter','latex','FontSize',16)
box off

subplot(2,4,2)
hold on
plot(1/1000*xvec_GAs(1:end),GA44data1(1:end),':x','LineWidth',2)
plot(1/1000*xvec_GAs(1:end),GA44data2(1:end),':xr','LineWidth',2)
title('GA44')
xlabel('Dist (mm)','Interpreter','latex','FontSize',16)
ylabel('Conc (nM)','Interpreter','latex','FontSize',16)
box off

subplot(2,4,3)
hold on
plot(1/1000*xvec_GAs(1:end),GA19data1(1:end),':x','LineWidth',2)
plot(1/1000*xvec_GAs(1:end),GA19data2(1:end),':xr','LineWidth',2)
title('GA19')
xlabel('Dist (mm)','Interpreter','latex','FontSize',16)
ylabel('Conc (nM)','Interpreter','latex','FontSize',16)
box off

subplot(2,4,4)
hold on
plot(1/1000*xvec_GAs(1:end),GA20data1(1:end),':x','LineWidth',2)
plot(1/1000*xvec_GAs(1:end),GA20data2(1:end),':xr','LineWidth',2)
title('GA20')
xlabel('Dist (mm)','Interpreter','latex','FontSize',16)
ylabel('Conc (nM)','Interpreter','latex','FontSize',16)
box off

subplot(2,4,5)
hold on
plot(1/1000*xvec_GAs(1:end),GA1data1(1:end),':x','LineWidth',2)
plot(1/1000*xvec_GAs(1:end),GA1data2(1:end),':xr','LineWidth',2)
title('GA1')
xlabel('Dist (mm)','Interpreter','latex','FontSize',16)
ylabel('Conc (nM)','Interpreter','latex','FontSize',16)
box off

subplot(2,4,6)
hold on
plot(1/1000*xvec_GAs(1:end),GA29data1(1:end),':x','LineWidth',2)
plot(1/1000*xvec_GAs(1:end),GA29data2(1:end),':xr','LineWidth',2)
title('GA29')
xlabel('Dist (mm)','Interpreter','latex','FontSize',16)
ylabel('Conc (nM)','Interpreter','latex','FontSize',16)
box off

subplot(2,4,7)
hold on
plot(1/1000*xvec_GAs(1:end),GA8data1(1:end),':x','LineWidth',2)
plot(1/1000*xvec_GAs(1:end),GA8data2(1:end),':xr','LineWidth',2)
title('GA8')
xlabel('Dist (mm)','Interpreter','latex','FontSize',16)
ylabel('Conc (nM)','Interpreter','latex','FontSize',16)
box off
%legend1=legend('GA44','GA19','GA20','GA1','GA29','GA8')
%set(legend1,'Interpreter','latex','Location','SouthEast','FontSize',24)
%box('off')
%set(gca,'FontSize',20)
