
function [f,J,sol_m,sol_EZ]=objfun_Apres_nM_cyt_constantinputs(thetahat,par_data,mdl_m,mdl_EZ,pars_m,pars_EZ,ntime,ntime_enz,nparams,nvars,J_inputs,recipmaxvec,data)

pars_m.theta=10.^thetahat;
pars_EZ.theta=pars_m.theta;

pars_m.x0=v0_initialconditions_constantinputs(pars_m.theta,data);

% Solve for x and dxdtheta using sensitivity equations:
[sol_m,~,dxdtheta_m,~] = mdl_m.solveWithSensEqns_constantinputs(pars_m,data);
pars_EZ.x0=sol_m.x(:,end);
[sol_EZ,~,dxdtheta_EZ,~] = mdl_EZ.solveWithSensEqns_constantinputs(pars_EZ,data);

sol_GAs=[sol_m.x(:,2:end-1),sol_EZ.x(:,2:end)]; % Note skip the first time point entry, as this in the initial condition.
ntGAs=nvars*ntime;
sol_GAs_vec=reshape(sol_GAs',ntGAs,1); 
sol=[sol_GAs_vec;pars_m.theta(9)*ones(ntime,1);pars_m.theta(10)*ones(ntime_enz,1);pars_m.theta(11)*ones(ntime_enz,1);pars_m.theta(12)*ones(ntime_enz,1)];

% Calculate the objective function value, f: 
f=recipmaxvec.*(sol-par_data); % The function whose sum of squares is minimized.

% Calculate the Jacobian:
% Form dxdtheta to include both regions and input functions:
dxdtheta=cat(3,dxdtheta_m(:,:,2:end-1),dxdtheta_EZ(:,:,2:end));
% rearranges the elements of dxdtheta to form J
z=permute(dxdtheta,[3 1 2]);
J_vars=reshape(z,[ntGAs,nparams]);
J=[J_vars;J_inputs];

% Scale J due to model predicting cytoplasmic conc and objective function
% scaling with max of each dataset:
overallscale=repmat(recipmaxvec,1,nparams);  
J=overallscale.*J;

% Scale J as working in log parameter space:
multmat=log(10)*repmat(pars_m.theta,[(ntGAs+ntime+ntime_enz*3) 1]);
J=multmat.*J;





    









