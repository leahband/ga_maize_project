function [mdl,pars,obsData] = makeModel_bsplinesandprescribedinput_Acpresc(modelName,symbCalcFlag)


% For cytoplasmic expansion case, introduce
% z(11): gamma=alpha/X in EZ cytoplasmic expansion case

% bslpine input functions:
% g(1) : GA53
% g(2) : GA20ox
% g(3) : GA3ox
% g(4) : GA2ox


% theta(1):  lambda_53
% theta(2): lambda_44
% theta(3):  lambda_19
% theta(4):  lambda_20
% theta(5)  gamma_20
% theta(6):   gamma_1
% theta(7):  gamma_29
% theta (8):   gamma_8

switch modelName
        
    case 'Maizeleaf_enzfit_bspline_Acpresc_innuc'
        v=6;
        u=4; % No of input funcs from bsplines
        w=6;  % No of input funcs prescribed from data
        nbasis=7;  % No of basis used to make input functions   
        nkin=8;
        p=nbasis*u+nkin;
        
        f{1}=['[1/z(1)*(z(2)*(theta(1)*g(1)*g(2)-theta(2)*x(1)*g(2))-x(1)*z(3));'];
        f{2}=['1/z(1)*(z(2)*(theta(2)*x(1)*g(2)-theta(3)*x(2)*g(2))-x(2)*z(3));'];
        f{3}=['1/z(1)*(z(2)*(theta(3)*x(2)*g(2)-theta(4)*x(3)*g(3)-theta(5)*x(3)*g(4))-x(3)*z(3));'];
        f{4}=['1/z(1)*(z(2)*(theta(4)*x(3)*g(3)-theta(6)*x(4)*g(4))-x(4)*z(3));'];
        f{5}=['1/z(1)*(z(2)*(theta(5)*x(3)*g(4)-theta(7)*x(5)*g(4))-x(5)*z(3));'];
        f{6}=['1/z(1)*(z(2)*(theta(6)*x(4)*g(4)-theta(8)*x(6)*g(4))-x(6)*z(3))]'];
        
        f = strcat(f{:})
        theta = ones(1,p); % Arbitrary
        x0 = ones(1,v);
        t =[2500,7500,12500,17500,25000,35000,45000,55000,65000,75000,85000,95000]
        
        
    case 'Maizeleaf_enzfit_bspline_EZ_Acpresc_innuc'
        v=6;
        u=4; % No of input funcs from bsplines
        w=6;  % No of input funcs prescribed from data
        nbasis=7;  % No of basis used to make input functions 
        nkin=8;
        p=nbasis*u+nkin;
        
        f{1}=['[1/(z(1)*z(4))*(z(5)*(theta(1)*g(1)*g(2)-theta(2)*x(1)*g(2))-z(6)*x(1));'];
        f{2}=['1/(z(1)*z(4))*(z(5)*(theta(2)*x(1)*g(2)-theta(3)*x(2)*g(2))-z(6)*x(2));'];
        f{3}=['1/(z(1)*z(4))*(z(5)*(theta(3)*x(2)*g(2)-theta(4)*x(3)*g(3)-theta(5)*x(3)*g(4))-z(6)*x(3));'];
        f{4}=['1/(z(1)*z(4))*(z(5)*(theta(4)*x(3)*g(3)-theta(6)*x(4)*g(4))-z(6)*x(4));'];
        f{5}=['1/(z(1)*z(4))*(z(5)*(theta(5)*x(3)*g(4)-theta(7)*x(5)*g(4))-z(6)*x(5));'];
        f{6}=['1/(z(1)*z(4))*(z(5)*(theta(6)*x(4)*g(4)-theta(8)*x(6)*g(4))-z(6)*x(6))]'];
        
        f = strcat(f{:})
        theta = ones(1,p); % Arbitrary
        x0 = ones(1,v);
        t =[2500,7500,12500,17500,25000,35000,45000,55000,65000,75000,85000,95000];
        
    case 'Maizeleaf_enzfit_bspline_Acpresc_notinnuc'
        v=6;
        u=4; % No of input funcs from bsplines
        w=6;  % No of input funcs prescribed from data
        nbasis=6;  % No of basis used to make input functions   
        nkin=8;
        p=nbasis*u+nkin;
        
        f{1}=['[1/z(1)*(theta(1)*g(1)*g(2)-theta(2)*x(1)*g(2)-x(1)*z(2));'];
        f{2}=['1/z(1)*(theta(2)*x(1)*g(2)-theta(3)*x(2)*g(2)-x(2)*z(2));'];
        
         f{1}=['[1/z(1)*(theta(1)*g(1)*g(2)-theta(2)*x(1)*g(2)+theta(9)*g(1)*g(5)-theta(10)*x(1)*g(5)-x(1)*z(2));'];
        f{2}=['1/z(1)*(theta(2)*x(1)*g(2)-theta(3)*x(2)*g(2)+theta(10)*x(1)*g(5)-theta(11)*x(2)*g(5)-x(2)*z(2));'];
        
        f{3}=['1/z(1)*(theta(3)*x(2)*g(2)-theta(4)*x(3)*g(3)-theta(5)*x(3)*g(4)-x(3)*z(2));'];
        f{4}=['1/z(1)*(theta(4)*x(3)*g(3)-theta(6)*x(4)*g(4)-x(4)*z(2));'];
        f{5}=['1/z(1)*(theta(5)*x(3)*g(4)-theta(7)*x(5)*g(4)-x(5)*z(2));'];
        f{6}=['1/z(1)*(theta(6)*x(4)*g(4)-theta(8)*x(6)*g(4)-x(6)*z(2))]'];
        
        f = strcat(f{:})
        theta = ones(1,p); % Arbitrary
        x0 = ones(1,v);
        t =[2500,7500,12500,17500,25000,35000,45000,55000,65000,75000,85000,95000];
        
    case 'Maizeleaf_enzfit_bspline_Acpresc_RER0'
        v=6;
        u=4; % No of input funcs from bsplines
        w=6;  % No of input funcs prescribed from data
        nbasis=6;  % No of basis used to make input functions   
        nkin=8;
        p=nbasis*u+nkin;
        
        f{1}=['[1/z(1)*(theta(1)*g(1)*g(2)-theta(2)*x(1)*g(2));'];
        f{2}=['1/z(1)*(theta(2)*x(1)*g(2)-theta(3)*x(2)*g(2));'];
        f{3}=['1/z(1)*(theta(3)*x(2)*g(2)-theta(4)*x(3)*g(3)-theta(5)*x(3)*g(4));'];
        f{4}=['1/z(1)*(theta(4)*x(3)*g(3)-theta(6)*x(4)*g(4));'];
        f{5}=['1/z(1)*(theta(5)*x(3)*g(4)-theta(7)*x(5)*g(4));'];
        f{6}=['1/z(1)*(theta(6)*x(4)*g(4)-theta(8)*x(6)*g(4))]'];
        
        f = strcat(f{:})
        theta = ones(1,p); % Arbitrary
        x0 = ones(1,v);
        t =[2500,7500,12500,17500,25000,35000,45000,55000,65000,75000,85000,95000];
        
        
    case 'Maizeleaf_enzfit_bspline_EZ_Acpresc_notinnuc'
        v=6;
        u=4; % No of input funcs from bsplines
        w=6;  % No of input funcs prescribed from data
        nbasis=6;  % No of basis used to make input functions 
        nkin=8;
        p=nbasis*u+nkin;
        
        f{1}=['[1/(z(1)*(z(3)+z(4)*z(5)))*(z(3)*(theta(1)*g(1)*g(2)-theta(2)*x(1)*g(2))-z(4)*z(6)*x(1));'];
        f{2}=['1/(z(1)*(z(3)+z(4)*z(5)))*(z(3)*(theta(2)*x(1)*g(2)-theta(3)*x(2)*g(2))-z(4)*z(6)*x(2));'];
        f{3}=['1/(z(1)*(z(3)+z(4)*z(5)))*(z(3)*(theta(3)*x(2)*g(2)-theta(4)*x(3)*g(3)-theta(5)*x(3)*g(4))-z(4)*z(6)*x(3));'];
        f{4}=['1/(z(1)*(z(3)+z(4)*z(5)))*(z(3)*(theta(4)*x(3)*g(3)-theta(6)*x(4)*g(4))-z(4)*z(6)*x(4));'];
        f{5}=['1/(z(1)*(z(3)+z(4)*z(5)))*(z(3)*(theta(5)*x(3)*g(4)-theta(7)*x(5)*g(4))-z(4)*z(6)*x(5));'];
        f{6}=['1/(z(1)*(z(3)+z(4)*z(5)))*(z(3)*(theta(6)*x(4)*g(4)-theta(8)*x(6)*g(4))-z(4)*z(6)*x(6))]'];
        
        f = strcat(f{:})
        theta = ones(1,p); % Arbitrary
        x0 = ones(1,v);
        t =[2500,7500,12500,17500,25000,35000,45000,55000,65000,75000,85000,95000];
        
     case 'Maizeleaf_enzfit_bspline_Acpresc_GA20ox_innuc'
        v=6;
        u=5; % No of input funcs from bsplines
        w=6;  % No of input funcs prescribed from data
        nbasis=7;  % No of basis used to make input functions   
        nkin=11;
        p=nbasis*u+nkin;
        
        f{1}=['[1/z(1)*(z(2)*(theta(1)*g(1)*g(2)-theta(2)*x(1)*g(2)+theta(9)*g(1)*g(5)-theta(10)*x(1)*g(5))-x(1)*z(3));'];
        f{2}=['1/z(1)*(z(2)*(theta(2)*x(1)*g(2)-theta(3)*x(2)*g(2)+theta(10)*x(1)*g(5)-theta(11)*x(2)*g(5))-x(2)*z(3));'];
        f{3}=['1/z(1)*(z(2)*(theta(3)*x(2)*g(2)+theta(11)*x(2)*g(5)-theta(4)*x(3)*g(3)-theta(5)*x(3)*g(4))-x(3)*z(3));'];
        f{4}=['1/z(1)*(z(2)*(theta(4)*x(3)*g(3)-theta(6)*x(4)*g(4))-x(4)*z(3));'];
        f{5}=['1/z(1)*(z(2)*(theta(5)*x(3)*g(4)-theta(7)*x(5)*g(4))-x(5)*z(3));'];
        f{6}=['1/z(1)*(z(2)*(theta(6)*x(4)*g(4)-theta(8)*x(6)*g(4))-x(6)*z(3))]'];
        
        f = strcat(f{:})
        theta = ones(1,p); % Arbitrary
        x0 = ones(1,v);
        t =[2500,7500,12500,17500,25000,35000,45000,55000,65000,75000,85000,95000];
        
        
    case 'Maizeleaf_enzfit_bspline_EZ_Acpresc_GA20ox_innuc'
        v=6;
        u=5; % No of input funcs from bsplines
        w=6;  % No of input funcs prescribed from data
        nbasis=7;  % No of basis used to make input functions 
        nkin=11;
        p=nbasis*u+nkin;
        
        f{1}=['[1/(z(1)*z(4))*(z(5)*(theta(1)*g(1)*g(2)-theta(2)*x(1)*g(2)+theta(9)*g(1)*g(5)-theta(10)*x(1)*g(5))-z(6)*x(1));'];
        f{2}=['1/(z(1)*z(4))*(z(5)*(theta(2)*x(1)*g(2)-theta(3)*x(2)*g(2)+theta(10)*x(1)*g(5)-theta(11)*x(2)*g(5))-z(6)*x(2));'];
        f{3}=['1/(z(1)*z(4))*(z(5)*(theta(3)*x(2)*g(2)+theta(11)*x(2)*g(5)-theta(4)*x(3)*g(3)-theta(5)*x(3)*g(4))-z(6)*x(3));'];
        f{4}=['1/(z(1)*z(4))*(z(5)*(theta(4)*x(3)*g(3)-theta(6)*x(4)*g(4))-z(6)*x(4));'];
        f{5}=['1/(z(1)*z(4))*(z(5)*(theta(5)*x(3)*g(4)-theta(7)*x(5)*g(4))-z(6)*x(5));'];
        f{6}=['1/(z(1)*z(4))*(z(5)*(theta(6)*x(4)*g(4)-theta(8)*x(6)*g(4))-z(6)*x(6))]'];
        
        f = strcat(f{:})
        theta = ones(1,p); % Arbitrary
        x0 = ones(1,v);
        t =[2500,7500,12500,17500,25000,35000,45000,55000,65000,75000,85000,95000];
        
        
        case 'Maizeleaf_enzfit_bspline_Acpresc_GA20ox_trans_innuc'
        v=6;
        u=5; % No of input funcs from bsplines
        w=6;  % No of input funcs prescribed from data
        nbasis=8;  % No of basis used to make input functions   
        nkin=9;
        p=nbasis*u+nkin;
        
        f{1}=['[1/z(1)*(z(2)*(theta(1)*g(1)*g(2)-theta(2)*x(1)*g(2)+theta(9)*theta(1)*g(1)*g(5)-theta(9)*theta(2)*x(1)*g(5))-x(1)*z(3));'];
        f{2}=['1/z(1)*(z(2)*(theta(2)*x(1)*g(2)-theta(3)*x(2)*g(2)+theta(9)*theta(2)*x(1)*g(5)-theta(9)*theta(3)*x(2)*g(5))-x(2)*z(3));'];
        f{3}=['1/z(1)*(z(2)*(theta(3)*x(2)*g(2)+theta(9)*theta(3)*x(2)*g(5)-theta(4)*x(3)*g(3)-theta(5)*x(3)*g(4))-x(3)*z(3));'];
        f{4}=['1/z(1)*(z(2)*(theta(4)*x(3)*g(3)-theta(6)*x(4)*g(4))-x(4)*z(3));'];
        f{5}=['1/z(1)*(z(2)*(theta(5)*x(3)*g(4)-theta(7)*x(5)*g(4))-x(5)*z(3));'];
        f{6}=['1/z(1)*(z(2)*(theta(6)*x(4)*g(4)-theta(8)*x(6)*g(4))-x(6)*z(3))]'];
        
        f = strcat(f{:})
        theta = ones(1,p); % Arbitrary
        x0 = ones(1,v);
        t =[2500,7500,12500,17500,25000,35000,45000,55000,65000,75000,85000,95000];
        
        
    case 'Maizeleaf_enzfit_bspline_EZ_Acpresc_GA20ox_trans_innuc'
        v=6;
        u=5; % No of input funcs from bsplines
        w=6;  % No of input funcs prescribed from data
        nbasis=8;  % No of basis used to make input functions 
        nkin=9;
        p=nbasis*u+nkin;
        
        f{1}=['[1/(z(1)*z(4))*(z(5)*(theta(1)*g(1)*g(2)-theta(2)*x(1)*g(2)+theta(9)*theta(1)*g(1)*g(5)-theta(9)*theta(2)*x(1)*g(5))-z(6)*x(1));'];
        f{2}=['1/(z(1)*z(4))*(z(5)*(theta(2)*x(1)*g(2)-theta(3)*x(2)*g(2)+theta(9)*theta(2)*x(1)*g(5)-theta(9)*theta(3)*x(2)*g(5))-z(6)*x(2));'];
        f{3}=['1/(z(1)*z(4))*(z(5)*(theta(3)*x(2)*g(2)+theta(9)*theta(3)*x(2)*g(5)-theta(4)*x(3)*g(3)-theta(5)*x(3)*g(4))-z(6)*x(3));'];
        f{4}=['1/(z(1)*z(4))*(z(5)*(theta(4)*x(3)*g(3)-theta(6)*x(4)*g(4))-z(6)*x(4));'];
        f{5}=['1/(z(1)*z(4))*(z(5)*(theta(5)*x(3)*g(4)-theta(7)*x(5)*g(4))-z(6)*x(5));'];
        f{6}=['1/(z(1)*z(4))*(z(5)*(theta(6)*x(4)*g(4)-theta(8)*x(6)*g(4))-z(6)*x(6))]'];
        
        f = strcat(f{:})
        theta = ones(1,p); % Arbitrary
        x0 = ones(1,v);
        t =[2500,7500,12500,17500,25000,35000,45000,55000,65000,75000,85000,95000];
        
case 'Maizeleaf_enzfit_bspline_Acpresc_constenz_innuc'
        v=6;
        u=1; % No of input funcs from bsplines
        w=6;  % No of input funcs prescribed from data
        nbasis=7;  % No of basis used to make input functions   
        nkin=8;
        p=nbasis*u+nkin+3;
        
        f{1}=['[1/z(1)*(z(2)*(theta(1)*g(1)*theta(9)-theta(2)*x(1)*theta(9))-x(1)*z(3));'];
        f{2}=['1/z(1)*(z(2)*(theta(2)*x(1)*theta(9)-theta(3)*x(2)*theta(9))-x(2)*z(3));'];
        f{3}=['1/z(1)*(z(2)*(theta(3)*x(2)*theta(9)-theta(4)*x(3)*theta(10)-theta(5)*x(3)*theta(11))-x(3)*z(3));'];
        f{4}=['1/z(1)*(z(2)*(theta(4)*x(3)*theta(10)-theta(6)*x(4)*theta(11))-x(4)*z(3));'];
        f{5}=['1/z(1)*(z(2)*(theta(5)*x(3)*theta(11)-theta(7)*x(5)*theta(11))-x(5)*z(3));'];
        f{6}=['1/z(1)*(z(2)*(theta(6)*x(4)*theta(11)-theta(8)*x(6)*theta(11))-x(6)*z(3))]'];
        
        f = strcat(f{:})
        theta = ones(1,p); % Arbitrary
        x0 = ones(1,v);
        t =[2500,7500,12500,17500,25000,35000,45000,55000,65000,75000,85000,95000];

        
        
    case 'Maizeleaf_enzfit_bspline_EZ_Acpresc_constenz_innuc'
        v=6;
        u=1; % No of input funcs from bsplines
        w=6;  % No of input funcs prescribed from data
        nbasis=7;  % No of basis used to make input functions 
        nkin=8;
        p=nbasis*u+nkin+3;
        
        f{1}=['[1/(z(1)*z(4))*(z(5)*(theta(1)*g(1)*theta(9)-theta(2)*x(1)*theta(9))-z(6)*x(1));'];
        f{2}=['1/(z(1)*z(4))*(z(5)*(theta(2)*x(1)*theta(9)-theta(3)*x(2)*theta(9))-z(6)*x(2));'];
        f{3}=['1/(z(1)*z(4))*(z(5)*(theta(3)*x(2)*theta(9)-theta(4)*x(3)*theta(10)-theta(5)*x(3)*theta(11))-z(6)*x(3));'];
        f{4}=['1/(z(1)*z(4))*(z(5)*(theta(4)*x(3)*theta(10)-theta(6)*x(4)*theta(11))-z(6)*x(4));'];
        f{5}=['1/(z(1)*z(4))*(z(5)*(theta(5)*x(3)*theta(11)-theta(7)*x(5)*theta(11))-z(6)*x(5));'];
        f{6}=['1/(z(1)*z(4))*(z(5)*(theta(6)*x(4)*theta(11)-theta(8)*x(6)*theta(11))-z(6)*x(6))]'];
        
        f = strcat(f{:})
        theta = ones(1,p); % Arbitrary
        x0 = ones(1,v);
        t =[2500,7500,12500,17500,25000,35000,45000,55000,65000,75000,85000,95000];
        
    case 'Maizeleaf_enzfit_bspline_Acpresc_constGA53_innuc'
        v=6;
        u=3; % No of input funcs from bsplines
        w=6;  % No of input funcs prescribed from data
        nbasis=7;  % No of basis used to make input functions   
        nkin=8;
        p=nbasis*u+nkin+1;
        
        f{1}=['[1/z(1)*(z(2)*(theta(1)*theta(9)*g(1)-theta(2)*x(1)*g(1))-x(1)*z(3));'];
        f{2}=['1/z(1)*(z(2)*(theta(2)*x(1)*g(1)-theta(3)*x(2)*g(1))-x(2)*z(3));'];
        f{3}=['1/z(1)*(z(2)*(theta(3)*x(2)*g(1)-theta(4)*x(3)*g(2)-theta(5)*x(3)*g(3))-x(3)*z(3));'];
        f{4}=['1/z(1)*(z(2)*(theta(4)*x(3)*g(2)-theta(6)*x(4)*g(3))-x(4)*z(3));'];
        f{5}=['1/z(1)*(z(2)*(theta(5)*x(3)*g(3)-theta(7)*x(5)*g(3))-x(5)*z(3));'];
        f{6}=['1/z(1)*(z(2)*(theta(6)*x(4)*g(3)-theta(8)*x(6)*g(3))-x(6)*z(3))]'];
        
        f = strcat(f{:})
        theta = ones(1,p); % Arbitrary
        x0 = ones(1,v);
        t =[2500,7500,12500,17500,25000,35000,45000,55000,65000,75000,85000,95000];
        
        
    case 'Maizeleaf_enzfit_bspline_EZ_Acpresc_constGA53_innuc'
        v=6;
        u=3; % No of input funcs from bsplines
        w=6;  % No of input funcs prescribed from data
        nbasis=7;  % No of basis used to make input functions 
        nkin=8;
        p=nbasis*u+nkin+1;
        
        f{1}=['[1/(z(1)*z(4))*(z(5)*(theta(1)*theta(9)*g(1)-theta(2)*x(1)*g(1))-z(6)*x(1));'];
        f{2}=['1/(z(1)*z(4))*(z(5)*(theta(2)*x(1)*g(1)-theta(3)*x(2)*g(1))-z(6)*x(2));'];
        f{3}=['1/(z(1)*z(4))*(z(5)*(theta(3)*x(2)*g(1)-theta(4)*x(3)*g(2)-theta(5)*x(3)*g(3))-z(6)*x(3));'];
        f{4}=['1/(z(1)*z(4))*(z(5)*(theta(4)*x(3)*g(2)-theta(6)*x(4)*g(3))-z(6)*x(4));'];
        f{5}=['1/(z(1)*z(4))*(z(5)*(theta(5)*x(3)*g(3)-theta(7)*x(5)*g(3))-z(6)*x(5));'];
        f{6}=['1/(z(1)*z(4))*(z(5)*(theta(6)*x(4)*g(3)-theta(8)*x(6)*g(3))-z(6)*x(6))]'];
        
        f = strcat(f{:})
        theta = ones(1,p); % Arbitrary
        x0 = ones(1,v);
        t =[2500,7500,12500,17500,25000,35000,45000,55000,65000,75000,85000,95000];    

    case 'Maizeleaf_enzfit_bspline_Acpresc_constenz_notinnuc'
        v=6;
        u=1; % No of input funcs from bsplines
        w=6;  % No of input funcs prescribed from data
        nbasis=6;  % No of basis used to make input functions   
        nkin=8;
        p=nbasis*u+nkin+3;
        
        f{1}=['[1/z(1)*(theta(1)*g(1)*theta(9)-theta(2)*x(1)*theta(9)-x(1)*z(2));'];
        f{2}=['1/z(1)*(theta(2)*x(1)*theta(9)-theta(3)*x(2)*theta(9)-x(2)*z(2));'];
        f{3}=['1/z(1)*(theta(3)*x(2)*theta(9)-theta(4)*x(3)*theta(10)-theta(5)*x(3)*theta(11)-x(3)*z(2));'];
        f{4}=['1/z(1)*(theta(4)*x(3)*theta(10)-theta(6)*x(4)*theta(11)-x(4)*z(2));'];
        f{5}=['1/z(1)*(theta(5)*x(3)*theta(11)-theta(7)*x(5)*theta(11)-x(5)*z(2));'];
        f{6}=['1/z(1)*(theta(6)*x(4)*theta(11)-theta(8)*x(6)*theta(11)-x(6)*z(2))]'];
        
        f = strcat(f{:})
        theta = ones(1,p); % Arbitrary
        x0 = ones(1,v);
        t =[2500,7500,12500,17500,25000,35000,45000,55000,65000,75000,85000,95000];
        
        
    case 'Maizeleaf_enzfit_bspline_EZ_Acpresc_constenz_notinnuc'
        v=6;
        u=1; % No of input funcs from bsplines
        w=6;  % No of input funcs prescribed from data
        nbasis=6;  % No of basis used to make input functions 
        nkin=8;
        p=nbasis*u+nkin+3;
        
        f{1}=['[1/(z(1)*(z(3)+z(4)*z(5)))*(z(3)*(theta(1)*g(1)*theta(9)-theta(2)*x(1)*theta(9))-z(4)*z(6)*x(1));'];
        f{2}=['1/(z(1)*(z(3)+z(4)*z(5)))*(z(3)*(theta(2)*x(1)*theta(9)-theta(3)*x(2)*theta(9))-z(4)*z(6)*x(2));'];
        f{3}=['1/(z(1)*(z(3)+z(4)*z(5)))*(z(3)*(theta(3)*x(2)*theta(9)-theta(4)*x(3)*theta(10)-theta(5)*x(3)*theta(11))-z(4)*z(6)*x(3));'];
        f{4}=['1/(z(1)*(z(3)+z(4)*z(5)))*(z(3)*(theta(4)*x(3)*theta(10)-theta(6)*x(4)*theta(11))-z(4)*z(6)*x(4));'];
        f{5}=['1/(z(1)*(z(3)+z(4)*z(5)))*(z(3)*(theta(5)*x(3)*theta(11)-theta(7)*x(5)*theta(11))-z(4)*z(6)*x(5));'];
        f{6}=['1/(z(1)*(z(3)+z(4)*z(5)))*(z(3)*(theta(6)*x(4)*theta(11)-theta(8)*x(6)*theta(11))-z(4)*z(6)*x(6))]'];
        
        f = strcat(f{:})
        theta = ones(1,p); % Arbitrary
        x0 = ones(1,v);
        t =[2500,7500,12500,17500,25000,35000,45000,55000,65000,75000,85000,95000];
        
    case 'Maizeleaf_enzfit_bspline_Acpresc_constGA53_notinnuc'
        v=6;
        u=3; % No of input funcs from bsplines
        w=6;  % No of input funcs prescribed from data
        nbasis=6;  % No of basis used to make input functions   
        nkin=8;
        p=nbasis*u+nkin+1;
        
        f{1}=['[1/z(1)*(theta(1)*theta(9)*g(1)-theta(2)*x(1)*g(1)-x(1)*z(2));'];
        f{2}=['1/z(1)*(theta(2)*x(1)*g(1)-theta(3)*x(2)*g(1)-x(2)*z(2));'];
        f{3}=['1/z(1)*(theta(3)*x(2)*g(1)-theta(4)*x(3)*g(2)-theta(5)*x(3)*g(3)-x(3)*z(2));'];
        f{4}=['1/z(1)*(theta(4)*x(3)*g(2)-theta(6)*x(4)*g(3)-x(4)*z(2));'];
        f{5}=['1/z(1)*(theta(5)*x(3)*g(3)-theta(7)*x(5)*g(3)-x(5)*z(2));'];
        f{6}=['1/z(1)*(theta(6)*x(4)*g(3)-theta(8)*x(6)*g(3)-x(6)*z(2))]'];
        
        f = strcat(f{:})
        theta = ones(1,p); % Arbitrary
        x0 = ones(1,v);
        t =[2500,7500,12500,17500,25000,35000,45000,55000,65000,75000,85000,95000];
        
        
    case 'Maizeleaf_enzfit_bspline_EZ_Acpresc_constGA53_notinnuc'
        v=6;
        u=3; % No of input funcs from bsplines
        w=6;  % No of input funcs prescribed from data
        nbasis=6;  % No of basis used to make input functions 
        nkin=8;
        p=nbasis*u+nkin+1;
        
        f{1}=['[1/(z(1)*(z(3)+z(4)*z(5)))*(z(3)*(theta(1)*theta(9)*g(1)-theta(2)*x(1)*g(1))-z(4)*z(6)*x(1));'];
        f{2}=['1/(z(1)*(z(3)+z(4)*z(5)))*(z(3)*(theta(2)*x(1)*g(1)-theta(3)*x(2)*g(1))-z(4)*z(6)*x(2));'];
        f{3}=['1/(z(1)*(z(3)+z(4)*z(5)))*(z(3)*(theta(3)*x(2)*g(1)-theta(4)*x(3)*g(2)-theta(5)*x(3)*g(3))-z(4)*z(6)*x(3));'];
        f{4}=['1/(z(1)*(z(3)+z(4)*z(5)))*(z(3)*(theta(4)*x(3)*g(2)-theta(6)*x(4)*g(3))-z(4)*z(6)*x(4));'];
        f{5}=['1/(z(1)*(z(3)+z(4)*z(5)))*(z(3)*(theta(5)*x(3)*g(3)-theta(7)*x(5)*g(3))-z(4)*z(6)*x(5));'];
        f{6}=['1/(z(1)*(z(3)+z(4)*z(5)))*(z(3)*(theta(6)*x(4)*g(3)-theta(8)*x(6)*g(3))-z(4)*z(6)*x(6))]'];
        
        f = strcat(f{:})
        theta = ones(1,p); % Arbitrary
        x0 = ones(1,v);
        t =[2500,7500,12500,17500,25000,35000,45000,55000,65000,75000,85000,95000];
              
        
     case 'Maizeleaf_enzfit_bspline_Acpresc_GA20ox_notinnuc'
        v=6;
        u=5; % No of input funcs from bsplines
        w=6;  % No of input funcs prescribed from data
        nbasis=6;  % No of basis used to make input functions   
        nkin=11;
        p=nbasis*u+nkin;
        
        f{1}=['[1/z(1)*(theta(1)*g(1)*g(2)-theta(2)*x(1)*g(2)+theta(9)*g(1)*g(5)-theta(10)*x(1)*g(5)-x(1)*z(2));'];
        f{2}=['1/z(1)*(theta(2)*x(1)*g(2)-theta(3)*x(2)*g(2)+theta(10)*x(1)*g(5)-theta(11)*x(2)*g(5)-x(2)*z(2));'];
        f{3}=['1/z(1)*(theta(3)*x(2)*g(2)+theta(11)*x(2)*g(5)-theta(4)*x(3)*g(3)-theta(5)*x(3)*g(4)-x(3)*z(2));'];
        f{4}=['1/z(1)*(theta(4)*x(3)*g(3)-theta(6)*x(4)*g(4)-x(4)*z(2));'];
        f{5}=['1/z(1)*(theta(5)*x(3)*g(4)-theta(7)*x(5)*g(4)-x(5)*z(2));'];
        f{6}=['1/z(1)*(theta(6)*x(4)*g(4)-theta(8)*x(6)*g(4)-x(6)*z(2))]'];
        
        f = strcat(f{:})
        theta = ones(1,p); % Arbitrary
        x0 = ones(1,v);
        t =[2500,7500,12500,17500,25000,35000,45000,55000,65000,75000,85000,95000];
        
        
    case 'Maizeleaf_enzfit_bspline_EZ_Acpresc_GA20ox_notinnuc'
        v=6;
        u=5; % No of input funcs from bsplines
        w=6;  % No of input funcs prescribed from data
        nbasis=6;  % No of basis used to make input functions 
        nkin=11;
        p=nbasis*u+nkin;
        
        f{1}=['[1/(z(1)*(z(3)+z(4)*z(5)))*(z(3)*(theta(1)*g(1)*g(2)-theta(2)*x(1)*g(2)+theta(9)*g(1)*g(5)-theta(10)*x(1)*g(5))-z(4)*z(6)*x(1));'];
        f{2}=['1/(z(1)*(z(3)+z(4)*z(5)))*(z(3)*(theta(2)*x(1)*g(2)-theta(3)*x(2)*g(2)+theta(10)*x(1)*g(5)-theta(11)*x(2)*g(5))-z(4)*z(6)*x(2));'];
        f{3}=['1/(z(1)*(z(3)+z(4)*z(5)))*(z(3)*(theta(3)*x(2)*g(2)+theta(11)*x(2)*g(5)-theta(4)*x(3)*g(3)-theta(5)*x(3)*g(4))-z(4)*z(6)*x(3));'];
        f{4}=['1/(z(1)*(z(3)+z(4)*z(5)))*(z(3)*(theta(4)*x(3)*g(3)-theta(6)*x(4)*g(4))-z(4)*z(6)*x(4));'];
        f{5}=['1/(z(1)*(z(3)+z(4)*z(5)))*(z(3)*(theta(5)*x(3)*g(4)-theta(7)*x(5)*g(4))-z(4)*z(6)*x(5));'];
        f{6}=['1/(z(1)*(z(3)+z(4)*z(5)))*(z(3)*(theta(6)*x(4)*g(4)-theta(8)*x(6)*g(4))-z(4)*z(6)*x(6))]'];
        
        f = strcat(f{:})
        theta = ones(1,p); % Arbitrary
        x0 = ones(1,v);
        t =[2500,7500,12500,17500,25000,35000,45000,55000,65000,75000,85000,95000];
              
end

mdl = modl_bsplines(modelName,p,v,u,w,nbasis,nkin);

if nargout > 1; pars = params(mdl, theta, x0, t); end
if nargin > 1 && symbCalcFlag, doSymbolicCalcs_bsplines(modelName,f,p,v,u,w); end
if exist('xObs','var'), obsData = ser(t,xObs); else obsData = []; end
