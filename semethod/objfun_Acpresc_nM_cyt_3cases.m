% Updated Sept 6th 2017. Now fits solutions in nM. Incorporates data where
% enzymes and metabolites have different time points. 

function [f,J]=objfun_Acpresc_nM_cyt_3cases(thetahat,par_data,mdl_m,mdl_EZ,pars_m,pars_EZ,ntime,ntime_enz,nparams,nkinparams,basisvals_meas_tp,basisvals_meas_tp_enz,Jinputs_c1,Jinputs_c2,Jinputs_c3,...
    recipmaxvec,mt_c1,mt_c2,mt_c3,EZt_c1,EZt_c2,EZt_c3,data_c1,data_c2,data_c3,initialt,breaks,norder,th_pos_c1,th_pos_c2,th_pos_c3)

% Solutions for case one:
pars_m.theta=10.^thetahat(th_pos_c1);
pars_EZ.theta=pars_m.theta;
pars_m.t=mt_c1;
pars_EZ.t=EZt_c1;
th_inputfunc_reshape=reshape(pars_m.theta(nkinparams+1:end),mdl_m.nbasis,mdl_m.u);
pars_m.x0=v0_initialconditions(pars_m.theta,th_inputfunc_reshape,data_c1.initialt,breaks,norder,data_c1);

[sol_m_case1,~,dxdtheta_m_case1,~] = mdl_m.solveWithSensEqns_bsplines(pars_m,data_c1,breaks,norder);
pars_EZ.x0=sol_m_case1.x(:,end);
[sol_EZ_case1,~,dxdtheta_EZ_case1,~] = mdl_EZ.solveWithSensEqns_bsplines(pars_EZ,data_c1,breaks,norder);

sol_input_GA_case1=basisvals_meas_tp*th_inputfunc_reshape(:,1);
sol_input_enz_case1=basisvals_meas_tp_enz*th_inputfunc_reshape(:,2:4);
sol_GAs_nM_case1=[sol_m_case1.x(:,2:end-1),sol_EZ_case1.x(:,2:end);sol_input_GA_case1']; % Note now skip the first time point entry, as this in the initial condition.

ntGAs=(mdl_m.v+1)*ntime;
solvec_case1=[reshape(sol_GAs_nM_case1',ntGAs,1);reshape(sol_input_enz_case1,3*ntime_enz,1)];

% Solutions for case two:
pars_m.theta=10.^(thetahat(th_pos_c2));
pars_EZ.theta=pars_m.theta;
pars_m.t=mt_c2;
pars_EZ.t=EZt_c2;
th_inputfunc_reshape=reshape(pars_m.theta(nkinparams+1:end),mdl_m.nbasis,mdl_m.u);
pars_m.x0=v0_initialconditions(pars_m.theta,th_inputfunc_reshape,initialt,breaks,norder,data_c2);

[sol_m_case2,~,dxdtheta_m_case2,~] = mdl_m.solveWithSensEqns_bsplines(pars_m,data_c2,breaks,norder);
pars_EZ.x0=sol_m_case2.x(:,end);
[sol_EZ_case2,~,dxdtheta_EZ_case2,~] = mdl_EZ.solveWithSensEqns_bsplines(pars_EZ,data_c2,breaks,norder);

sol_input_GA_case2=basisvals_meas_tp*th_inputfunc_reshape(:,1);
sol_input_enz_case2=basisvals_meas_tp_enz*th_inputfunc_reshape(:,2:4);
sol_GAs_nM_case2=[sol_m_case2.x(:,2:end-1),sol_EZ_case2.x(:,2:end);sol_input_GA_case2']; % Note now skip the first time point entry, as this in the initial condition.
solvec_case2=[reshape(sol_GAs_nM_case2',ntGAs,1);reshape(sol_input_enz_case2,3*ntime_enz,1)];

% Solutions for case three:
%thetamult3=10.^thetahat(end)*thf_posc3+(1-thf_posc3);
%pars_m.theta=thetamult3.*10.^(thetahat(th_pos_c3));
pars_m.theta=10.^(thetahat(th_pos_c3));
pars_EZ.theta=pars_m.theta;
pars_m.t=mt_c3;
pars_EZ.t=EZt_c3;
th_inputfunc_reshape=reshape(pars_m.theta(nkinparams+1:end),mdl_m.nbasis,mdl_m.u);
pars_m.x0=v0_initialconditions(pars_m.theta,th_inputfunc_reshape,initialt,breaks,norder,data_c3);

[sol_m_case3,~,dxdtheta_m_case3,~] = mdl_m.solveWithSensEqns_bsplines(pars_m,data_c3,breaks,norder);
pars_EZ.x0=sol_m_case3.x(:,end);
[sol_EZ_case3,~,dxdtheta_EZ_case3,~] = mdl_EZ.solveWithSensEqns_bsplines(pars_EZ,data_c3,breaks,norder);

sol_input_GA_case3=basisvals_meas_tp*th_inputfunc_reshape(:,1);
sol_input_enz_case3=basisvals_meas_tp_enz*th_inputfunc_reshape(:,2:4);
sol_GAs_nM_case3=[sol_m_case3.x(:,2:end-1),sol_EZ_case3.x(:,2:end);sol_input_GA_case3']; % Note now skip the first time point entry, as this in the initial condition.
solvec_case3=[reshape(sol_GAs_nM_case3',ntGAs,1);reshape(sol_input_enz_case3,3*ntime_enz,1)];

% Calculate f: 
f=recipmaxvec.*([solvec_case1;solvec_case2;solvec_case3]-par_data); %The function whose sum of squares is minimized.

% Calculate J:
% Form dxdtheta to include both regions and input functions:
dxdtheta_case1_case1param=cat(3,dxdtheta_m_case1(:,:,2:end-1),dxdtheta_EZ_case1(:,:,2:end));
dxdtheta_case1=zeros(mdl_m.v,nparams,ntime);
dxdtheta_case1(:,[th_pos_c1],:)=dxdtheta_case1_case1param;
z=permute(dxdtheta_case1,[3 1 2]);
J_vars_case1=reshape(z,[mdl_m.v*ntime,nparams]);

dxdtheta_case2_case2param=cat(3,dxdtheta_m_case2(:,:,2:end-1),dxdtheta_EZ_case2(:,:,2:end));
dxdtheta_case2=zeros(mdl_m.v,nparams,ntime);
dxdtheta_case2(:,[th_pos_c2],:)=dxdtheta_case2_case2param;
z=permute(dxdtheta_case2,[3 1 2]);
J_vars_case2=reshape(z,[mdl_m.v*ntime,nparams]);

dxdtheta_case3_case3param=cat(3,dxdtheta_m_case3(:,:,2:end-1),dxdtheta_EZ_case3(:,:,2:end));
dxdtheta_case3=zeros(mdl_m.v,nparams,ntime);
dxdtheta_case3(:,[th_pos_c3],:)=dxdtheta_case3_case3param;
z=permute(dxdtheta_case3,[3 1 2]);
J_vars_case3=reshape(z,[mdl_m.v*ntime,nparams]);

J=[J_vars_case1;Jinputs_c1;J_vars_case2;Jinputs_c2;J_vars_case3;Jinputs_c3];

% Scale J due to model predicting cytoplasmic conc and objective function
% scaling with max of each dataset:
overallscale=repmat(recipmaxvec,1,nparams);  
J=overallscale.*J;

% Scale J as working in log parameter space:
multmat=log(10)*repmat(10.^thetahat,[3*(ntGAs+ntime_enz*3) 1]);
J=multmat.*J;







    









