% Estimates model parameters for the B104 wildtype and GA20oxOE data, 

% Assumes that Maize and Arabidopsis GA20ox have different rates of
% oxidation.


% Parameter estimates are used in plotting Figure 4.


function runsemthod_B104wtGA20ox_Acpresc_nM_cyt_Arabdiff

[mdl_wt_m,pars_wt_m] = makeModel_bsplinesandprescribedinput_Acpresc('Maizeleaf_enzfit_bspline_Acpresc_innuc',1);
[mdl_wt_EZ,pars_wt_EZ] = makeModel_bsplinesandprescribedinput_Acpresc('Maizeleaf_enzfit_bspline_EZ_Acpresc_innuc',1);

[mdl_GA20ox_m,pars_GA20ox_m] = makeModel_bsplinesandprescribedinput_Acpresc('Maizeleaf_enzfit_bspline_Acpresc_GA20ox_innuc',1);
[mdl_GA20ox_EZ,pars_GA20ox_EZ] = makeModel_bsplinesandprescribedinput_Acpresc('Maizeleaf_enzfit_bspline_EZ_Acpresc_GA20ox_innuc',1);

addpath ('../modelcreation_outputs')

nruns=10;

%load data
load ../data/GA20oxOE/data_GA20oxOE_GAconc_nM_GA20oxAdata.mat

data_c1=data;
data_c2=data_GA20ox;

data_c1.initialt=1000;
data_c2.initialt=1000;
tvec=[data_c1.initialt;data_c1.pos_GAs];
mt_c1=[tvec(tvec<data_c1.xm);data_c1.xm];
EZt_c1=[data_c1.xm;tvec(tvec>data_c1.xm)];
mt_c2=[tvec(tvec<data_c2.xm);data_c2.xm];
EZt_c2=[data_c2.xm;tvec(tvec>data_c2.xm)];

ntime=length(data_c1.pos_GAs);
ntime_enz=length(data_c1.pos_enz);
pos_enz=data_c1.pos_enz;
u_wt=mdl_wt_m.u;
nenz_wt=u_wt-1;
u_GA20ox=mdl_GA20ox_m.u;
nenz_GA20ox=u_GA20ox-1;
nvars=mdl_wt_m.v;
nkin_wt=mdl_wt_m.nkin;
nkin_GA20ox=mdl_GA20ox_m.nkin;

addpath ('../fdaM')
load ../fittedinputfunctions/fittedinputs_B104wt_GA53nM_nbasis7_vector_norder3_evenbasis_innuc_initialt1000_B104Adata.mat theta_fittedinputs nbasis norder basis breaks rng
theta_fittedinputs_wt=theta_fittedinputs;
load ../fittedinputfunctions/fittedinputs_B104GA20ox_Arabdiff_GA53nM_nbasis7_vector_norder3_evenbasis_innuc_initialt1000_GA20oxOEAdata.mat theta_fittedinputs 
theta_fittedinputs_GA20ox=theta_fittedinputs;

nparams=nkin_GA20ox+nbasis*u_wt+nbasis*u_GA20ox;

if mdl_wt_m.nbasis ~= nbasis
    warning('nbasis inconsistent')
end

basisvals_meas_tp=eval_basis(data.pos_GAs,basis);
basisvals_meas_tp_enz=eval_basis(pos_enz,basis);

nbasisA=6;
data_c1.kappa=1;
data_c2.kappa=1;
basisA = create_bspline_basis(rng, nbasisA,norder);
data_c1=calculateequationexpressions_innuc(data_c1,basisA,data_c1.initialt);
data_c2=calculateequationexpressions_innuc(data_c2,basisA,data_c2.initialt);

%Convert data to cytoplasmic concentrations, assuming GA in nucleus (see dataconversion_wcompartments for alternative assumptions):

[cyt_conv_c1,par_data_c1]=convertdata_cytconc(data_c1);
[cyt_conv_c2,par_data_c2]=convertdata_cytconc_GA20oxOEArabdiff(data_c2);

par_data=[par_data_c1;par_data_c2];

plotGAs_data_twocases(data.pos_GAs,par_data_c1,par_data_c2)

% Calculate Jacobian matrix for the inputs:
J_GA53_c1=[zeros(ntime,nkin_GA20ox),full(basisvals_meas_tp),zeros(ntime,nbasis*nenz_wt),zeros(ntime,nbasis*u_GA20ox)];
J_enz_c1=zeros(nenz_wt*ntime_enz,nparams);
for i=1:nenz_wt
    J_enz_c1(ntime_enz*(i-1)+1:ntime_enz*i,:)=[zeros(ntime_enz,nkin_GA20ox),zeros(ntime_enz,nbasis*i),full(basisvals_meas_tp_enz),zeros(ntime_enz,nbasis*(nenz_wt-i)),zeros(ntime_enz,nbasis*u_GA20ox)];
end
J_inputs_c1=[J_GA53_c1;J_enz_c1];

J_GA53_c2=[zeros(ntime,nkin_GA20ox+nbasis*u_wt),full(basisvals_meas_tp),zeros(ntime,nbasis*nenz_GA20ox)];
J_enz_c2=zeros(nenz_GA20ox*ntime_enz,nparams);
for i=1:nenz_GA20ox
    J_enz_c2(ntime_enz*(i-1)+1:ntime_enz*i,:)=[zeros(ntime_enz,nkin_GA20ox),zeros(ntime_enz,nbasis*u_wt),zeros(ntime_enz,nbasis*i),full(basisvals_meas_tp_enz),zeros(ntime_enz,nbasis*(nenz_GA20ox-i))];
end
J_inputs_c2=[J_GA53_c2;J_enz_c2];

% Calculate vector of maximum values for scaling:
maxGA44_c1=max(cyt_conv_c1.*data_c1.GA44);
maxGA19_c1=max(cyt_conv_c1.*data_c1.GA19);
maxGA20_c1=max(cyt_conv_c1.*data_c1.GA20);
maxGA1_c1=max(cyt_conv_c1.*data_c1.GA1);
maxGA29_c1=max(cyt_conv_c1.*data_c1.GA29);
maxGA8_c1=max(cyt_conv_c1.*data_c1.GA8);
maxGA53_c1=max(cyt_conv_c1.*data_c1.GA53);
maxGA20ox_c1=max(data_c1.GA20ox);
maxGA3ox_c1=max(data_c1.GA3ox2);
maxGA2ox_c1=max(data_c1.GA2ox);

maxGA44_c2=max(cyt_conv_c2.*data_c2.GA44);
maxGA19_c2=max(cyt_conv_c2.*data_c2.GA19);
maxGA20_c2=max(cyt_conv_c2.*data_c2.GA20);
maxGA1_c2=max(cyt_conv_c2.*data_c2.GA1);
maxGA29_c2=max(cyt_conv_c2.*data_c2.GA29);
maxGA8_c2=max(cyt_conv_c2.*data_c2.GA8);
maxGA53_c2=max(cyt_conv_c2.*data_c2.GA53);
maxGA20ox_Maize_c2=max(data_c2.GA20ox_Maize);
maxGA3ox_c2=max(data_c2.GA3ox2);
maxGA2ox_c2=max(data_c2.GA2ox);
maxGA20ox_Arab_c2=max(data_c2.GA20ox_Arab);

maxvec_c1=[maxGA44_c1*ones(ntime,1);maxGA19_c1*ones(ntime,1);maxGA20_c1*ones(ntime,1);maxGA1_c1*ones(ntime,1);...
    maxGA29_c1*ones(ntime,1);maxGA8_c1*ones(ntime,1);maxGA53_c1*ones(ntime,1);maxGA20ox_c1*ones(ntime_enz,1);...
    maxGA3ox_c1*ones(ntime_enz,1);maxGA2ox_c1*ones(ntime_enz,1)];

maxvec_c2=[maxGA44_c2*ones(ntime,1);maxGA19_c2*ones(ntime,1);maxGA20_c2*ones(ntime,1);maxGA1_c2*ones(ntime,1);...
    maxGA29_c2*ones(ntime,1);maxGA8_c2*ones(ntime,1);maxGA53_c2*ones(ntime,1);maxGA20ox_Maize_c2*ones(ntime_enz,1);...
    maxGA3ox_c2*ones(ntime_enz,1);maxGA2ox_c2*ones(ntime_enz,1);maxGA20ox_Arab_c2*ones(ntime_enz,1)];

maxvec=[maxvec_c1;maxvec_c2];
recipmaxvec=1./maxvec;

% Calculate vector of maximum values for scaling:
maxvec_GAs_c1=zeros((mdl_wt_m.v+1)*ntime,1);
maxvec_GAs_c2=zeros((mdl_wt_m.v+1)*ntime,1);

for i=1:mdl_wt_m.v+1
    maxvec_GAs_c1((i-1)*ntime+1:i*ntime)=max(par_data_c1((i-1)*ntime+1:i*ntime))*ones(ntime,1);
    maxvec_GAs_c2((i-1)*ntime+1:i*ntime)=max(par_data_c2((i-1)*ntime+1:i*ntime))*ones(ntime,1);
end
maxvec_enz_c1=zeros(nenz_wt*ntime_enz,1);
maxvec_enz_c2=zeros(nenz_GA20ox*ntime_enz,1);
for i=1:nenz_wt
    maxvec_enz_c1((i-1)*ntime_enz+1:i*ntime_enz)=max(par_data_c1((mdl_wt_m.v+1)*ntime+(i-1)*ntime_enz+1:(mdl_wt_m.v+1)*ntime+i*ntime_enz))*ones(ntime_enz,1);
end
for i=1:nenz_GA20ox
    maxvec_enz_c2((i-1)*ntime_enz+1:i*ntime_enz)=max(par_data_c2((mdl_wt_m.v+1)*ntime+(i-1)*ntime_enz+1:(mdl_wt_m.v+1)*ntime+i*ntime_enz))*ones(ntime_enz,1);
end
maxvec_v2=[maxvec_GAs_c1;maxvec_enz_c1;maxvec_GAs_c2;maxvec_enz_c2];

maxvec-maxvec_v2
recipmaxvec=1./maxvec;

ndata=14*ntime+7*ntime_enz;

lb=-6*ones(nparams,1);
ub=[3*ones(nkin_GA20ox,1);4*ones(nbasis*(u_wt+u_GA20ox),1)];

theta0_01=lhsdesign(nruns,nkin_GA20ox);
theta0=zeros(nruns,nparams);
for j=1:nruns
    theta0(j,1:nkin_GA20ox)=theta0_01(j,:).*(ub(1:nkin_GA20ox)-lb(1:nkin_GA20ox))'+lb(1:nkin_GA20ox)';
    theta0(j,nkin_GA20ox+1:nkin_GA20ox+u_wt*nbasis)=theta_fittedinputs_wt;
    theta0(j,nkin_GA20ox+u_wt*nbasis+1:end)=theta_fittedinputs_GA20ox;
end


theta=zeros(nruns,nparams);
fval=zeros(1,nruns);
exitflag=zeros(1,nruns);
telapsed_eachrun=zeros(1,nruns);

objfun=@(thetahat)objfun_Acpresc_nM_cyt_2cases_Arabdiff(thetahat,par_data,mdl_wt_m,mdl_wt_EZ,mdl_GA20ox_m,mdl_GA20ox_EZ,pars_wt_m,pars_wt_EZ,pars_GA20ox_m,pars_GA20ox_EZ,ntime,ntime_enz,nparams,nvars,u_wt,u_GA20ox,nkin_wt,nkin_GA20ox,nbasis,...
    basisvals_meas_tp,basisvals_meas_tp_enz,J_inputs_c1,J_inputs_c2,recipmaxvec,data_c1,data_c2,breaks,norder,mt_c1,EZt_c1,mt_c2,EZt_c2);
                                                                      
options = optimset('Jacobian','on','Display', 'off');%'MaxFunEvals',500);%'DerivativeCheck','on');%,'Diagnostics','on');
%parpool('AttachedFiles',{'bsplineM.m'})

tstart = tic;
for i=1:nruns
    i
    [theta(i,:),fval(i),~,exitflag(i)] = lsqnonlin(objfun,theta0(i,:),lb,ub,options);
    telapsed_eachrun(i) = toc(tstart);
end

%delete(gcp('nocreate'))
telapsed = toc(tstart);

[f_best,I]=min(fval);
theta_best=theta(I,:);
 
AIC=2*nparams+ndata*log(f_best/ndata)
AICc=2*nparams*ndata/(ndata-nparams-1)+ndata*log(f_best/ndata)

time_for_run(1)=telapsed_eachrun(1);
for j=2:nruns;
    time_for_run(j)=telapsed_eachrun(j)-telapsed_eachrun(j-1);
end

save ../parameter_estimates/semethod_B104wtandGA20oxOE_Arabdiff_nM_cyt_evenbasis_nbas7_Acpresc_lbm6_innuc_init1000_B104wtAdata_n10_test.mat

% Plots are made using plot_enzfittedsolution_wEZ_2cases.m