function plotGAs_data(xvec_GAs,xvec_enz,par_data)
ntime=length(xvec_GAs);
ntime_enz=length(xvec_enz);


GA44data=par_data(1:ntime);
GA19data=par_data(ntime+1:2*ntime);
GA20data=par_data(2*ntime+1:3*ntime);
GA1data=par_data(3*ntime+1:4*ntime);
GA29data=par_data(4*ntime+1:5*ntime);
GA8data=par_data(5*ntime+1:6*ntime);
GA53data=par_data(6*ntime+1:7*ntime);
GA20oxdata=par_data(7*ntime+1:7*ntime+ntime_enz);
GA3oxdata=par_data(7*ntime+ntime_enz+1:7*ntime+2*ntime_enz);
GA2oxdata=par_data(7*ntime+2*ntime_enz+1:end);

figure

subplot(2,5,1)
plot(1/1000*xvec_GAs(1:end),GA53data(1:end),':x','LineWidth',2)
hold on
title('GA53')
xlabel('Dist (mm)','Interpreter','latex','FontSize',16)
ylabel('Conc (nM)','Interpreter','latex','FontSize',16)
box off

subplot(2,5,2)
plot(1/1000*xvec_enz(1:end),GA20oxdata(1:end),':x','LineWidth',2)
hold on
title('GA20ox')
xlabel('Dist (mm)','Interpreter','latex','FontSize',16)
ylabel('Conc (nM)','Interpreter','latex','FontSize',16)
box off

subplot(2,5,3)
plot(1/1000*xvec_enz(1:end),GA3oxdata(1:end),':x','LineWidth',2)
hold on
title('GA3ox')
xlabel('Dist (mm)','Interpreter','latex','FontSize',16)
ylabel('Conc (nM)','Interpreter','latex','FontSize',16)
box off

subplot(2,5,4)
plot(1/1000*xvec_enz(1:end),GA2oxdata(1:end),':x','LineWidth',2)
hold on
title('GA2ox')
xlabel('Dist (mm)','Interpreter','latex','FontSize',16)
ylabel('Conc (nM)','Interpreter','latex','FontSize',16)
box off

subplot(2,5,5)
plot(1/1000*xvec_GAs(1:end),GA44data(1:end),':x','LineWidth',2)
hold on
title('GA44')
xlabel('Dist (mm)','Interpreter','latex','FontSize',16)
ylabel('Conc (nM)','Interpreter','latex','FontSize',16)
box off

subplot(2,5,6)
plot(1/1000*xvec_GAs(1:end),GA19data(1:end),':x','LineWidth',2)
hold on
title('GA19')
xlabel('Dist (mm)','Interpreter','latex','FontSize',16)
ylabel('Conc (nM)','Interpreter','latex','FontSize',16)
box off

subplot(2,5,7)
plot(1/1000*xvec_GAs(1:end),GA20data(1:end),':x','LineWidth',2)
hold on
title('GA20')
xlabel('Dist (mm)','Interpreter','latex','FontSize',16)
ylabel('Conc (nM)','Interpreter','latex','FontSize',16)
box off

subplot(2,5,8)
plot(1/1000*xvec_GAs(1:end),GA1data(1:end),':x','LineWidth',2)
hold on
title('GA1')
xlabel('Dist (mm)','Interpreter','latex','FontSize',16)
ylabel('Conc (nM)','Interpreter','latex','FontSize',16)
box off

subplot(2,5,9)
plot(1/1000*xvec_GAs(1:end),GA29data(1:end),':x','LineWidth',2)
title('GA29')
xlabel('Dist (mm)','Interpreter','latex','FontSize',16)
ylabel('Conc (nM)','Interpreter','latex','FontSize',16)
box off

subplot(2,5,10)
plot(1/1000*xvec_GAs(1:end),GA8data(1:end),':x','LineWidth',2)
title('GA8')
xlabel('Dist (mm)','Interpreter','latex','FontSize',16)
ylabel('Conc (nM)','Interpreter','latex','FontSize',16)
box off
%legend1=legend('GA44','GA19','GA20','GA1','GA29','GA8')
%set(legend1,'Interpreter','latex','Location','SouthEast','FontSize',24)
%box('off')
%set(gca,'FontSize',20)
