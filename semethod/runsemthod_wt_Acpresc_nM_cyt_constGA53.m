% Estimates model parameters for the B73 wildtype, control data, 
% assuming that the GA53 input fucntion is constants.


% Parameter estimates are used in plotting Figure S17.


function runsemthod_wt_Acpresc_nM_cyt_constGA53

[mdl_m,pars_m] = makeModel_bsplinesandprescribedinput_Acpresc('Maizeleaf_enzfit_bspline_Acpresc_constGA53_innuc',1);
[mdl_EZ,pars_EZ] = makeModel_bsplinesandprescribedinput_Acpresc('Maizeleaf_enzfit_bspline_EZ_Acpresc_constGA53_innuc',1);

addpath ('../modelcreation_outputs')

nruns=10;

%load enzyme data points:
load ../data/drought_cold/data_PCRenz_GAconc_nM_threecases_final_Ainterp_allenzpts.mat



data.initialt=1000;
tvec=[data.initialt;data.pos_GAs];
pars_m.t=[tvec(tvec<data.xm);data.xm];
pars_EZ.t=[data.xm;tvec(tvec>data.xm)];

ntime=length(data.pos_GAs);
ntime_enz=length(data.pos_enz);
pos_enz=data.pos_enz;
nbasis=mdl_m.nbasis;
u=mdl_m.u;
nenz=3;
nparams=mdl_m.p;
nvars=mdl_m.v;
nkinparams=mdl_m.nkin;

addpath ('../fdaM')

load ../fittedinputfunctions/fittedinputs_3cases_allenzpts_GA53nM_nbasis7_vector_norder3_evenbasis_lbm6_initialt1000.mat theta_fittedinputs_wt nbasis norder basis breaks rng

if mdl_m.nbasis ~= nbasis
    warning('nbasis inconsistent')
end

basisvals_meas_tp_enz=eval_basis(pos_enz,basis);

nbasisA=6;
data.kappa=1;
basisA = create_bspline_basis(rng, nbasisA,norder);
data=calculateequationexpressions_innuc(data,basisA,data.initialt);

% Convert data to cytoplasmic concentrations:
[cyt_conv,par_data]=convertdata_cytconc(data);

plotGAs_data(data.pos_GAs,data.pos_enz,par_data)

% Order parameters : kinetic parameters, constant enz parameters, bspline GA53 parameters.

% Calculate Jacobian matrix for the inputs:
J_GA53=[zeros(ntime,nkinparams),ones(ntime,1),zeros(ntime,nenz*nbasis)];
J_enz=zeros(nenz*ntime_enz,nparams);
for i=1:nenz
    J_enz(ntime_enz*(i-1)+1:ntime_enz*i,:)=[zeros(ntime_enz,nkinparams+1),zeros(ntime_enz,nbasis*(i-1)),full(basisvals_meas_tp_enz),zeros(ntime_enz,nbasis*(nenz-i))];
end
J_inputs=[J_GA53;J_enz];


% calculate vector of maximum values for scaling:
maxvec_GAs=zeros((mdl_m.v+1)*ntime,1);
for i=1:mdl_m.v+1
    maxvec_GAs((i-1)*ntime+1:i*ntime)=max(par_data((i-1)*ntime+1:i*ntime))*ones(ntime,1);
end
maxvec_enz=zeros(nenz*ntime_enz,1);
for i=1:nenz
    maxvec_enz((i-1)*ntime_enz+1:i*ntime_enz)=max(par_data(7*ntime+(i-1)*ntime_enz+1:(mdl_m.v+1)*ntime+i*ntime_enz))*ones(ntime_enz,1);
end

maxvec=[maxvec_GAs;maxvec_enz];
recipmaxvec=1./maxvec;

ndata=7*ntime+3*ntime_enz;


lb=[-6*ones(nkinparams,1);-1;-6*ones(nbasis*u,1)];
ub=[3*ones(nkinparams,1);4;4*ones(nbasis*u,1)];

objfun=@(thetahat)objfun_Acpresc_nM_cyt_constGA53(thetahat,par_data,mdl_m,mdl_EZ,pars_m,pars_EZ,ntime,ntime_enz,nparams,nvars,u,nkinparams,nbasis,basisvals_meas_tp_enz,J_inputs,recipmaxvec,data,breaks,norder);

options = optimset('Jacobian','on','Display', 'off','MaxFunEvals',200);%'DerivativeCheck','on');%,'Diagnostics','on');

%options = optimset(options, 'UseParallel','Always');
%matlabpool open local 4

%parpool('AttachedFiles',{'bsplineM.m'})

theta0_01=lhsdesign(nruns,nkinparams);
theta0=zeros(nruns,nparams);
for j=1:nruns
     theta0(j,1:nkinparams)=theta0_01(j,:).*(ub(1:nkinparams)-lb(1:nkinparams))'+lb(1:nkinparams)';
     theta0(j,nkinparams+1)=mean(cyt_conv.*data.GA53);
     theta0(j,nkinparams+2:end)=theta_fittedinputs_wt(nbasis+1:end)';
end


theta=zeros(nruns,nparams);
fval=zeros(1,nruns);
exitflag=zeros(1,nruns);
telapsed_eachrun=zeros(1,nruns);

tstart = tic;
for i=1:nruns
    i
    [theta(i,:),fval(i),~,exitflag(i)] = lsqnonlin(objfun,theta0(i,:),lb,ub,options);
    telapsed_eachrun(i) = toc(tstart);
 end;
 telapsed = toc(tstart)
[f_best,I]=min(fval)
theta_best=theta(I,:);
%delete(gcp('nocreate'))
 
AIC=2*nparams+ndata*log(f_best/ndata)
AICc=2*nparams*ndata/(ndata-nparams-1)+ndata*log(f_best/ndata)

time_for_run(1)=telapsed_eachrun(1);
for j=2:nruns
    time_for_run(j)=telapsed_eachrun(j)-telapsed_eachrun(j-1);
end

save ../parameter_estimates/semethod_wt_nM_allenzpts_cyt_evenbasis_nbas7_Acpresc_constGA53fitted_lbm6_initialt1000_innuc_test.mat 
