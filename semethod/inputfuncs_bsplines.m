function [g,basisvals]=inputfuncs_bsplines(t,theta,mdl,br,norder);

%global basis
%basisvals=eval_basis(t,basis);  % horizontal vector, 1 x b, basis function values at time t,

% For some reason, parfor doesn't like reading in br and norder as global
% variables.
basisvals = bsplineM(t, br, norder,0,0);
th_inputfunc_reshape=reshape(theta(mdl.p-mdl.nbasis*mdl.u+1:end),mdl.nbasis,mdl.u); % Matrix bxu, columns are parameters for each input function.
%th_inputfunc_reshape=reshape(pars_m.theta(nkinparams+1:end),mdl_m.nbasis,mdl_m.u);
g=basisvals*th_inputfunc_reshape;  % horizontal matrix, 1xu

end

