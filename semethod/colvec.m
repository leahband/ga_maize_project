function out = colvec(x)

% Check that x is a vector (as opposed to a matrix); if not then
% output zero, else output x as a column vector

siz = size(x);

if length(siz) == 2
    if siz(1) == 1
        out = x';
    elseif siz(2) == 1
        out = x;
    else
        error('Input is a matrix');
    end
else
    error('Input has dimension greater than 2');
end