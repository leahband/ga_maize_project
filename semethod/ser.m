classdef ser
   
   properties
      t
      x
   end
   
   methods
      
      function out = ser(t,x)
         if size(t,2) ~= size(x,2)
            error('Number of data points must be equal for t and s');
         else
            out.t = t;
            out.x = x;
         end
      end
      
      function out = addNoise(s,sigma,noiseType)

         [v numObs] = size(s.x);
         
         if nargin<2 || isempty(sigma), sigma = 1; end
         if nargin<3 || isempty(noiseType), noiseType = 0; end
         
         if numel(sigma) == 1
            sigma = sigma*ones(v,1);
         elseif numel(sigma) ~= v
            error('sigma must have numel(sigma) equal to 1 or v');
         else
            sigma = colvec(sigma);
         end
         
         switch noiseType
            case 0  % sd of noise is sigma [DEFAULT]
               xNoisy = s.x + repmat(sigma, [1 numObs]).*randn(v,numObs);
               out = ser(s.t,xNoisy);
            case 1  % sd of noise is sigma * (mean of abs obs values)  
               meanAbsSignal = mean(abs(s.x),2);
               out = addNoise(s,sigma.*meanAbsSignal,0);
         end
         
      end
      
      function [] = plot(s,varargin)
         if size(s.x,1) == size(s.x,2), s.x = s.x'; end % this corrects for 'plot's annoying and ambiguous handling of input arguments when the second argument is square
         plot(s.t,s.x,varargin{:});
      end
      
      function [] = plotOnMultipleAxes(s,varargin)
         v = size(s.x,1);
         for v_ = 1:v
            subplot(v,1,v_); hold on;
            plot(s.t,s.x(v_,:),varargin{:});
            if v_ ~= v, set(gca,'XTickLabel',[]); end
            set(gca,'Box','on');
         end
      end
      
      function out = smoothEval(s,varargin)    
         sm = smooth(s,varargin{:});
         out = sm.eval(s.t);
      end
      
      function out = smooth(s,varargin)
         defVals = {1,2,5,length(s.t)};
         [defVals{1:length(varargin)}] = varargin{:};
         lambda = defVals{1};
         derivToPenalise = defVals{2};
         order = defVals{3};
         numKnots = defVals{4};
         v = size(s.x,1);
         if numel(lambda) == 1, lambda = repmat(lambda, [1 v]); end
         
         range = [min(s.t),max(s.t)];
         knots = linspace(range(1),range(2),numKnots);
         nbasis = numKnots+order-2;
         
         datbasis = create_bspline_basis(range,nbasis,order,knots);
         
         C = zeros(nbasis,v);
         for v_ = 1:v
            xFDPar = fdPar(datbasis,derivToPenalise,lambda(v_));
            [fdobj_S,df_S,gcv_S,C(:,v_)] = smooth_basis(s.t,s.x(v_,:),xFDPar);
         end
         
         out = smth(datbasis,C);
         
      end
      
   end
   
end
