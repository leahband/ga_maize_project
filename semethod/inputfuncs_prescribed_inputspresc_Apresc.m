function z=inputfuncs_prescribed_inputspresc_Apresc(t,data,th_inputfunc_reshape,br,norder)

%load ../data_PCRenz.mat
%global data

% Use linear interpolation to provide z values at time t,
% Writing this out takes 0.011 seconds to do 100,000, using interp1 takes
% 16 seconds

datapt=floor((t-data.initialt)/50+1);
tdiff=rem(t-data.initialt,50);
if tdiff == 0
    z(1)=data.vel(datapt);
    z(2)=data.z2(datapt);
    z(3)=data.z3(datapt);
    z(4)=data.z4(datapt);
    z(6)=data.z6(datapt);
else
    z(1)=tdiff/50*(data.vel(datapt+1)-data.vel(datapt))+data.vel(datapt);
    z(2)=tdiff/50*(data.z2(datapt+1)-data.z2(datapt))+data.z2(datapt);
    z(3)=tdiff/50*(data.z3(datapt+1)-data.z3(datapt))+data.z3(datapt);
    z(4)=tdiff/50*(data.z4(datapt+1)-data.z4(datapt))+data.z4(datapt);
    z(6)=tdiff/50*(data.z6(datapt+1)-data.z6(datapt))+data.z6(datapt);
end

z(5)=data.z5;

basisvals = bsplineM(t, br, norder,0,0);
z(7:10)=basisvals*(10.^th_inputfunc_reshape);



