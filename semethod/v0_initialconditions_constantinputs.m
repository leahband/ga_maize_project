function x0=v0_initialconditions_constantinputs(theta,data)

lambda53=theta(1);lambda44=theta(2);lambda19=theta(3);lambda20=theta(4);gamma20=theta(5);gamma1=theta(6);mu29=theta(7);mu8=theta(8);

GA53init=theta(9);
GA20oxmRNA=theta(10);
GA3oxmRNA=theta(11);
GA2oxmRNA=theta(12);


dilution=data.A_smooth(1)*data.cell_length(1)*data.RER(1)/(data.A_smooth(1)*data.cell_length(1)-data.Vnuc);

GA44init=lambda53*GA20oxmRNA*GA53init/(lambda44*GA20oxmRNA+dilution);
GA19init=lambda44*GA20oxmRNA*GA44init/(lambda19*GA20oxmRNA+dilution);
GA20init=lambda19*GA19init*GA20oxmRNA/(lambda20*GA3oxmRNA+gamma20*GA2oxmRNA+dilution);
GA1init=lambda20*GA20init*GA3oxmRNA/(gamma1*GA2oxmRNA+dilution);
GA29init=gamma20*GA20init*GA2oxmRNA/(mu29*GA2oxmRNA+dilution);
GA8init=gamma1*GA1init*GA2oxmRNA/(mu8*GA2oxmRNA+dilution);

x0=[GA44init;GA19init;GA20init;GA1init;GA29init;GA8init];