function [C_nM]=v0RER0solution_bsplines(theta,xvec,basis,mdl_m)
% Solution with RER and v=0:
a=10.^theta;
th_inputfunc_reshape=reshape(a(mdl_m.nkin+1:end),mdl_m.nbasis,mdl_m.u);


lambda53=a(1);lambda44=a(2);lambda19=a(3);lambda20=a(4);gamma20=a(5);gamma1=a(6);mu29=a(7);mu8=a(8);

basisvals=eval_basis(xvec,basis);
sol_input=basisvals*th_inputfunc_reshape;

GA53=sol_input(:,1);
GA20oxmRNA=sol_input(:,2);
GA3oxmRNA=sol_input(:,3);
GA2oxmRNA=sol_input(:,4);
% 
% GA53(1)
% GA20oxmRNA(1)
% GA3oxmRNA(1)
% GA2oxmRNA(1)

GA44=lambda53/lambda44*GA53;
GA19=lambda53/lambda19*GA53;
GA20=(lambda19*GA19.*GA20oxmRNA./(lambda20*GA3oxmRNA+gamma20*GA2oxmRNA));
GA1=(lambda20*GA20.*GA3oxmRNA./(gamma1*GA2oxmRNA));
GA29=(gamma20/mu29*GA20);
GA8=(gamma1/mu8*GA1);
% 
% GA44(1)
% GA19(1)
% GA20(1)
% GA1(1)
% GA29(1)
% GA8(1)


C_nM=[GA44,GA19,GA20,GA1,GA29,GA8,GA53,GA20oxmRNA,GA3oxmRNA,GA2oxmRNA]';
