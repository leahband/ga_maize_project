% Plots model fits and data for the control, cold and drought datasets.

% Assuming different GA2ox parameters in cold and different GA20ox
% parameters in drought, we obtained the parameter estimates supplied in 
% semethod_3cases_coldGA2oxdiffdroughtGA20oxdiff_nM_allenzpts_cyt_evenbasis_nbas7_Acpresc_lbm6_initialt1000_innuc_inpaper.mat
% and the code here produces Fig 5H-Q.

% Assuming all parameters same between different conditions, we obtained
% parameter set supplied in semethod_3cases_eqparam_nM_allenzpts_cyt_evenbasis_nbas7_Acpresc_lbm6_initialt1000_innuc_inpaper.mat
% and the code here produces Fig S18.


clear
addpath ('../fdaM')
addpath ('../modelcreation_outputs')
addpath('../semethod')
 
% For different GA2ox parameters in cold and different GA20ox:
%load ../parameter_estimates/semethod_3cases_coldGA2oxdiffdroughtGA20oxdiff_nM_allenzpts_cyt_evenbasis_nbas7_Acpresc_lbm6_initialt1000_innuc_inpaper.mat 

% For all parameters the same in the three conditions:
load ../parameter_estimates/semethod_3cases_eqparam_nM_allenzpts_cyt_evenbasis_nbas7_Acpresc_lbm6_initialt1000_innuc_inpaper.mat

initialt=tvec(1);

if rem(data.xm,50)==0 && rem(max(data.pos_GAs),50)==0
    pars_m.t=[initialt:50:data.xm];
    pars_EZ.t=[data.xm:50:max(data.pos_GAs)];
    x_results=[pars_m.t(1:end-1),pars_EZ.t]; % Same as data.position
else
    pars_m.t=[initialt:50:data.xm,data.xm];
    pars_EZ.t=[data.xm:50:max(data.pos_GAs)-1,max(data.pos_GAs)];
    x_results=[pars_m.t(1:end-1),pars_EZ.t(2:end)]; % Same as data.position
end

basisvals=eval_basis(x_results,basis);


% Solve wild type model with best fit parameters:

pars_m.theta=10.^theta_best(th_pos_c1)
pars_EZ.theta=pars_m.theta;
th_inputfunc_reshape=reshape(pars_m.theta(nkinparams+1:end),mdl_m.nbasis,mdl_m.u);
sol_input_GA_c1=basisvals*th_inputfunc_reshape(:,1);
sol_input_enz_c1=basisvals*th_inputfunc_reshape(:,2:mdl_m.u);
pars_m.x0=v0_initialconditions(pars_m.theta,th_inputfunc_reshape,initialt,breaks,norder,data_c1);
[sol_m,~,~,~] = mdl_m.solveWithSensEqns_bsplines(pars_m,data_c1,breaks,norder);
pars_EZ.x0=sol_m.x(:,end);
[sol_EZ,~,~,~] = mdl_EZ.solveWithSensEqns_bsplines(pars_EZ,data_c1,breaks,norder);
sol_GAs_nM_cyt_c1=[sol_m.x(:,1:end-1),sol_EZ.x(:,1:end);sol_input_GA_c1'];

% Solve model for cold conditions:
if rem(data_c2.xm,50)==0 && rem(max(data_c2.pos_GAs),50)==0
    pars_m.t=[initialt:50:data_c2.xm];
    pars_EZ.t=[data_c2.xm:50:max(data_c2.pos_GAs)];
    x_results=[pars_m.t(1:end-1),pars_EZ.t]; % Same as data_c2.position
else
    pars_m.t=[initialt:50:data_c2.xm,data_c2.xm];
    pars_EZ.t=[data_c2.xm:50:max(data_c2.pos_GAs)-1,max(data_c2.pos_GAs)];
    x_results=[pars_m.t(1:end-1),pars_EZ.t(2:end)]; % Same as data.position
end

theta_best_c2=theta_best(th_pos_c2);
pars_m.theta=10.^theta_best_c2;
pars_EZ.theta=pars_m.theta;
th_inputfunc_reshape=reshape(pars_m.theta(nkinparams+1:end),mdl_m.nbasis,mdl_m.u);
sol_input_GA_c2=basisvals*th_inputfunc_reshape(:,1);
sol_input_enz_c2=basisvals*th_inputfunc_reshape(:,2:mdl_m.u);
pars_m.x0=v0_initialconditions(pars_m.theta,th_inputfunc_reshape,initialt,breaks,norder,data_c2);
[sol_m,~,~,~] = mdl_m.solveWithSensEqns_bsplines(pars_m,data_c2,breaks,norder);
pars_EZ.x0=sol_m.x(:,end);
[sol_EZ,~,~,~] = mdl_EZ.solveWithSensEqns_bsplines(pars_EZ,data_c2,breaks,norder);
sol_GAs_nM_cyt_c2=[sol_m.x(:,1:end-1),sol_EZ.x(:,1:end);sol_input_GA_c2'];

% Solve model for drought conditions:
if rem(data_c3.xm,50)==0 && rem(max(data_c3.pos_GAs),50)==0
    pars_m.t=[initialt:50:data_c3.xm];
    pars_EZ.t=[data_c3.xm:50:max(data_c3.pos_GAs)];
    x_results=[pars_m.t(1:end-1),pars_EZ.t]; % Same as data_c3.position
else
    pars_m.t=[initialt:50:data_c3.xm,data_c3.xm];
    pars_EZ.t=[data_c3.xm:50:max(data_c3.pos_GAs)-1,max(data_c3.pos_GAs)];
    x_results=[pars_m.t(1:end-1),pars_EZ.t(2:end)]; % Same as data.position
end

theta_best_c3=theta_best(th_pos_c3);
pars_m.theta=10.^theta_best_c3;
pars_EZ.theta=pars_m.theta;
th_inputfunc_reshape=reshape(pars_m.theta(nkinparams+1:end),mdl_m.nbasis,mdl_m.u);
sol_input_GA_c3=basisvals*th_inputfunc_reshape(:,1);
sol_input_enz_c3=basisvals*th_inputfunc_reshape(:,2:mdl_m.u);
pars_m.x0=v0_initialconditions(pars_m.theta,th_inputfunc_reshape,initialt,breaks,norder,data_c3);
[sol_m,~,~,~] = mdl_m.solveWithSensEqns_bsplines(pars_m,data_c3,breaks,norder);
pars_EZ.x0=sol_m.x(:,end);
[sol_EZ,~,~,~] = mdl_EZ.solveWithSensEqns_bsplines(pars_EZ,data_c3,breaks,norder);
sol_GAs_nM_cyt_c3=[sol_m.x(:,1:end-1),sol_EZ.x(:,1:end);sol_input_GA_c3'];

plotGAsff_compare_threedatasets(x_results,sol_GAs_nM_cyt_c1',sol_GAs_nM_cyt_c2',sol_GAs_nM_cyt_c3',sol_input_enz_c1,sol_input_enz_c2,sol_input_enz_c3,par_data_c1,par_data_c2,par_data_c3,data_c1,data_c2,data_c3)

