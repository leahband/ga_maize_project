% Produces figures showing wild-type data and model fits  given a set of estimated parameters
% (obtained using program runsemthod_wt_Acpresc_nM_cyt.m )

% Using supplied parameters in semethod_wt_nM_allenzpts_cyt_evenbasis_nbas7_Acpresc_lbm6_initialt1000_innuc_inpaper.mat
% produces figures 2B-K and 3A-C,
% Using supplied parameters in semethod_B104wt_nM_cyt_evenbasis_nbas7_Acpresc_lbm6_innuc_init1000_B104wtAdata.mat
% produces figure S10.


clear
addpath ('../fdaM')
addpath ('../semethod')
addpath ('../modelcreation_outputs')

% To run for B73 wild-type estimated parameters:
load ../parameter_estimates/semethod_wt_nM_allenzpts_cyt_evenbasis_nbas7_Acpresc_lbm6_initialt1000_innuc_inpaper.mat

% To run for B104 wild-type estimated parameters to produce S10, use instead:
%load ../parameter_estimates/semethod_B104wt_nM_cyt_evenbasis_nbas7_Acpresc_lbm6_innuc_init1000_B104wtAdata.mat

initialt=tvec(1);

if rem(data.xm,50)==0 && rem(max(data.pos_GAs),50)==0
    pars_m.t=[initialt:50:data.xm];
    pars_EZ.t=[data.xm:50:max(data.pos_GAs)];
    x_results=[pars_m.t(1:end-1),pars_EZ.t]; % Same as data.position
else
    pars_m.t=[initialt:50:data.xm,data.xm];
    pars_EZ.t=[data.xm:50:max(data.pos_GAs)-1,max(data.pos_GAs)];
    x_results=[pars_m.t(1:end-1),pars_EZ.t(2:end)]; % Same as data.position
end

% Solve model with best fit parameters:
pars_m.theta=10.^theta_best;
pars_EZ.theta=pars_m.theta;
th_inputfunc_reshape=reshape(pars_m.theta(nkinparams+1:mdl_m.p),mdl_m.nbasis,mdl_m.u);

pars_m.x0=v0_initialconditions(pars_m.theta,th_inputfunc_reshape,initialt,breaks,norder,data);
[sol_m,~,~,~] = mdl_m.solveWithSensEqns_bsplines(pars_m,data,breaks,norder);
pars_EZ.x0=sol_m.x(:,end);
[sol_EZ,~,~,~] = mdl_EZ.solveWithSensEqns_bsplines(pars_EZ,data,breaks,norder);
sol_GAs=[sol_m.x(:,1:end-1),sol_EZ.x(:,1:end)];

plotGAsandff_Apresc_cyt(x_results,sol_GAs,data,theta_best,nbasis,nkinparams,par_data,basis)

% Compare with no movement, no dilution, v=0, RER=0 solution:
sol_GAs_v0RER0=v0RER0solution_bsplines(theta_best(1:mdl_m.p),x_results,basis,mdl_m);
plotGAs_compare_nM(x_results,sol_GAs',sol_GAs_v0RER0')

% Compare with no movment, v=0 solution:
sol_GAs_v0=v0solution_bsplines(theta_best(1:mdl_m.p),x_results,basis,mdl_m,data,nkinparams,nbasis);
plotGAs_compare_nM(x_results,sol_GAs',sol_GAs_v0')


% Effect of no dilution but still movement, RER=0 and dAdx=0:
% To run for B73 wild-type estimated parameters:
load ../parameter_estimates/semethod_wt_nM_allenzpts_cyt_evenbasis_nbas7_Acpresc_lbm6_initialt1000_innuc_inpaper.mat

%load ../data/drought_cold/data_PCRenz_GAconc_nM_threecases_final_Ainterp_allenzpts.mat
%data.kappa=1;
%data.initialt=initialt;
%data.vel
data.RER=zeros(size(data.RER));
data=calculateequationexpressions_innuc_RER0dAdx0(data,data.initialt);


pars_m.theta=10.^theta_best;
%pars_m.theta(1:8)=0.001*ones(size(pars_m.theta(1:8)));
pars_EZ.theta=pars_m.theta;
pars_m.x0=v0_initialconditions(pars_m.theta,th_inputfunc_reshape,initialt,breaks,norder,data);
[sol_m_RER0dAdx0,~,~,~] = mdl_m.solveWithSensEqns_bsplines(pars_m,data,breaks,norder);
pars_EZ.x0=sol_m_RER0dAdx0.x(:,end);
[sol_EZ_RER0dAdx0,~,~,~] = mdl_EZ.solveWithSensEqns_bsplines(pars_EZ,data,breaks,norder);
sol_GAs_RER0dAdx0=[sol_m_RER0dAdx0.x(:,1:end-1),sol_EZ_RER0dAdx0.x(:,1:end)];


plotGAs_compare_nM(x_results,sol_GAs',sol_GAs_RER0dAdx0')

plotGAsandff_Apresc_cyt_wRER0v0sols(x_results,sol_GAs,data,theta_best,nbasis,nkinparams,par_data,basis,sol_GAs_RER0dAdx0,sol_GAs_v0,sol_GAs_v0RER0)



%set(gcf, 'PaperPosition', [0   0   20   15]); print -dpng -r1000 Fig2_wtfits.png
%set(gcf, 'PaperPosition', [0   0   10   10]); print -dpng -r1000 Fig2_wtfits_v0RER0.png
