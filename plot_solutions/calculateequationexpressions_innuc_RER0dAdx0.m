
function [data]=calculateequationexpressions_innuc_RER0dAdx0(data,initialt)


%figure
%plotfit_fd(data.Ac,data.pos_Ac, fdobj) 
initpos=initialt/50+1
finalpos=max(data.pos_GAs)/50+1
% 
% data.position=data.position(initpos:finalpos);
% data.RER=zeros(size(data.position));
% data.cell_length=data.cell_length(initpos:finalpos);
% data.vel=data.vel(initpos:finalpos);

data.A_smooth=data.Ac(1)*ones(size(data.position)); 
data.dAdx_smooth=zeros(size(data.position));

data.Vnuc=0.5*data.A_smooth(1)*data.cell_length(1);
data.Vmf=data.cell_length(data.xm/50+2-initpos)*data.A_smooth(data.xm/50+2-initpos);

data.z2=(data.A_smooth.*data.cell_length-data.Vnuc)./(data.A_smooth.*data.cell_length);
data.z3=data.RER+data.vel.*data.dAdx_smooth./data.A_smooth;

data.z4=data.Vmf+data.kappa*(data.A_smooth.*data.cell_length-data.Vmf);
data.z5=data.Vmf-data.Vnuc;
data.z6=data.kappa*data.cell_length.*(data.vel.*data.dAdx_smooth+data.A_smooth.*data.RER);



