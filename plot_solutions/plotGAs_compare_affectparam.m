function plotGAs_compare_affectparam(xvec,C1,C2,C3,C4,C5,C6)


GA1_case1=C1(:,4);
GA1_case2=C2(:,4);
GA1_case3=C3(:,4);
GA1_case4=C4(:,4);
GA1_case5=C5(:,4);
GA1_case6=C6(:,4);



figure
subplot(1,3,1)
plot(1/1000*xvec(1:end),GA1_case1(1:end),'b','LineWidth',2)
hold on
plot(1/1000*xvec(1:end),GA1_case2(1:end),'g','LineWidth',2)
%title('GA_1')
legend('Control','Constant GA_{53}')
xlabel('Distance (mm)','Interpreter','tex','FontSize',10)
ylabel('GA1 conc (nM)','Interpreter','tex','FontSize',10)
ylim([0 0.7])

box off

subplot(1,3,2)
plot(1/1000*xvec(1:end),GA1_case1(1:end),'b','LineWidth',2)
hold on
plot(1/1000*xvec(1:end),GA1_case3(1:end),'m','LineWidth',2)
%title('GA_1')
legend('Control','Constant enzymes')
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('GA1 conc (nM)','Interpreter','latex','FontSize',10)
ylim([0 0.7])

box off

subplot(1,3,3)
plot(1/1000*xvec(1:end),GA1_case1(1:end),'b','LineWidth',2)
hold on
plot(1/1000*xvec(1:end),GA1_case4(1:end),'g','LineWidth',2)
plot(1/1000*xvec(1:end),GA1_case5(1:end),'m','LineWidth',2)
plot(1/1000*xvec(1:end),GA1_case6(1:end),'c','LineWidth',2)
%title('GA_1')
legend('Control','Double GA20ox rates','Double GA3ox rates','Double GA2ox rates')
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('GA1 conc (nM)','Interpreter','latex','FontSize',10)
ylim([0 1.2])
box off
