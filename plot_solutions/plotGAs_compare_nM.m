function plotGAs_compare_nM(xvec,C1,C2)

GA44_case1=C1(:,1);
GA19_case1=C1(:,2);
GA20_case1=C1(:,3);
GA1_case1=C1(:,4);
GA29_case1=C1(:,5);
GA8_case1=C1(:,6);

GA44_case2=C2(:,1);
GA19_case2=C2(:,2);
GA20_case2=C2(:,3);
GA1_case2=C2(:,4);
GA29_case2=C2(:,5);
GA8_case2=C2(:,6);


figure
subplot(2,3,1)
plot(1/1000*xvec(1:end),GA44_case1(1:end),'r','LineWidth',1.4)
hold on
plot(1/1000*xvec(1:end),GA44_case2(1:end),'--r','LineWidth',1.4)
title('GA_{44}')
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('Concentration (nM)','Interpreter','latex','FontSize',16)
subplot(2,3,2)
plot(1/1000*xvec(1:end),GA19_case1(1:end),'g','LineWidth',1.4)
hold on
plot(1/1000*xvec(1:end),GA19_case2(1:end),'--g','LineWidth',1.4)
title('GA_{19}')
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('Concentration (nM)','Interpreter','latex','FontSize',16)
subplot(2,3,3)
plot(1/1000*xvec(1:end),GA20_case1(1:end),'m','LineWidth',1.4)
hold on
plot(1/1000*xvec(1:end),GA20_case2(1:end),'--m','LineWidth',1.4)
title('GA_{20}')
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('Concentration (nM)','Interpreter','latex','FontSize',16)
subplot(2,3,4)
plot(1/1000*xvec(1:end),GA1_case1(1:end),'k','LineWidth',1.4)
hold on
plot(1/1000*xvec(1:end),GA1_case2(1:end),'--k','LineWidth',1.4)
title('GA1')
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('Concentration (nM)','Interpreter','latex','FontSize',16)
subplot(2,3,5)
plot(1/1000*xvec(1:end),GA29_case1(1:end),'b','LineWidth',1.4)
hold on
plot(1/1000*xvec(1:end),GA29_case2(1:end),'--b','LineWidth',1.4)
title('GA_{29}')
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('Concentration (nM)','Interpreter','latex','FontSize',16)
subplot(2,3,6)
plot(1/1000*xvec(1:end),GA8_case1(1:end),'c','LineWidth',1.4)
hold on
plot(1/1000*xvec(1:end),GA8_case2(1:end),'--c','LineWidth',1.4)
title('GA_{8}')
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('Concentration (nM)','Interpreter','latex','FontSize',16)
%legend1=legend('GA44','GA19','GA20','GA1','GA29','GA8')
%set(legend1,'Interpreter','latex','Location','SouthEast','FontSize',24)
%box('off')
%set(gca,'FontSize',20)


figure
plot(1/1000*xvec(1:end),GA1_case1(1:end),'-k','LineWidth',1.4)
hold on
title('GA1')
plot(1/1000*xvec(1:end),GA1_case2(1:end),'--k','LineWidth',1.4)
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('Concentration (nM)','Interpreter','latex','FontSize',16)
box off