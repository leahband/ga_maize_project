% Plots model fits and data for the control, wild type (B73) supposing that
%  (I) GA20ox rate parameters are halved or doubled (Figs S12, 3F)
% (II) GA3ox rate parameters are halved or doubled (Figs S13, 3F)
% (III)  GA2ox rate parameters are halved or doubled (Figs S14, 3F)
% or (IV) the GA53 or enzyme inputs are constant and prescribed as mean of data (Figs 3GH).

% Uses parameter estimates supplied in the parameter_estimates subfolder.
% Code to estimate these parameters in these cases is provided in semethod.

clear
addpath ('../fdaM')
addpath ('../modelcreation_outputs')
addpath('../semethod')

load ../parameter_estimates/semethod_wt_nM_allenzpts_cyt_evenbasis_nbas7_Acpresc_lbm6_initialt1000_innuc_inpaper.mat 

initialt=tvec(1)

if rem(data.xm,50)==0 && rem(max(data.pos_GAs),50)==0
    pars_m.t=[initialt:50:data.xm];
    pars_EZ.t=[data.xm:50:max(data.pos_GAs)];
    x_results=[pars_m.t(1:end-1),pars_EZ.t]; % Same as data.position
else
    pars_m.t=[initialt:50:data.xm,data.xm];
    pars_EZ.t=[data.xm:50:max(data.pos_GAs)-1,max(data.pos_GAs)];
    x_results=[pars_m.t(1:end-1),pars_EZ.t(2:end)]; % Same as data.position
end


pars_m.theta=10.^theta_best;
pars_EZ.theta=pars_m.theta;
th_inputfunc_reshape=reshape(pars_m.theta(nkinparams+1:mdl_m.p),mdl_m.nbasis,mdl_m.u);

pars_m.x0=v0_initialconditions(pars_m.theta,th_inputfunc_reshape,initialt,breaks,norder,data);
[sol_m,~,~,~] = mdl_m.solveWithSensEqns_bsplines(pars_m,data,breaks,norder);
pars_EZ.x0=sol_m.x(:,end);
[sol_EZ,~,~,~] = mdl_EZ.solveWithSensEqns_bsplines(pars_EZ,data,breaks,norder);
sol_GAs=[sol_m.x(:,1:end-1),sol_EZ.x(:,1:end)];

plotGAsandff_Apresc_cyt(x_results,sol_GAs,data,theta_best,nbasis,nkinparams,par_data,basis)

%GA20ox doubled:
pars_m.theta=10.^theta_best;
pars_m.theta(1:3)=2*pars_m.theta(1:3);
pars_EZ.theta=pars_m.theta;
th_inputfunc_reshape=reshape(pars_m.theta(nkinparams+1:mdl_m.p),mdl_m.nbasis,mdl_m.u);

pars_m.x0=v0_initialconditions(pars_m.theta,th_inputfunc_reshape,initialt,breaks,norder,data);
[sol_m,~,~,~] = mdl_m.solveWithSensEqns_bsplines(pars_m,data,breaks,norder);
pars_EZ.x0=sol_m.x(:,end);
[sol_EZ,~,~,~] = mdl_EZ.solveWithSensEqns_bsplines(pars_EZ,data,breaks,norder);
sol_GAs_doubleGA20ox=[sol_m.x(:,1:end-1),sol_EZ.x(:,1:end)];


%GA20ox halved:
pars_m.theta=10.^theta_best;
pars_m.theta(1:3)=0.5*pars_m.theta(1:3);
pars_EZ.theta=pars_m.theta;
th_inputfunc_reshape=reshape(pars_m.theta(nkinparams+1:mdl_m.p),mdl_m.nbasis,mdl_m.u);

pars_m.x0=v0_initialconditions(pars_m.theta,th_inputfunc_reshape,initialt,breaks,norder,data);
[sol_m,~,~,~] = mdl_m.solveWithSensEqns_bsplines(pars_m,data,breaks,norder);
pars_EZ.x0=sol_m.x(:,end);
[sol_EZ,~,~,~] = mdl_EZ.solveWithSensEqns_bsplines(pars_EZ,data,breaks,norder);
sol_GAs_halfGA20ox=[sol_m.x(:,1:end-1),sol_EZ.x(:,1:end)];


plotGAs_compare_nM_threecases(x_results,sol_GAs',sol_GAs_doubleGA20ox',sol_GAs_halfGA20ox')
legend('Control','GA20ox rates doubled','GA20ox rates halved')

%GA3ox doubled:
pars_m.theta=10.^theta_best;
pars_m.theta(4)=2*pars_m.theta(4);
pars_EZ.theta=pars_m.theta;
th_inputfunc_reshape=reshape(pars_m.theta(nkinparams+1:mdl_m.p),mdl_m.nbasis,mdl_m.u);

pars_m.x0=v0_initialconditions(pars_m.theta,th_inputfunc_reshape,initialt,breaks,norder,data);
[sol_m,~,~,~] = mdl_m.solveWithSensEqns_bsplines(pars_m,data,breaks,norder);
pars_EZ.x0=sol_m.x(:,end);
[sol_EZ,~,~,~] = mdl_EZ.solveWithSensEqns_bsplines(pars_EZ,data,breaks,norder);
sol_GAs_doubleGA3ox=[sol_m.x(:,1:end-1),sol_EZ.x(:,1:end)];


%GA3ox halved:
pars_m.theta=10.^theta_best;
pars_m.theta(4)=0.5*pars_m.theta(4);
pars_EZ.theta=pars_m.theta;
th_inputfunc_reshape=reshape(pars_m.theta(nkinparams+1:mdl_m.p),mdl_m.nbasis,mdl_m.u);

pars_m.x0=v0_initialconditions(pars_m.theta,th_inputfunc_reshape,initialt,breaks,norder,data);
[sol_m,~,~,~] = mdl_m.solveWithSensEqns_bsplines(pars_m,data,breaks,norder);
pars_EZ.x0=sol_m.x(:,end);
[sol_EZ,~,~,~] = mdl_EZ.solveWithSensEqns_bsplines(pars_EZ,data,breaks,norder);
sol_GAs_halfGA3ox=[sol_m.x(:,1:end-1),sol_EZ.x(:,1:end)];

plotGAs_compare_nM_threecases(x_results,sol_GAs',sol_GAs_doubleGA3ox',sol_GAs_halfGA3ox')
legend('Control','GA3ox rates doubled','GA3ox rates halved')

%GA2ox doubled:
pars_m.theta=10.^theta_best;
pars_m.theta(5:8)=2*pars_m.theta(5:8);
pars_EZ.theta=pars_m.theta;
th_inputfunc_reshape=reshape(pars_m.theta(nkinparams+1:mdl_m.p),mdl_m.nbasis,mdl_m.u);

pars_m.x0=v0_initialconditions(pars_m.theta,th_inputfunc_reshape,initialt,breaks,norder,data);
[sol_m,~,~,~] = mdl_m.solveWithSensEqns_bsplines(pars_m,data,breaks,norder);
pars_EZ.x0=sol_m.x(:,end);
[sol_EZ,~,~,~] = mdl_EZ.solveWithSensEqns_bsplines(pars_EZ,data,breaks,norder);
sol_GAs_doubleGA2ox=[sol_m.x(:,1:end-1),sol_EZ.x(:,1:end)];


%GA2ox halved:
pars_m.theta=10.^theta_best;
pars_m.theta(5:8)=0.5*pars_m.theta(5:8);
pars_EZ.theta=pars_m.theta;
th_inputfunc_reshape=reshape(pars_m.theta(nkinparams+1:mdl_m.p),mdl_m.nbasis,mdl_m.u);

pars_m.x0=v0_initialconditions(pars_m.theta,th_inputfunc_reshape,initialt,breaks,norder,data);
[sol_m,~,~,~] = mdl_m.solveWithSensEqns_bsplines(pars_m,data,breaks,norder);
pars_EZ.x0=sol_m.x(:,end);
[sol_EZ,~,~,~] = mdl_EZ.solveWithSensEqns_bsplines(pars_EZ,data,breaks,norder);
sol_GAs_halfGA2ox=[sol_m.x(:,1:end-1),sol_EZ.x(:,1:end)];

plotGAs_compare_nM_threecases(x_results,sol_GAs',sol_GAs_doubleGA2ox',sol_GAs_halfGA2ox')
legend('Control','GA2ox rates doubled','GA2ox rates halved')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%55
% Solutions assuming enzyme input functions are constant (spatially uniform) and GA53 fitted using bsplines:

load ../parameter_estimates/semethod_wt_nM_allenzpts_cyt_evenbasis_nbas7_Acpresc_constenzfitted_lbm6_init1000_innuc.mat mdl_m mdl_EZ

pars_m.theta=zeros(1,mdl_m.p);
pars_m.theta(1:mdl_m.nkin)=10.^theta_best(1:mdl_m.nkin);
% Prescribe parameters for the constant enzyme levels as the mean of the
% data.
pars_m.theta(mdl_m.nkin+1)=mean(par_data(7*ntime+1:7*ntime+ntime_enz));
pars_m.theta(mdl_m.nkin+2)=mean(par_data(7*ntime+ntime_enz+1:7*ntime+2*ntime_enz));
pars_m.theta(mdl_m.nkin+3)=mean(par_data(7*ntime+2*ntime_enz+1:end));

pars_m.theta(mdl_m.nkin+3+1:mdl_m.nkin+3+nbasis)=10.^theta_best(8+1:8+nbasis);
pars_EZ.theta=pars_m.theta;


th_inputfunc=pars_m.theta(nkinparams+4:end)';
pars_m.x0=v0_initialconditions_constenz(pars_m.theta,th_inputfunc,initialt,breaks,norder,data);

% Solve for x and dxdtheta using sensitivity equations:

[sol_m,~,~,~] = mdl_m.solveWithSensEqns_bsplines(pars_m,data,breaks,norder);
pars_EZ.x0=sol_m.x(:,end);
[sol_EZ,~,~,~] = mdl_EZ.solveWithSensEqns_bsplines(pars_EZ,data,breaks,norder);
sol_GAs_constenz=[sol_m.x(:,1:end-1),sol_EZ.x(:,1:end)];


plotGAsandff_Apresc_cyt_constenz(x_results,sol_GAs_constenz,data,log10(pars_m.theta),par_data,basis,th_inputfunc)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%55
% Solutions assuming GA53 function is constant (spatially uniform) and enzymes fitted using bsplines:

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%55

load ../parameter_estimates/semethod_wt_nM_allenzpts_cyt_evenbasis_nbas7_Acpresc_constGA53fitted_lbm6_initialt1000_innuc.mat mdl_m mdl_EZ
pars_m.theta=zeros(1,mdl_m.p);
pars_m.theta(1:mdl_m.nkin)=10.^theta_best(1:mdl_m.nkin);
pars_m.theta(mdl_m.nkin+1)=mean(par_data(6*ntime+1:7*ntime));
pars_m.theta(mdl_m.nkin+1+1:mdl_m.nkin+1+3*nbasis)=10.^theta_best(8+nbasis+1:8+4*nbasis);
pars_EZ.theta=pars_m.theta;

th_inputfunc_reshape=reshape(pars_m.theta(mdl_m.nkin+2:end),mdl_m.nbasis,mdl_m.u);
pars_m.x0=v0_initialconditions_constGA53(pars_m.theta,th_inputfunc_reshape,initialt,breaks,norder,data);

% Solve for x and dxdtheta using sensitivity equations:
[sol_m,~,~,~] = mdl_m.solveWithSensEqns_bsplines(pars_m,data,breaks,norder);
pars_EZ.x0=sol_m.x(:,end);
[sol_EZ,~,~,~] = mdl_EZ.solveWithSensEqns_bsplines(pars_EZ,data,breaks,norder);
sol_GAs_constGA53=[sol_m.x(:,1:end-1),sol_EZ.x(:,1:end)];

plotGAsandff_Apresc_cyt_constGA53(x_results,sol_GAs_constGA53,data,theta_best,par_data,basis,th_inputfunc_reshape)

plotGAs_compare_affectparam(x_results,sol_GAs',sol_GAs_constGA53',sol_GAs_constenz',sol_GAs_doubleGA20ox',sol_GAs_doubleGA3ox',sol_GAs_doubleGA2ox')

set(gcf, 'PaperPosition', [0   0   15   5]);