% Plots model fits and data for the B73 wild-type and GA20ox datasets.

% First, assuming both Maize and Arab GA20ox have the same parameters,
% produces Fig S17, using supplied parameter estimates in 
% semethod_B104wtandGA20oxOE_nM_cyt_evenbasis_nbas7_Acpresc_lbm6_innuc_init1000_B104wtAdata_n50

% Then assuming that Maize and Arab GA20ox have different parameters,
% produces Fig 4, using supplied parameter estimates in 
% semethod_B104wtandGA20oxOE_Arabdiff_nM_cyt_evenbasis_nbas7_Acpresc_lbm6_innuc_init1000_B104wtAdata_n50.mat
clear
addpath ('../fdaM')
addpath ('../modelcreation_outputs')
addpath('../semethod')

% Plot solutions assuming both Maize and Arab GA20ox have same parameters:

load ../parameter_estimates/semethod_B104wtandGA20oxOE_nM_cyt_evenbasis_nbas7_Acpresc_lbm6_innuc_init1000_B104wtAdata_n50.mat


initialt=tvec(1)

% Solve wild type model with best fit parameters, fitting both datasets:
if rem(data_c1.xm,50)==0 && rem(max(data_c1.pos_GAs),50)==0
    pars_m.t=[initialt:50:data_c1.xm];
    pars_EZ.t=[data_c1.xm:50:max(data_c1.pos_GAs)];
    x_results=[pars_m.t(1:end-1),pars_EZ.t]; % Same as data.position
else
    pars_m.t=[initialt:50:data_c1.xm,data_c1.xm];
    pars_EZ.t=[data_c1.xm:50:max(data_c1.pos_GAs)-1,max(data_c1.pos_GAs)];
    x_results=[pars_m.t(1:end-1),pars_EZ.t(2:end)]; % Same as data.position
end

basisvals=eval_basis(x_results,basis);

pars_m.theta=10.^theta_best(1:mdl_m.p);
pars_EZ.theta=pars_m.theta;
th_inputfunc_reshape=reshape(pars_m.theta(nkinparams+1:end),mdl_m.nbasis,mdl_m.u);
sol_input_GA_wt=basisvals*th_inputfunc_reshape(:,1);
sol_input_enz_wt=basisvals*th_inputfunc_reshape(:,2:mdl_m.u);
pars_m.x0=v0_initialconditions(pars_m.theta,th_inputfunc_reshape,initialt,breaks,norder,data_c1);
[sol_m,~,~,~] = mdl_m.solveWithSensEqns_bsplines(pars_m,data_c1,breaks,norder);
pars_EZ.x0=sol_m.x(:,end);
[sol_EZ,~,~,~] = mdl_EZ.solveWithSensEqns_bsplines(pars_EZ,data_c1,breaks,norder);
sol_GAs_nM_cyt_wt=[sol_m.x(:,1:end-1),sol_EZ.x(:,1:end);sol_input_GA_wt'];


% Solve GA20ox solution with best fit parameters:
if rem(data_c2.xm,50)==0 && rem(max(data_c2.pos_GAs),50)==0
    pars_m.t=[initialt:50:data_c2.xm];
    pars_EZ.t=[data_c2.xm:50:max(data_c2.pos_GAs)];
    x_results=[pars_m.t(1:end-1),pars_EZ.t]; % Same as data_c2.position
else
    pars_m.t=[initialt:50:data_c2.xm,data_c2.xm];
    pars_EZ.t=[data_c2.xm:50:max(data_c2.pos_GAs)-1,max(data_c2.pos_GAs)];
    x_results=[pars_m.t(1:end-1),pars_EZ.t(2:end)]; % Same as data.position
end

theta_best_GA20ox=theta_best([1:nkinparams,mdl_m.p+1:end])
pars_m.theta=10.^theta_best_GA20ox;
pars_EZ.theta=pars_m.theta;
th_inputfunc_reshape=reshape(pars_m.theta(nkinparams+1:end),mdl_m.nbasis,mdl_m.u);
sol_input_GA_GA20ox=basisvals*th_inputfunc_reshape(:,1);
sol_input_enz_GA20ox=basisvals*th_inputfunc_reshape(:,2:mdl_m.u);
pars_m.x0=v0_initialconditions(pars_m.theta,th_inputfunc_reshape,initialt,breaks,norder,data_c2);
[sol_m,~,~,~] = mdl_m.solveWithSensEqns_bsplines(pars_m,data_c2,breaks,norder);
pars_EZ.x0=sol_m.x(:,end);
[sol_EZ,~,~,~] = mdl_EZ.solveWithSensEqns_bsplines(pars_EZ,data_c2,breaks,norder);
sol_GAs_nM_cyt_GA20ox=[sol_m.x(:,1:end-1),sol_EZ.x(:,1:end);sol_input_GA_GA20ox'];


plotGAsff_compare_twodatasets(x_results,sol_GAs_nM_cyt_wt',sol_GAs_nM_cyt_GA20ox',sol_input_enz_wt,sol_input_enz_GA20ox,par_data_c1,par_data_c2,data_c1,data_c2)


%% Fitting wildtype and GA20oxOE assuming both Maize and Arab GA20ox have different parameters:
load ../parameter_estimates/semethod_B104wtandGA20oxOE_Arabdiff_nM_cyt_evenbasis_nbas7_Acpresc_lbm6_innuc_init1000_B104wtAdata_n50.mat

initialt=data_c1.initialt;
if rem(data_c1.xm,50)==0 && rem(max(data_c1.pos_GAs),50)==0
    pars_wt_m.t=[initialt:50:data_c1.xm];
    pars_wt_EZ.t=[data_c1.xm:50:max(data_c1.pos_GAs)];
    x_results=[pars_wt_m.t(1:end-1),pars_wt_EZ.t]; % Same as data.position
else
    pars_wt_m.t=[initialt:50:data_c1.xm,data_c1.xm];
    pars_wt_EZ.t=[data_c1.xm:50:max(data_c1.pos_GAs)-1,max(data_c1.pos_GAs)];
    x_results=[pars_wt_m.t(1:end-1),pars_wt_EZ.t(2:end)]; % Same as data.position
end

pars_wt_m.theta=10.^[theta_best(1:8),theta_best(nkin_GA20ox+1:nkin_GA20ox+nbasis*mdl_wt_m.u)];
pars_wt_EZ.theta=pars_wt_m.theta;
size(pars_wt_m.theta(nkin_wt+1:end))
nbasis
mdl_wt_m.u
th_inputfunc_reshape=reshape(pars_wt_m.theta(nkin_wt+1:end),nbasis,mdl_wt_m.u);
sol_input_GA_wt=basisvals*th_inputfunc_reshape(:,1);
sol_input_enz_wt=basisvals*th_inputfunc_reshape(:,2:mdl_wt_m.u);
pars_wt_m.x0=v0_initialconditions(pars_wt_m.theta,th_inputfunc_reshape,initialt,breaks,norder,data_c1);
[sol_m,~,~,~] = mdl_wt_m.solveWithSensEqns_bsplines(pars_wt_m,data_c1,breaks,norder);
pars_wt_EZ.x0=sol_m.x(:,end);
[sol_EZ,~,~,~] = mdl_wt_EZ.solveWithSensEqns_bsplines(pars_wt_EZ,data_c1,breaks,norder);
sol_GAs_nM_cyt_wt=[sol_m.x(:,1:end-1),sol_EZ.x(:,1:end);sol_input_GA_wt'];


% Solve GA20ox solution with best fit parameters:
if rem(data_c2.xm,50)==0 && rem(max(data_c2.pos_GAs),50)==0
    pars_GA20ox_m.t=[initialt:50:data_c2.xm];
    pars_GA20ox_EZ.t=[data_c2.xm:50:max(data_c2.pos_GAs)];
    x_results=[pars_GA20ox_m.t(1:end-1),pars_GA20ox_EZ.t]; % Same as data_c2.position
else
    pars_GA20ox_m.t=[initialt:50:data_c2.xm,data_c2.xm];
    pars_GA20ox_EZ.t=[data_c2.xm:50:max(data_c2.pos_GAs)-1,max(data_c2.pos_GAs)];
    x_results=[pars_GA20ox_m.t(1:end-1),pars_GA20ox_EZ.t(2:end)]; % Same as data.position
end

theta_best_GA20ox=theta_best([1:nkin_GA20ox,nkin_GA20ox+u_wt*nbasis+1:end]);
pars_GA20ox_m.theta=10.^theta_best_GA20ox;
pars_GA20ox_EZ.theta=pars_GA20ox_m.theta;
th_inputfunc_reshape=reshape(pars_GA20ox_m.theta(nkin_GA20ox+1:end),nbasis,mdl_GA20ox_m.u);
sol_input_GA_GA20ox=basisvals*th_inputfunc_reshape(:,1);
sol_input_enz_GA20ox=basisvals*th_inputfunc_reshape(:,2:mdl_GA20ox_m.u);
pars_GA20ox_m.x0=v0_initialconditions(pars_GA20ox_m.theta,th_inputfunc_reshape,initialt,breaks,norder,data_c2);
[sol_m,~,~,~] = mdl_GA20ox_m.solveWithSensEqns_bsplines(pars_GA20ox_m,data_c2,breaks,norder);
pars_GA20ox_EZ.x0=sol_m.x(:,end);
[sol_EZ,~,~,~] = mdl_GA20ox_EZ.solveWithSensEqns_bsplines(pars_GA20ox_EZ,data_c2,breaks,norder);
sol_GAs_nM_cyt_GA20ox=[sol_m.x(:,1:end-1),sol_EZ.x(:,1:end);sol_input_GA_GA20ox'];

%plotGAsandff_Apresc_cyt_Arabdiff(x_results,sol_GAs_nM_cyt_GA20ox,data_c2,theta_best_GA20ox,nbasis,nkin_GA20ox,par_data_c2,basis)
%PLots use values of DZ length given in Nelissen 2012 (SI table) - distance
%between the leaf base and most distal mitotic figure observed using DAPI
%staining. 
data_c1.xm=1000*19.1;
data_c2.xm=1000*26.5;
plotGAsff_compare_twodatasets_Arabdiff(x_results,sol_GAs_nM_cyt_wt',sol_GAs_nM_cyt_GA20ox',sol_input_enz_wt,sol_input_enz_GA20ox,par_data_c1,par_data_c2,data_c1,data_c2)

