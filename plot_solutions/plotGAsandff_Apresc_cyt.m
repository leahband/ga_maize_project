function plotGAsandff_Apresc_cyt(xvec,C,data,theta,nbasis,nkinparams,par_data,basis)

ntime=length(data.pos_GAs);
ntime_enz=length(data.pos_enz);

GA44data=par_data(1:ntime);
GA19data=par_data(ntime+1:2*ntime);
GA20data=par_data(2*ntime+1:3*ntime);
GA1data=par_data(3*ntime+1:4*ntime);
GA29data=par_data(4*ntime+1:5*ntime);
GA8data=par_data(5*ntime+1:6*ntime);
GA53data=par_data(6*ntime+1:7*ntime);
GA20oxdata=par_data(7*ntime+1:7*ntime+ntime_enz);
GA3oxdata=par_data(7*ntime+ntime_enz+1:7*ntime+2*ntime_enz);
GA2oxdata=par_data(7*ntime+2*ntime_enz+1:end);

C=C';
GA44=C(:,1);
GA19=C(:,2);
GA20=C(:,3);
GA1=C(:,4);
GA29=C(:,5);
GA8=C(:,6);


a=10.^theta;
GA53coeffs=a(nkinparams+1:nkinparams+nbasis)';
GA20oxcoeffs=a(nkinparams+nbasis+1:nkinparams+2*nbasis)';
GA3oxcoeffs=a(nkinparams+2*nbasis+1:nkinparams+3*nbasis)';
GA2oxcoeffs=a(nkinparams+3*nbasis+1:nkinparams+4*nbasis)';

basisvals=eval_basis(xvec,basis);
GA53=basisvals*GA53coeffs;
GA20oxmRNA=basisvals*GA20oxcoeffs;
GA3oxmRNA=basisvals*GA3oxcoeffs;
GA2oxmRNA=basisvals*GA2oxcoeffs;


figure

subplot(3,4,1)

plot(1/1000*xvec,GA53,'-b','LineWidth',2)
hold on
plot(1/1000*data.pos_GAs,GA53data,'*r','LineWidth',2)
%title('GA53 (amol/cm)')
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('Conc (nM)','Interpreter','latex','FontSize',10)
title('GA_{53}')
box off

subplot(3,4,2)

plot(1/1000*xvec,GA20oxmRNA,'-b','LineWidth',2)
hold on
%errorbar(1/1000*data.pos_enz,data.GA20oxmRNA_measured,data.GA20oxmRNA_measured_se,'*g','LineWidth',2)
plot(1/1000*data.pos_enz,GA20oxdata,'*r','LineWidth',2)
%title('GA20ox mRNA')
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('Transcript level','Interpreter','latex','FontSize',10)
title('GA20ox')
box off

subplot(3,4,3)

plot(1/1000*xvec,GA3oxmRNA,'-b','LineWidth',2)
hold on
%errorbar(1/1000*data.pos_enz,data.GA3oxmRNA_measured,data.GA3oxmRNA_measured_se,'*m','LineWidth',2)
plot(1/1000*data.pos_enz,GA3oxdata,'*r','LineWidth',2)
%title('GA3ox mRNA')
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('Transcript level','Interpreter','latex','FontSize',10)
title('GA3ox')
box off

subplot(3,4,4)

plot(1/1000*xvec,GA2oxmRNA,'-b','LineWidth',2)
hold on
%errorbar(1/1000*data.pos_enz,data.GA2oxmRNA_measured,data.GA2oxmRNA_measured_se,'*k','LineWidth',2)
plot(1/1000*data.pos_enz,GA2oxdata,'*r','LineWidth',2)
%title('GA2ox mRNA')
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('Transcript level','Interpreter','latex','FontSize',10)
title('GA2ox')
box off


subplot(3,4,5)

size(xvec)
size(GA44)
plot(1/1000*xvec(1:end),GA44(1:end),'-b','LineWidth',2)
hold on
plot(1/1000*data.pos_GAs,GA44data,'*r','LineWidth',2)
%title('GA44')
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('Conc (nM)','Interpreter','latex','FontSize',10)
title('GA_{44}')
box off

subplot(3,4,6)

plot(1/1000*xvec(1:end),GA19(1:end),'-b','LineWidth',2)
hold on
plot(1/1000*data.pos_GAs,GA19data,'*r','LineWidth',2)
%title('GA19')
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('Conc (nM)','Interpreter','latex','FontSize',10)
title('GA_{19}')
box off

subplot(3,4,7)

plot(1/1000*xvec(1:end),GA20(1:end),'-b','LineWidth',2)
hold on
plot(1/1000*data.pos_GAs,GA20data,'*r','LineWidth',2)
%title('GA20')
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('Conc (nM)','Interpreter','latex','FontSize',10)
title('GA_{20}')
box off

subplot(3,4,8)

plot(1/1000*xvec(1:end),GA1(1:end),'-b','LineWidth',2)
hold on
plot(1/1000*data.pos_GAs,GA1data,'*r','LineWidth',2)
plot(1/1000*data.xm*ones(1,11),[0:0.1:1],'k--','LineWidth',2);
%title('GA1')
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('Conc (nM)','Interpreter','latex','FontSize',10)
title('GA_1')
box off

subplot(3,4,9)

plot(1/1000*xvec(1:end),GA29(1:end),'-b','LineWidth',2)
hold on
plot(1/1000*data.pos_GAs,GA29data,'*r','LineWidth',2)
%title('GA29')
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('Conc (nM)','Interpreter','latex','FontSize',10)
title('GA_{29}')
box off

subplot(3,4,10)

plot(1/1000*xvec(1:end),GA8(1:end),'-b','LineWidth',2)
hold on
plot(1/1000*data.pos_GAs,GA8data,'*r','LineWidth',2)
%title('GA8')
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('Conc (nM)','Interpreter','latex','FontSize',10)
title('GA_8')
box off
%legend1=legend('GA44','GA19','GA20','GA1','GA29','GA8')
%set(legend1,'Interpreter','latex','Location','SouthEast','FontSize',24)
%box('off')
%set(gca,'FontSize',20)
