function [C_nM]=v0solution_bsplines(theta,xvec,basis,mdl_m,data,nkinparams,nbasis)
% Solution with RER and v=0:
a=10.^theta;

%th_inputfunc_reshape=reshape(a(mdl_m.nkin+1:end),mdl_m.nbasis,mdl_m.u);

lambda53=a(1);lambda44=a(2);lambda19=a(3);lambda20=a(4);gamma20=a(5);gamma1=a(6);mu29=a(7);mu8=a(8);

%basisvals=eval_basis(xvec,basis);
%sol_input=basisvals*(10.^th_inputfunc_reshape);
% 
% GA53=sol_input(:,1);
% GA20oxmRNA=sol_input(:,2);
% GA3oxmRNA=sol_input(:,3);
% GA2oxmRNA=sol_input(:,4);

GA53coeffs=a(nkinparams+1:nkinparams+nbasis)';
GA20oxcoeffs=a(nkinparams+nbasis+1:nkinparams+2*nbasis)';
GA3oxcoeffs=a(nkinparams+2*nbasis+1:nkinparams+3*nbasis)';
GA2oxcoeffs=a(nkinparams+3*nbasis+1:nkinparams+4*nbasis)';

basisvals=eval_basis(xvec,basis);
GA53=basisvals*GA53coeffs;
GA20oxmRNA=basisvals*GA20oxcoeffs;
GA3oxmRNA=basisvals*GA3oxcoeffs;
GA2oxmRNA=basisvals*GA2oxcoeffs;

 
dilution=data.A_smooth'.*data.cell_length'.*data.RER'./(data.A_smooth'.*data.cell_length'-data.Vnuc);


GA44=lambda53*GA20oxmRNA.*GA53./(lambda44*GA20oxmRNA+dilution);
GA19=lambda44*GA20oxmRNA.*GA44./(lambda19*GA20oxmRNA+dilution);
GA20=lambda19*GA19.*GA20oxmRNA./(lambda20*GA3oxmRNA+gamma20*GA2oxmRNA+dilution);
GA1=lambda20*GA20.*GA3oxmRNA./(gamma1*GA2oxmRNA+dilution);
GA29=gamma20*GA20.*GA2oxmRNA./(mu29*GA2oxmRNA+dilution);
GA8=gamma1*GA1.*GA2oxmRNA./(mu8*GA2oxmRNA+dilution);


C_nM=[GA44,GA19,GA20,GA1,GA29,GA8,GA53,GA20oxmRNA,GA3oxmRNA,GA2oxmRNA]';
