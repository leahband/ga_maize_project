function plotGAsff_compare_twodatasets_Arabdiff(xvec,C1,C2,C1_enz,C2_enz,par_data_case1,par_data_case2,data1,data2)

GA44_case1=C1(:,1);
GA19_case1=C1(:,2);
GA20_case1=C1(:,3);
GA1_case1=C1(:,4);
GA29_case1=C1(:,5);
GA8_case1=C1(:,6);
GA53_case1=C1(:,7);

GA44_case2=C2(:,1);
GA19_case2=C2(:,2);
GA20_case2=C2(:,3);
GA1_case2=C2(:,4);
GA29_case2=C2(:,5);
GA8_case2=C2(:,6);
GA53_case2=C2(:,7);

GA20ox_case1=C1_enz(:,1);
GA3ox_case1=C1_enz(:,2);
GA2ox_case1=C1_enz(:,3);

GA20ox_Maize_case2=C2_enz(:,1);
GA3ox_case2=C2_enz(:,2);
GA2ox_case2=C2_enz(:,3);
GA20ox_Arab_case2=C2_enz(:,4);

ntime=length(data1.pos_GAs);

GA44data_case1=par_data_case1(1:ntime);
GA19data_case1=par_data_case1(ntime+1:2*ntime);
GA20data_case1=par_data_case1(2*ntime+1:3*ntime);
GA1data_case1=par_data_case1(3*ntime+1:4*ntime);
GA29data_case1=par_data_case1(4*ntime+1:5*ntime);
GA8data_case1=par_data_case1(5*ntime+1:6*ntime);
GA53data_case1=par_data_case1(6*ntime+1:7*ntime);

GA44data_case2=par_data_case2(1:ntime);
GA19data_case2=par_data_case2(ntime+1:2*ntime);
GA20data_case2=par_data_case2(2*ntime+1:3*ntime);
GA1data_case2=par_data_case2(3*ntime+1:4*ntime);
GA29data_case2=par_data_case2(4*ntime+1:5*ntime);
GA8data_case2=par_data_case2(5*ntime+1:6*ntime);
GA53data_case2=par_data_case2(6*ntime+1:7*ntime);



figure

subplot(3,4,1)
plot(1/1000*data1.position,data1.RER,'b','LineWidth',2)
hold on
plot(1/1000*data2.position,data2.RER,'r','LineWidth',2)
%plot(1/1000*[data1.xm,data1.xm],[0 0.1],'b--','LineWidth',2)
%plot(1/1000*[data2.xm,data2.xm],[0 0.1],'r--','LineWidth',2)
ylim([0 0.1])
title('RER')%,'Interpreter','latex','FontSize',10)
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('RER (1/h)','Interpreter','latex','FontSize',10)
legend('Wild-type','UBI::GA20ox-1')
box off

subplot(3,4,2)
plot(1/1000*xvec,GA53_case1,'-b','LineWidth',2)
hold on
plot(1/1000*xvec,GA53_case2,'r','LineWidth',2)
plot(1/1000*data1.pos_GAs,GA53data_case1,'bs','LineWidth',2)
plot(1/1000*data2.pos_GAs,GA53data_case2,'r*','LineWidth',2)
title('GA_{53}')%,'Interpreter','latex','FontSize',10)
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('Conc (nM)','Interpreter','latex','FontSize',10)
box off

subplot(3,4,3)
plot(1/1000*xvec,GA20ox_case1,'-b','LineWidth',2)
hold on
plot(1/1000*xvec,GA20ox_Maize_case2,'r','LineWidth',2)
%errorbar(1/1000*data1.pos_enz,data1.GA20ox,data1.GA20ox_se,'bs','LineWidth',2)
%errorbar(1/1000*data2.pos_enz,data2.GA20ox_Maize,data2.GA20ox_Maize_se,'r*','LineWidth',2)
plot(1/1000*data1.pos_enz,data1.GA20ox,'bs','LineWidth',2)
plot(1/1000*data2.pos_enz,data2.GA20ox_Maize,'r*','LineWidth',2)
title('ZmGA20ox')%,'Interpreter','latex','FontSize',10)
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('Transcript level','Interpreter','latex','FontSize',10)
box off

subplot(3,4,4)

hold on
plot(1/1000*xvec,GA20ox_Arab_case2,'r','LineWidth',2)
%errorbar(1/1000*data2.pos_enz,data2.GA20ox_Arab,data2.GA20ox_Arab_se,'r*','LineWidth',2)
plot(1/1000*data2.pos_enz,data2.GA20ox_Arab,'r*','LineWidth',2)
title('AtGA20ox')%,'Interpreter','latex','FontSize',10)
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('Transcript level','Interpreter','latex','FontSize',10)
box off

subplot(3,4,5)
plot(1/1000*xvec,GA3ox_case1,'-b','LineWidth',2)
hold on
plot(1/1000*xvec,GA3ox_case2,'r','LineWidth',2)
%errorbar(1/1000*data1.pos_enz,data1.GA3oxmRNA_measured,data1.GA3oxmRNA_measured_se,'*m','LineWidth',2)
%errorbar(1/1000*data1.pos_enz,data1.GA3ox2,data1.GA3ox2_se,'bs','LineWidth',2)
%errorbar(1/1000*data2.pos_enz,data2.GA3ox2,data2.GA3ox2_se,'r*','LineWidth',2)
plot(1/1000*data1.pos_enz,data1.GA3ox2,'bs','LineWidth',2)
plot(1/1000*data2.pos_enz,data2.GA3ox2,'r*','LineWidth',2)
title('GA3ox')%,'Interpreter','latex','FontSize',10)
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('Transcript level','Interpreter','latex','FontSize',10)
box off

subplot(3,4,6)
plot(1/1000*xvec,GA2ox_case1,'-b','LineWidth',2)
hold on
plot(1/1000*xvec,GA2ox_case2,'r','LineWidth',2)
%errorbar(1/1000*data1.pos_enz,data1.GA2oxmRNA_measured,data1.GA2oxmRNA_measured_se,'*k','LineWidth',2)
%errorbar(1/1000*data1.pos_enz,data1.GA2ox,data1.GA2ox_se,'bs','LineWidth',2)
%errorbar(1/1000*data2.pos_enz,data2.GA2ox,data2.GA2ox_se,'r*','LineWidth',2)
plot(1/1000*data1.pos_enz,data1.GA2ox,'bs','LineWidth',2)
plot(1/1000*data2.pos_enz,data2.GA2ox,'r*','LineWidth',2)
plot(1/1000*data1.xm*ones(1,101),[0:0.1:10],'-.');
title('GA2ox')%,'Interpreter','latex','FontSize',10)
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('Transcript level','Interpreter','latex','FontSize',10)
box off

subplot(3,4,7)
plot(1/1000*xvec,GA44_case1,'-b','LineWidth',2)
hold on
plot(1/1000*xvec,GA44_case2,'r','LineWidth',2)
plot(1/1000*data1.pos_GAs,GA44data_case1,'bs','LineWidth',2)
plot(1/1000*data2.pos_GAs,GA44data_case2,'r*','LineWidth',2)
title('GA_{44}')%,'Interpreter','latex','FontSize',10)
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('Conc (nM)','Interpreter','latex','FontSize',10)
box off

subplot(3,4,8)
plot(1/1000*xvec,GA19_case1,'-b','LineWidth',2)
hold on
plot(1/1000*xvec,GA19_case2,'r','LineWidth',2)
plot(1/1000*data1.pos_GAs,GA19data_case1,'bs','LineWidth',2)
plot(1/1000*data2.pos_GAs,GA19data_case2,'r*','LineWidth',2)
title('GA_{19}')%,'Interpreter','latex','FontSize',10)
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('Conc (nM)','Interpreter','latex','FontSize',10)
box off

subplot(3,4,9)
plot(1/1000*xvec,GA20_case1,'-b','LineWidth',2)
hold on
plot(1/1000*xvec,GA20_case2,'r','LineWidth',2)
plot(1/1000*data1.pos_GAs,GA20data_case1,'bs','LineWidth',2)
plot(1/1000*data2.pos_GAs,GA20data_case2,'r*','LineWidth',2)
title('GA_{20}')%,'Interpreter','latex','FontSize',10)
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('Conc (nM)','Interpreter','latex','FontSize',10)
box off

subplot(3,4,10)
plot(1/1000*xvec,GA1_case1,'-b','LineWidth',2)
hold on
plot(1/1000*xvec,GA1_case2,'r','LineWidth',2)
plot(1/1000*data1.pos_GAs,GA1data_case1,'bs','LineWidth',2)
plot(1/1000*data2.pos_GAs,GA1data_case2,'r*','LineWidth',2)
plot(1/1000*[data1.xm,data1.xm],[0 1.5],'b--','LineWidth',2)
plot(1/1000*[data2.xm,data2.xm],[0 1.5],'r--','LineWidth',2)
ylim([0 1.5])
title('GA_1')%,'Interpreter','latex','FontSize',10)
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('Conc (nM)','Interpreter','latex','FontSize',10)
box off

subplot(3,4,11)
plot(1/1000*xvec,GA29_case1,'-b','LineWidth',2)
hold on
plot(1/1000*xvec,GA29_case2,'r','LineWidth',2)
plot(1/1000*data1.pos_GAs,GA29data_case1,'bs','LineWidth',2)
plot(1/1000*data2.pos_GAs,GA29data_case2,'r*','LineWidth',2)
title('GA_{29}')%,'Interpreter','latex','FontSize',10)
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('Conc (nM)','Interpreter','latex','FontSize',10)
box off

subplot(3,4,12)
plot(1/1000*xvec,GA8_case1,'-b','LineWidth',2)
hold on
plot(1/1000*xvec,GA8_case2,'r','LineWidth',2)
plot(1/1000*data1.pos_GAs,GA8data_case1,'bs','LineWidth',2)
plot(1/1000*data2.pos_GAs,GA8data_case2,'r*','LineWidth',2)
title('GA_8')%,'Interpreter','latex','FontSize',10)
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('Conc (nM)','Interpreter','latex','FontSize',10)
box off
%legend1=legend('GA44','GA19','GA20','GA1','GA29','GA8')
%set(legend1,'Interpreter','latex','Location','SouthEast','FontSize',24)
%box('off')
%set(gca,'FontSize',20)