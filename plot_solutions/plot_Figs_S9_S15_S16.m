% Plots model fits and data for the control, wild type (B73) supposing that
%  (I) all four input functions are constant, 
% (II) the enzyme input functions are constant (Fig S16),
% (III) the GA53 input function is constant (Fig S15),
% or (IV) the input functions are prescribed using pre-fitted b-spline
% functions (Fig S9).

% Uses parameter estimates supplied in the parameter_estimates subfolder.
% Code to estimate these parameters in these cases is provided in semethod.



clear
addpath ('../fdaM')
addpath ('../semethod')
addpath ('../modelcreation_outputs')

 
load ../parameter_estimates/semethod_wt_nM_allenzpts_cyt_evenbasis_nbas7_Acpresc_lbm6_initialt1000_innuc_inpaper.mat 


initialt=tvec(1);

if rem(data.xm,50)==0 && rem(max(data.pos_GAs),50)==0
    pars_m.t=[initialt:50:data.xm];
    pars_EZ.t=[data.xm:50:max(data.pos_GAs)];
    x_results=[pars_m.t(1:end-1),pars_EZ.t]; % Same as data.position
else
    pars_m.t=[initialt:50:data.xm,data.xm];
    pars_EZ.t=[data.xm:50:max(data.pos_GAs)-1,max(data.pos_GAs)];
    x_results=[pars_m.t(1:end-1),pars_EZ.t(2:end)]; % Same as data.position
end

% Solve model with best fit parameters:
pars_m.theta=10.^theta_best;
pars_EZ.theta=pars_m.theta;
th_inputfunc_reshape=reshape(pars_m.theta(nkinparams+1:nparams),mdl_m.nbasis,mdl_m.u);
pars_m.x0=v0_initialconditions(pars_m.theta,th_inputfunc_reshape,initialt,breaks,norder,data);
pars_m.x0
[sol_m,~,~,~] = mdl_m.solveWithSensEqns_bsplines(pars_m,data,breaks,norder);
pars_EZ.x0=sol_m.x(:,end);
[sol_EZ,~,~,~] = mdl_EZ.solveWithSensEqns_bsplines(pars_EZ,data,breaks,norder);

sol_GAs_nM_cyt=[sol_m.x(:,1:end-1),sol_EZ.x(:,1:end)];

plotGAsandff_Apresc_cyt(x_results,sol_GAs_nM_cyt,data,theta_best,nbasis,nkinparams,par_data,basis)

AICc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%55
% Solutions assuming input functions are constant (spatially uniform):
clear x_results
load ../parameter_estimates/semethod_wt_nM_allenzpts_cyt_evenbasis_Acpresc_constantinputsfitted_lbm6_init1000_innuc.mat

pars_m.theta=10.^theta_best;
pars_EZ.theta=pars_m.theta;
if rem(data.xm,50)==0 && rem(max(data.pos_GAs),50)==0
    pars_m.t=[initialt:50:data.xm];
    pars_EZ.t=[data.xm:50:max(data.pos_GAs)];
    x_results=[pars_m.t(1:end-1),pars_EZ.t]; % Same as data.position
else
    pars_m.t=[initialt:50:data.xm,data.xm];
    pars_EZ.t=[data.xm:50:max(data.pos_GAs)-1,max(data.pos_GAs)];
    x_results=[pars_m.t(1:end-1),pars_EZ.t(2:end)]; % Same as data.position
end

pars_m.x0=v0_initialconditions_constantinputs(pars_m.theta,data);

% Solve for x and dxdtheta using sensitivity equations:
[sol_m_constinputs,~,~,~] = mdl_m.solveWithSensEqns_constantinputs(pars_m,data);
pars_EZ.x0=sol_m_constinputs.x(:,end);
[sol_EZ_constinputs,~,~,~] = mdl_EZ.solveWithSensEqns_constantinputs(pars_EZ,data);
sol_GAs_constinputs=[sol_m.x(:,1:end-1),sol_EZ.x(:,1:end)]; 

plotGAsandff_Apresc_cyt_constinputs(x_results,sol_GAs_constinputs,data,theta_best,par_data)
AICc_constinputs=AICc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%55
% Solutions assuming enzyme input functions are constant (spatially uniform) and GA53 fitted using bsplines:
clear x_results
load ../parameter_estimates/semethod_wt_nM_allenzpts_cyt_evenbasis_nbas7_Acpresc_constenzfitted_lbm6_init1000_innuc.mat

pars_m.theta=10.^theta_best;
pars_EZ.theta=pars_m.theta;
if rem(data.xm,50)==0 && rem(max(data.pos_GAs),50)==0
    pars_m.t=[initialt:50:data.xm];
    pars_EZ.t=[data.xm:50:max(data.pos_GAs)];
    x_results=[pars_m.t(1:end-1),pars_EZ.t]; % Same as data.position
else
    pars_m.t=[initialt:50:data.xm,data.xm];
    pars_EZ.t=[data.xm:50:max(data.pos_GAs)-1,max(data.pos_GAs)];
    x_results=[pars_m.t(1:end-1),pars_EZ.t(2:end)]; % Same as data.position
end

th_inputfunc=pars_m.theta(nkinparams+4:end)';
pars_m.x0=v0_initialconditions_constenz(pars_m.theta,th_inputfunc,initialt,breaks,norder,data);

% Solve for x and dxdtheta using sensitivity equations:
[sol_m,~,~,~] = mdl_m.solveWithSensEqns_bsplines(pars_m,data,breaks,norder);
pars_EZ.x0=sol_m.x(:,end);
[sol_EZ,~,~,~] = mdl_EZ.solveWithSensEqns_bsplines(pars_EZ,data,breaks,norder);
sol_GAs_constenz=[sol_m.x(:,1:end-1),sol_EZ.x(:,1:end)];


plotGAsandff_Apresc_cyt_constenz(x_results,sol_GAs_constenz,data,theta_best,par_data,basis,th_inputfunc)


AICc_constenz=AICc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%55
% Solutions assuming GA53 input function is constant (spatially uniform) and enzyme input functions are fitted using bsplines:
clear x_results

load ../parameter_estimates/semethod_wt_nM_allenzpts_cyt_evenbasis_nbas7_Acpresc_constGA53fitted_lbm6_initialt1000_innuc.mat

pars_m.theta=10.^theta_best;
pars_EZ.theta=pars_m.theta;
if rem(data.xm,50)==0 && rem(max(data.pos_GAs),50)==0
    pars_m.t=[initialt:50:data.xm];
    pars_EZ.t=[data.xm:50:max(data.pos_GAs)];
    x_results=[pars_m.t(1:end-1),pars_EZ.t]; % Same as data.position
else
    pars_m.t=[initialt:50:data.xm,data.xm];
    pars_EZ.t=[data.xm:50:max(data.pos_GAs)-1,max(data.pos_GAs)];
    x_results=[pars_m.t(1:end-1),pars_EZ.t(2:end)]; % Same as data.position
end


th_inputfunc_reshape=reshape(pars_m.theta(nkinparams+2:end),nbasis,u);
pars_m.x0=v0_initialconditions_constGA53(pars_m.theta,th_inputfunc_reshape,initialt,breaks,norder,data);

% Solve for x and dxdtheta using sensitivity equations:
[sol_m,~,~,~] = mdl_m.solveWithSensEqns_bsplines(pars_m,data,breaks,norder);
pars_EZ.x0=sol_m.x(:,end);
[sol_EZ,~,~,~] = mdl_EZ.solveWithSensEqns_bsplines(pars_EZ,data,breaks,norder);
sol_GAs_constGA53=[sol_m.x(:,1:end-1),sol_EZ.x(:,1:end)];

plotGAsandff_Apresc_cyt_constGA53(x_results,sol_GAs_constGA53,data,theta_best,par_data,basis,th_inputfunc_reshape)


AICc_constGA53=AICc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
% Solutions assuming prescribing input functions using pre-fitted bspline functions:
clear x_results
load ../parameter_estimates/semethod_wt_nM_allenzpts_cyt_evenbasis_nbas7_Acpresc_prescinputs_lbm6_init1000_innuc.mat

pars_m.theta=10.^theta_best;
pars_EZ.theta=pars_m.theta;
if rem(data.xm,50)==0 && rem(max(data.pos_GAs),50)==0
    pars_m.t=[initialt:50:data.xm];
    pars_EZ.t=[data.xm:50:max(data.pos_GAs)];
    x_results=[pars_m.t(1:end-1),pars_EZ.t]; % Same as data.position
else
    pars_m.t=[initialt:50:data.xm,data.xm];
    pars_EZ.t=[data.xm:50:max(data.pos_GAs)-1,max(data.pos_GAs)];
    x_results=[pars_m.t(1:end-1),pars_EZ.t(2:end)]; % Same as data.position
end

pars_m.theta=10.^theta_best;
pars_EZ.theta=pars_m.theta;

pars_m.x0=v0_initialconditions(pars_m.theta,theta_fittedinputs_reshape,data.initialt,breaks,norder,data);

% Solve for x and dxdtheta using sensitivity equations:
[sol_m,~,dxdtheta_m,~] = mdl_m.solveWithSensEqns_prescribedinput(pars_m,data,breaks,norder,theta_fittedinputs_reshape);
pars_EZ.x0=sol_m.x(:,end);
[sol_EZ,~,dxdtheta_EZ,~] = mdl_EZ.solveWithSensEqns_prescribedinput(pars_EZ,data,breaks,norder,theta_fittedinputs_reshape);

sol_GAs_prescinputs=[sol_m.x(:,1:end-1),sol_EZ.x(:,1:end)];

plotGAsandff_Apresc_cyt_prescinputs(x_results,sol_GAs_prescinputs,data,cyt_conv,theta_fittedinputs_reshape,par_data,basis)


