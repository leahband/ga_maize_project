function plotGAsff_compare_threedatasets(xvec,C1,C2,C3,C1_enz,C2_enz,C3_enz,par_data_case1,par_data_case2,par_data_case3,data1,data2,data3)

GA44_case1=C1(:,1);
GA19_case1=C1(:,2);
GA20_case1=C1(:,3);
GA1_case1=C1(:,4);
GA29_case1=C1(:,5);
GA8_case1=C1(:,6);
GA53_case1=C1(:,7);

GA44_case2=C2(:,1);
GA19_case2=C2(:,2);
GA20_case2=C2(:,3);
GA1_case2=C2(:,4);
GA29_case2=C2(:,5);
GA8_case2=C2(:,6);
GA53_case2=C2(:,7);

GA44_case3=C3(:,1);
GA19_case3=C3(:,2);
GA20_case3=C3(:,3);
GA1_case3=C3(:,4);
GA29_case3=C3(:,5);
GA8_case3=C3(:,6);
GA53_case3=C3(:,7);

GA20ox_case1=C1_enz(:,1);
GA3ox_case1=C1_enz(:,2);
GA2ox_case1=C1_enz(:,3);

GA20ox_case2=C2_enz(:,1);
GA3ox_case2=C2_enz(:,2);
GA2ox_case2=C2_enz(:,3);

GA20ox_case3=C3_enz(:,1);
GA3ox_case3=C3_enz(:,2);
GA2ox_case3=C3_enz(:,3);


ntime=length(data1.pos_GAs);

GA44data_case1=par_data_case1(1:ntime);
GA19data_case1=par_data_case1(ntime+1:2*ntime);
GA20data_case1=par_data_case1(2*ntime+1:3*ntime);
GA1data_case1=par_data_case1(3*ntime+1:4*ntime);
GA29data_case1=par_data_case1(4*ntime+1:5*ntime);
GA8data_case1=par_data_case1(5*ntime+1:6*ntime);
GA53data_case1=par_data_case1(6*ntime+1:7*ntime);

GA44data_case2=par_data_case2(1:ntime);
GA19data_case2=par_data_case2(ntime+1:2*ntime);
GA20data_case2=par_data_case2(2*ntime+1:3*ntime);
GA1data_case2=par_data_case2(3*ntime+1:4*ntime);
GA29data_case2=par_data_case2(4*ntime+1:5*ntime);
GA8data_case2=par_data_case2(5*ntime+1:6*ntime);
GA53data_case2=par_data_case2(6*ntime+1:7*ntime);

GA44data_case3=par_data_case3(1:ntime);
GA19data_case3=par_data_case3(ntime+1:2*ntime);
GA20data_case3=par_data_case3(2*ntime+1:3*ntime);
GA1data_case3=par_data_case3(3*ntime+1:4*ntime);
GA29data_case3=par_data_case3(4*ntime+1:5*ntime);
GA8data_case3=par_data_case3(5*ntime+1:6*ntime);
GA53data_case3=par_data_case3(6*ntime+1:7*ntime);

figure

% subplot(3,4,2)
% hold on
% plot(1/1000*data1.pos_GAs,GA1data_case1,'b*--','LineWidth',2)
% plot(1/1000*data2.pos_GAs,GA1data_case2,'r*--','LineWidth',2)
% plot(1/1000*data2.pos_GAs,GA1data_case3,'g*--','LineWidth',2)
% title('GA_1 data')
% xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
% ylabel('Conc (nM)','Interpreter','latex','FontSize',10)
% box off

subplot(3,4,1)
plot(1/1000*xvec,GA53_case1,'-b','LineWidth',2)
hold on
plot(1/1000*xvec,GA53_case2,'-r','LineWidth',2)
plot(1/1000*xvec,GA53_case3,'-g','LineWidth',2)
plot(1/1000*data1.pos_GAs,GA53data_case1,'b*','LineWidth',2)
plot(1/1000*data2.pos_GAs,GA53data_case2,'r*','LineWidth',2)
plot(1/1000*data3.pos_GAs,GA53data_case3,'g*','LineWidth',2)
title('GA_{53}')
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('Conc (nM)','Interpreter','latex','FontSize',10)
box off

subplot(3,4,2)
plot(1/1000*xvec,GA20ox_case1,'-b','LineWidth',2)
hold on
plot(1/1000*xvec,GA20ox_case2,'-r','LineWidth',2)
plot(1/1000*xvec,GA20ox_case3,'-g','LineWidth',2)
%errorbar(1/1000*data1.pos_enz,data1.GA20oxmRNA_measured,data1.GA20oxmRNA_measured_std,'*g','LineWidth',2)
plot(1/1000*data1.pos_enz,data1.GA20ox,'b*','LineWidth',2)
plot(1/1000*data2.pos_enz,data2.GA20ox,'r*','LineWidth',2)
plot(1/1000*data2.pos_enz,data3.GA20ox,'g*','LineWidth',2)
title('GA20ox')
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('Transcript level','Interpreter','latex','FontSize',10)
box off

subplot(3,4,3)
plot(1/1000*xvec,GA3ox_case1,'-b','LineWidth',2)
hold on
plot(1/1000*xvec,GA3ox_case2,'-r','LineWidth',2)
plot(1/1000*xvec,GA3ox_case3,'-g','LineWidth',2)
%errorbar(1/1000*data1.pos_enz,data1.GA3oxmRNA_measured,data1.GA3oxmRNA_measured_std,'*m','LineWidth',2)
plot(1/1000*data1.pos_enz,data1.GA3ox2,'b*','LineWidth',2)
plot(1/1000*data2.pos_enz,data2.GA3ox2,'r*','LineWidth',2)
plot(1/1000*data2.pos_enz,data3.GA3ox2,'g*','LineWidth',2)
title('GA3ox')
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('Transcript level','Interpreter','latex','FontSize',10)
box off

subplot(3,4,4)
plot(1/1000*xvec,GA2ox_case1,'-b','LineWidth',2)
hold on
plot(1/1000*xvec,GA2ox_case2,'-r','LineWidth',2)
plot(1/1000*xvec,GA2ox_case3,'g-','LineWidth',2)
%errorbar(1/1000*data1.pos_enz,data1.GA2oxmRNA_measured,data1.GA2oxmRNA_measured_std,'*k','LineWidth',2)
plot(1/1000*data1.pos_enz,data1.GA2ox,'b*','LineWidth',2)
plot(1/1000*data2.pos_enz,data2.GA2ox,'r*','LineWidth',2)
plot(1/1000*data2.pos_enz,data3.GA2ox,'g*','LineWidth',2)
plot(1/1000*data1.xm*ones(1,101),[0:0.1:10],'-.');
title('GA2ox')
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('Transcript level','Interpreter','latex','FontSize',10)
box off

subplot(3,4,5)
plot(1/1000*xvec,GA44_case1,'-b','LineWidth',2)
hold on
plot(1/1000*xvec,GA44_case2,'-r','LineWidth',2)
plot(1/1000*xvec,GA44_case3,'-g','LineWidth',2)
plot(1/1000*data1.pos_GAs,GA44data_case1,'b*','LineWidth',2)
plot(1/1000*data2.pos_GAs,GA44data_case2,'r*','LineWidth',2)
plot(1/1000*data2.pos_GAs,GA44data_case3,'g*','LineWidth',2)
title('GA_{44}')
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('Conc (nM)','Interpreter','latex','FontSize',10)
box off

subplot(3,4,6)
plot(1/1000*xvec,GA19_case1,'-b','LineWidth',2)
hold on
plot(1/1000*xvec,GA19_case2,'-r','LineWidth',2)
plot(1/1000*xvec,GA19_case3,'-g','LineWidth',2)
plot(1/1000*data1.pos_GAs,GA19data_case1,'b*','LineWidth',2)
plot(1/1000*data2.pos_GAs,GA19data_case2,'r*','LineWidth',2)
plot(1/1000*data2.pos_GAs,GA19data_case3,'g*','LineWidth',2)
title('GA_{19}')
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('Conc (nM)','Interpreter','latex','FontSize',10)
box off

subplot(3,4,7)
plot(1/1000*xvec,GA20_case1,'-b','LineWidth',2)
hold on
plot(1/1000*xvec,GA20_case2,'-r','LineWidth',2)
plot(1/1000*xvec,GA20_case3,'-g','LineWidth',2)
plot(1/1000*data1.pos_GAs,GA20data_case1,'b*','LineWidth',2)
plot(1/1000*data2.pos_GAs,GA20data_case2,'r*','LineWidth',2)
plot(1/1000*data2.pos_GAs,GA20data_case3,'g*','LineWidth',2)
ylim([0 0.2])
title('GA_{20}')
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('Conc (nM)','Interpreter','latex','FontSize',10)
box off

subplot(3,4,8)
plot(1/1000*xvec,GA1_case1,'-b','LineWidth',2)
hold on
plot(1/1000*xvec,GA1_case2,'-r','LineWidth',2)
plot(1/1000*xvec,GA1_case3,'-g','LineWidth',2)
plot(1/1000*data1.pos_GAs,GA1data_case1,'b*','LineWidth',2)
plot(1/1000*data2.pos_GAs,GA1data_case2,'r*','LineWidth',2)
plot(1/1000*data2.pos_GAs,GA1data_case3,'g*','LineWidth',2)
title('GA_1')
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('Conc (nM)','Interpreter','latex','FontSize',10)
box off

subplot(3,4,10)
plot(1/1000*xvec,GA29_case1,'-b','LineWidth',2)
hold on
plot(1/1000*xvec,GA29_case2,'-r','LineWidth',2)
plot(1/1000*xvec,GA29_case3,'-g','LineWidth',2)
plot(1/1000*data1.pos_GAs,GA29data_case1,'b*','LineWidth',2)
plot(1/1000*data2.pos_GAs,GA29data_case2,'r*','LineWidth',2)
plot(1/1000*data2.pos_GAs,GA29data_case3,'g*','LineWidth',2)
ylim([0 0.15])
title('GA_{29}')
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('Conc (nM)','Interpreter','latex','FontSize',10)
box off

subplot(3,4,11)
plot(1/1000*xvec,GA8_case1,'-b','LineWidth',2)
hold on
plot(1/1000*xvec,GA8_case2,'-r','LineWidth',2)
plot(1/1000*xvec,GA8_case3,'-g','LineWidth',2)
plot(1/1000*data1.pos_GAs,GA8data_case1,'b*','LineWidth',2)
plot(1/1000*data2.pos_GAs,GA8data_case2,'r*','LineWidth',2)
plot(1/1000*data2.pos_GAs,GA8data_case3,'g*','LineWidth',2)
title('GA_8')
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('Conc (nM)','Interpreter','latex','FontSize',10)
box off
%legend1=legend('GA44','GA19','GA20','GA1','GA29','GA8')
%set(legend1,'Interpreter','latex','Location','SouthEast','FontSize',24)
%box('off')
%set(gca,'FontSize',20)