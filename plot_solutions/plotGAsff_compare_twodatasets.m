function plotGAsff_compare_twodatasets(xvec,C1,C2,C1_enz,C2_enz,par_data_case1,par_data_case2,data1,data2)

GA44_case1=C1(:,1);
GA19_case1=C1(:,2);
GA20_case1=C1(:,3);
GA1_case1=C1(:,4);
GA29_case1=C1(:,5);
GA8_case1=C1(:,6);
GA53_case1=C1(:,7);

GA44_case2=C2(:,1);
GA19_case2=C2(:,2);
GA20_case2=C2(:,3);
GA1_case2=C2(:,4);
GA29_case2=C2(:,5);
GA8_case2=C2(:,6);
GA53_case2=C2(:,7);

GA20ox_case1=C1_enz(:,1);
GA3ox_case1=C1_enz(:,2);
GA2ox_case1=C1_enz(:,3);

GA20ox_case2=C2_enz(:,1);
GA3ox_case2=C2_enz(:,2);
GA2ox_case2=C2_enz(:,3);

ntime=length(data1.pos_GAs);

GA44data_case1=par_data_case1(1:ntime);
GA19data_case1=par_data_case1(ntime+1:2*ntime);
GA20data_case1=par_data_case1(2*ntime+1:3*ntime);
GA1data_case1=par_data_case1(3*ntime+1:4*ntime);
GA29data_case1=par_data_case1(4*ntime+1:5*ntime);
GA8data_case1=par_data_case1(5*ntime+1:6*ntime);
GA53data_case1=par_data_case1(6*ntime+1:7*ntime);

GA44data_case2=par_data_case2(1:ntime);
GA19data_case2=par_data_case2(ntime+1:2*ntime);
GA20data_case2=par_data_case2(2*ntime+1:3*ntime);
GA1data_case2=par_data_case2(3*ntime+1:4*ntime);
GA29data_case2=par_data_case2(4*ntime+1:5*ntime);
GA8data_case2=par_data_case2(5*ntime+1:6*ntime);
GA53data_case2=par_data_case2(6*ntime+1:7*ntime);



figure

subplot(5,2,1)
plot(1/1000*xvec,GA53_case1,'-b','LineWidth',1.4)
hold on
plot(1/1000*xvec,GA53_case2,'--r','LineWidth',1.4)
plot(1/1000*data1.pos_GAs,GA53data_case1,'bs','LineWidth',1.4)
plot(1/1000*data2.pos_GAs,GA53data_case2,'r*','LineWidth',1.4)
title('GA_{53}')%,'Interpreter','latex','FontSize',14)
xlabel('Dist (mm)','Interpreter','latex','FontSize',14)
ylabel('Conc (nM)','Interpreter','latex','FontSize',14)
box off

subplot(5,2,2)
plot(1/1000*xvec,GA20ox_case1,'-b','LineWidth',1.4)
hold on
plot(1/1000*xvec,GA20ox_case2,'--r','LineWidth',1.4)
%errorbar(1/1000*data1.pos_enz,data1.GA20oxmRNA_measured,data1.GA20oxmRNA_measured_se,'*g','LineWidth',1.4)
errorbar(1/1000*data1.pos_enz,data1.GA20ox,data1.GA20ox_se,'bs','LineWidth',1.4)
errorbar(1/1000*data2.pos_enz,data2.GA20ox,data2.GA20ox_se,'r*','LineWidth',1.4)
title('GA20ox')%,'Interpreter','latex','FontSize',14)
xlabel('Dist (mm)','Interpreter','latex','FontSize',14)
ylabel('Transcript level','Interpreter','latex','FontSize',14)
box off

subplot(5,2,3)
plot(1/1000*xvec,GA3ox_case1,'-b','LineWidth',1.4)
hold on
plot(1/1000*xvec,GA3ox_case2,'--r','LineWidth',1.4)
%errorbar(1/1000*data1.pos_enz,data1.GA3oxmRNA_measured,data1.GA3oxmRNA_measured_se,'*m','LineWidth',1.4)
errorbar(1/1000*data1.pos_enz,data1.GA3ox2,data1.GA3ox2_se,'bs','LineWidth',1.4)
errorbar(1/1000*data2.pos_enz,data2.GA3ox2,data2.GA3ox2_se,'r*','LineWidth',1.4)
title('GA3ox')%,'Interpreter','latex','FontSize',14)
xlabel('Dist (mm)','Interpreter','latex','FontSize',14)
ylabel('Transcript level','Interpreter','latex','FontSize',14)
box off

subplot(5,2,4)
plot(1/1000*xvec,GA2ox_case1,'-b','LineWidth',1.4)
hold on
plot(1/1000*xvec,GA2ox_case2,'--r','LineWidth',1.4)
%errorbar(1/1000*data1.pos_enz,data1.GA2oxmRNA_measured,data1.GA2oxmRNA_measured_se,'*k','LineWidth',1.4)
errorbar(1/1000*data1.pos_enz,data1.GA2ox,data1.GA2ox_se,'bs','LineWidth',1.4)
errorbar(1/1000*data2.pos_enz,data2.GA2ox,data2.GA2ox_se,'r*','LineWidth',1.4)
plot(1/1000*data1.xm*ones(1,101),[0:0.1:10],'-.');
title('GA2ox')%,'Interpreter','latex','FontSize',14)
xlabel('Dist (mm)','Interpreter','latex','FontSize',14)
ylabel('Transcript level','Interpreter','latex','FontSize',14)
box off

subplot(5,2,5)
plot(1/1000*xvec,GA44_case1,'-b','LineWidth',1.4)
hold on
plot(1/1000*xvec,GA44_case2,'--r','LineWidth',1.4)
plot(1/1000*data1.pos_GAs,GA44data_case1,'bs','LineWidth',1.4)
plot(1/1000*data2.pos_GAs,GA44data_case2,'r*','LineWidth',1.4)
title('GA_{44}')%,'Interpreter','latex','FontSize',14)
xlabel('Dist (mm)','Interpreter','latex','FontSize',14)
ylabel('Conc (nM)','Interpreter','latex','FontSize',14)
box off

subplot(5,2,6)
plot(1/1000*xvec,GA19_case1,'-b','LineWidth',1.4)
hold on
plot(1/1000*xvec,GA19_case2,'--r','LineWidth',1.4)
plot(1/1000*data1.pos_GAs,GA19data_case1,'bs','LineWidth',1.4)
plot(1/1000*data2.pos_GAs,GA19data_case2,'r*','LineWidth',1.4)
title('GA_{19}')%,'Interpreter','latex','FontSize',14)
xlabel('Dist (mm)','Interpreter','latex','FontSize',14)
ylabel('Conc (nM)','Interpreter','latex','FontSize',14)
box off

subplot(5,2,7)
plot(1/1000*xvec,GA20_case1,'-b','LineWidth',1.4)
hold on
plot(1/1000*xvec,GA20_case2,'--r','LineWidth',1.4)
plot(1/1000*data1.pos_GAs,GA20data_case1,'bs','LineWidth',1.4)
plot(1/1000*data2.pos_GAs,GA20data_case2,'r*','LineWidth',1.4)
title('GA_{20}')%,'Interpreter','latex','FontSize',14)
xlabel('Dist (mm)','Interpreter','latex','FontSize',14)
ylabel('Conc (nM)','Interpreter','latex','FontSize',14)
box off

subplot(5,2,8)
plot(1/1000*xvec,GA1_case1,'-b','LineWidth',1.4)
hold on
plot(1/1000*xvec,GA1_case2,'--r','LineWidth',1.4)
plot(1/1000*data1.pos_GAs,GA1data_case1,'bs','LineWidth',1.4)
plot(1/1000*data2.pos_GAs,GA1data_case2,'r*','LineWidth',1.4)
title('GA_1')%,'Interpreter','latex','FontSize',14)
xlabel('Dist (mm)','Interpreter','latex','FontSize',14)
ylabel('Conc (nM)','Interpreter','latex','FontSize',14)
box off

subplot(5,2,9)
plot(1/1000*xvec,GA29_case1,'-b','LineWidth',1.4)
hold on
plot(1/1000*xvec,GA29_case2,'--r','LineWidth',1.4)
plot(1/1000*data1.pos_GAs,GA29data_case1,'bs','LineWidth',1.4)
plot(1/1000*data2.pos_GAs,GA29data_case2,'r*','LineWidth',1.4)
title('GA_{29}')%,'Interpreter','latex','FontSize',14)
xlabel('Dist (mm)','Interpreter','latex','FontSize',14)
ylabel('Conc (nM)','Interpreter','latex','FontSize',14)
box off

subplot(5,2,10)
plot(1/1000*xvec,GA8_case1,'-b','LineWidth',1.4)
hold on
plot(1/1000*xvec,GA8_case2,'--r','LineWidth',1.4)
plot(1/1000*data1.pos_GAs,GA8data_case1,'bs','LineWidth',1.4)
plot(1/1000*data2.pos_GAs,GA8data_case2,'r*','LineWidth',1.4)
title('GA_8')%,'Interpreter','latex','FontSize',14)
xlabel('Dist (mm)','Interpreter','latex','FontSize',14)
ylabel('Conc (nM)','Interpreter','latex','FontSize',14)
box off
%legend1=legend('GA44','GA19','GA20','GA1','GA29','GA8')
%set(legend1,'Interpreter','latex','Location','SouthEast','FontSize',24)
%box('off')
%set(gca,'FontSize',20)