# ga_maize_project

Band et al. Proc Natl Acad Sci USA, 2022
"Modelling reveals post-transcriptional regulation of GA metabolism enzymes in response to drought and cold"

Code and data for model of GA dynamics in the maize leaf growth zone in above paper.


The code relies on the "fda" MATLAB package by Ramsey and Hooker. It can be downloaded from 
https://www.psych.mcgill.ca/misc/fda/downloads/FDAfuns/Matlab/fdaM.zip
and should be extracted into a folder called fdaM. 
