% Data set provided distributions of cell lengths, 
% Here, we use these data to calculate the cell velocities.

clear
load  data_celllengths_3cases.mat
N= size(position,1);

% Table gives meristem lengths (based on DAPI staining):  
% merisze_wt = 17 mm,
%merisize_cold= 16 mm
% merisize _drought =10 mm, 
% However, this gives the position where cell division ceases, rather than being based on the kinematics.
% We note that these values lead to a region where cell production is negative,
% as cell length increases faster than velocity, so in the kinematic
% analysis below, we take the meristem size to be 
% equal to the position where the cell production rate reaches zero. 

merisize_wt=17000; % In microns
merisize_cold= 16000;
merisize_drought =20000;

prod_wt=23; % Values from Supplementary Table of Nelissen et al. 
prod_cold=16;
prod_drought=19;


for i=1:N
    if position(i)==merisize_wt;
        pos_meri_wt=i
    end
    if position(i)==merisize_cold;
        pos_meri_cold=i
    end
     if position(i)==merisize_drought;
        pos_meri_drought=i
    end
end

velocity_wt=zeros(N,1);
velocity_cold=zeros(N,1);
velocity_drought=zeros(N,1);

velocity_wt(pos_meri_wt:end)=prod_wt*cell_length_wt(pos_meri_wt:end);
velocity_cold(pos_meri_cold:end)=prod_cold*cell_length_cold(pos_meri_cold:end);
velocity_drought(pos_meri_drought:end)=prod_drought*cell_length_drought(pos_meri_drought:end);

gradient_wt=velocity_wt(pos_meri_wt)./position(pos_meri_wt);
gradient_cold=velocity_cold(pos_meri_cold)./position(pos_meri_cold);
gradient_drought=velocity_drought(pos_meri_drought)./position(pos_meri_drought);

velocity_wt(1:pos_meri_wt)=gradient_wt*position(1:pos_meri_wt);
velocity_cold(1:pos_meri_cold)=gradient_cold*position(1:pos_meri_cold);
velocity_drought(1:pos_meri_drought)=gradient_drought*position(1:pos_meri_drought);

F_wt=velocity_wt./cell_length_wt; % [1/time]
F_cold=velocity_cold./cell_length_cold; % [1/time]
F_drought=velocity_drought./cell_length_drought; % [1/time]

figure
subplot(1,2,1)
plot(position, velocity_wt)
hold on 
plot(position, velocity_cold,'r')
plot(position, velocity_drought,'g')
subplot(1,2,2)
hold on
plot(position, F_wt)
plot(position, F_cold,'r')
plot(position, F_drought,'g')

[C,I_wt]=max(F_wt);
merisize_wt=position(I_wt)
pos_meri_wt=I_wt

[C,I_cold]=max(F_cold);
merisize_cold=position(I_cold)
pos_meri_cold=I_cold;

[C,I_drought]=max(F_drought);
merisize_drought=position(I_drought)
pos_meri_drought=I_drought;

velocity_wt=zeros(N,1);
velocity_cold=zeros(N,1);
velocity_drought=zeros(N,1);

cell_length_wt(pos_meri_wt)
velocity_wt(pos_meri_wt:end)=prod_wt*cell_length_wt(pos_meri_wt:end);
velocity_cold(pos_meri_cold:end)=prod_cold*cell_length_cold(pos_meri_cold:end);
velocity_drought(pos_meri_drought:end)=prod_drought*cell_length_drought(pos_meri_drought:end);

gradient_wt=velocity_wt(pos_meri_wt)./position(pos_meri_wt);
gradient_cold=velocity_cold(pos_meri_cold)./position(pos_meri_cold);
gradient_drought=velocity_drought(pos_meri_drought)./position(pos_meri_drought);

velocity_wt(1:pos_meri_wt)=gradient_wt*position(1:pos_meri_wt);
velocity_cold(1:pos_meri_cold)=gradient_cold*position(1:pos_meri_cold);
velocity_drought(1:pos_meri_drought)=gradient_drought*position(1:pos_meri_drought);

F_wt=velocity_wt./cell_length_wt; % [1/time]
F_cold=velocity_cold./cell_length_cold; % [1/time]
F_drought=velocity_drought./cell_length_drought; % [1/time]

figure
subplot(1,2,1)
plot(position, velocity_wt)
hold on 
plot(position, velocity_cold,'r')
plot(position, velocity_drought,'g')
subplot(1,2,2)
hold on
plot(position, F_wt)
plot(position, F_cold,'r')
plot(position, F_drought,'g')

save kinematicdata_3cases_veladjusted.mat cell_length_wt cell_length_cold cell_length_drought position velocity_wt velocity_cold velocity_drought F_wt F_cold F_drought N
