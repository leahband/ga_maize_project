% Forms 'data' from different mat files, adjusting GA data into
% concentrations.

function make_GAdata_intoconcentrations_innM

load data_metabolites_control.mat
load data_metabolites_drought.mat
load data_metabolites_cold.mat


figure
title('Raw data')
subplot(2,4,1)
hold on
errorbar(0.001*data.pos_GAs,data.GA53_con,data.GA53_con_se,'bs-','LineWidth',2)
errorbar(0.001*data_drought.pos_GAs,data_drought.GA53,data_drought.GA53_se,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
errorbar(0.001*data_cold.pos_GAs,data_cold.GA53,data_cold.GA53_se,'ro-','MarkerFaceColor',[1 0 0],'LineWidth',2)
title('GA53','Interpreter','latex','FontSize',18)
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA53 (ng/gDW)','Interpreter','latex','FontSize',16)
box off

subplot(2,4,2)
hold on
errorbar(0.001*data.pos_GAs,data.GA44_con,data.GA44_con_se,'bs-','LineWidth',2)
errorbar(0.001*data_drought.pos_GAs,data_drought.GA44,data_drought.GA44_se,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
errorbar(0.001*data_cold.pos_GAs,data_cold.GA44,data_cold.GA44_se,'ro-','MarkerFaceColor',[1 0 0],'LineWidth',2)
title('GA44','Interpreter','latex','FontSize',18)
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA44 (ng/gDW)','Interpreter','latex','FontSize',16)
box off

subplot(2,4,3)
hold on
errorbar(0.001*data.pos_GAs,data.GA19_con,data.GA19_con_se,'bs-','LineWidth',2)
errorbar(0.001*data_drought.pos_GAs,data_drought.GA19,data_drought.GA19_se,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
errorbar(0.001*data_cold.pos_GAs,data_cold.GA19,data_cold.GA19_se,'ro-','MarkerFaceColor',[1 0 0],'LineWidth',2)
title('GA19','Interpreter','latex','FontSize',18)
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA19 (ng/gDW)','Interpreter','latex','FontSize',16)
box off

subplot(2,4,4)
hold on
errorbar(0.001*data.pos_GAs,data.GA20_con,data.GA20_con_se,'bs-','LineWidth',2)
errorbar(0.001*data_drought.pos_GAs,data_drought.GA20,data_drought.GA20_se,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
errorbar(0.001*data_cold.pos_GAs,data_cold.GA20,data_cold.GA20_se,'ro-','MarkerFaceColor',[1 0 0],'LineWidth',2)
title('GA20','Interpreter','latex','FontSize',18)
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA20 (ng/gDW)','Interpreter','latex','FontSize',16)
box off

subplot(2,4,5)
hold on
errorbar(0.001*data.pos_GAs,data.GA1_con,data.GA1_con_se,'bs-','LineWidth',2)
errorbar(0.001*data_drought.pos_GAs,data_drought.GA1,data_drought.GA1_se,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
errorbar(0.001*data_cold.pos_GAs,data_cold.GA1,data_cold.GA1_se,'ro-','MarkerFaceColor',[1 0 0],'LineWidth',2)
title('GA1','Interpreter','latex','FontSize',18)
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA1 (ng/gDW)','Interpreter','latex','FontSize',16)
box off

subplot(2,4,6)
hold on
errorbar(0.001*data.pos_GAs,data.GA29_con,data.GA29_con_se,'bs-','LineWidth',2)
errorbar(0.001*data_drought.pos_GAs,data_drought.GA29,data_drought.GA29_se,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
errorbar(0.001*data_cold.pos_GAs,data_cold.GA29,data_cold.GA29_se,'ro-','MarkerFaceColor',[1 0 0],'LineWidth',2)
title('GA29','Interpreter','latex','FontSize',18)
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA29 (ng/gDW),','Interpreter','latex','FontSize',16)
box off

subplot(2,4,7)
hold on
errorbar(0.001*data.pos_GAs,data.GA8_con,data.GA8_con_se,'bs-','LineWidth',2)
errorbar(0.001*data_drought.pos_GAs,data_drought.GA8,data_drought.GA8_se,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
errorbar(0.001*data_cold.pos_GAs,data_cold.GA8,data_cold.GA8_se,'ro-','MarkerFaceColor',[1 0 0],'LineWidth',2)
title('GA8','Interpreter','latex','FontSize',18)
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA8 (ng/gDW)','Interpreter','latex','FontSize',16)
box off

legend1=legend('Control','Drought','Cold')
set(legend1,'Interpreter','latex','Location','SouthEast','FontSize',24)

% Add in kinematic data calculated from
load kinematicdata_3cases_calc_velandmericorrected.mat

data.position=1000*position;
data_cold.position=1000*position;
data_drought.position=1000*position;

data.cell_length=cell_length_wt;
data_cold.cell_length=cell_length_cold;
data_drought.cell_length=cell_length_drought;

data.vel=velocity_wt;
data_cold.vel=velocity_cold;
data_drought.vel=velocity_drought;

data.RER=RER_wt;
data_cold.RER=RER_cold;
data_drought.RER=RER_drought;

data.xm=merisize_wt;
data_cold.xm=merisize_cold;
data_drought.xm=merisize_drought;

data.lmf=mericelllength_wt;
data_cold.lmf=mericelllength_cold;
data_drought.lmf=mericelllength_drought;

% Dry weight measurements in ugDW/cm
data.dryweight_con=[0.019;0.019;0.01956;0.01876;0.01868;0.019;0.01968;0.02124;0.0223;0.02314;0.02568;0.025];

data_cold.dryweight=[0.01952;0.01928;0.02064;0.02068;0.02044;0.02046;0.02108;0.02074;0.0229;0.024;0.0273;0.0242];

data_drought.dryweight=[0.01504;0.01688;0.01624;0.01678;0.01642;0.01671;0.01847;0.01868;0.02102;0.02023;0.02047;0.01956];

figure
hold on
plot(0.001*data.pos_GAs,data.dryweight_con,'bs-','LineWidth',2)
plot(0.001*data_drought.pos_GAs,data_drought.dryweight,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
plot(0.001*data_cold.pos_GAs,data_cold.dryweight,'ro-','MarkerFaceColor',[1 0 0],'LineWidth',2)
title('Dry weight','Interpreter','latex','FontSize',18)
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('Dry weight ($\mu$g/cm)','Interpreter','latex','FontSize',16)
box off
legend1=legend('Control','Drought','Cold')
set(legend1,'Interpreter','latex','Location','SouthEast','FontSize',18)


data.samplesize=[0.5;0.5;0.5;0.5;1;1;1;1;1;1;1;1];

% Molecular weights given in Daltons = g/mol
data.GA53_molweight=346.42;
data.GA20_molweight=331.39;
data.GA19_molweight=360.41;
data.GA44_molweight=362.42; % Open Lactone form (Hedden and Philips, Tr Plant Sci, 2000)
data.GA8_molweight=363.39;
data.GA1_molweight=347.39;
data.GA29_molweight=347.39;

% GA concentrations in ng/gDW = pg/mg DW = 10^(-12)g/(10^(-3) g DW)

% So GA measurements * Dry Weight/GA molecular weight gives value in
% 10^(-15) mol/cm, multiplying by 10^3 gives value in amol/cm 
%(i.e. 10^(-18) mol/cm)

load areas_controldroughtcold.mat data_areas
data.Ac=data_areas.coldwt;
data.Ac_se=data_areas.coldwt_se;
data.pos_Ac=data_areas.pos_Ac;

data_cold.Ac=data_areas.cold;
data_cold.Ac_se=data_areas.cold_se;
data_cold.pos_Ac=data_areas.pos_Ac;

data_drought.Ac=data.Ac;
data_drought.Ac_se=data.Ac_se;
data_drought.pos_Ac=data.pos_Ac;



% Interpolate to find areas at positions of GA measurements:
data.A_smooth=interp1(data.pos_Ac,data.Ac,data.pos_GAs,'pchip','extrap');
data_cold.A_smooth=interp1(data.pos_Ac,data_cold.Ac,data_cold.pos_GAs,'pchip','extrap');
data_drought.A_smooth=data.A_smooth;

figure
hold on
errorbar(0.001*data.pos_Ac,data.Ac,data.Ac_se,'bs-','LineWidth',2)
plot(0.001*data.pos_GAs,data.A_smooth,'r-x','LineWidth',2)
xlabel('Dist (mm)','Interpreter','latex','FontSize',16)
ylabel('Area (mm$^2$)','Interpreter','latex','FontSize',16)
box off



% Leaf area given in mm^2
% GA measurements * Dry Weight/(GA molecular weight* Leaf area) gives value in
% 10^(-7) M, multiply by 100 to giuve a value in nM.

data.GA53=10^2./(data.A_smooth*data.GA53_molweight).*data.GA53_con.*data.dryweight_con;
data.GA44=10^2./(data.A_smooth*data.GA44_molweight).*data.GA44_con.*data.dryweight_con;
data.GA19=10^2./(data.A_smooth*data.GA19_molweight).*data.GA19_con.*data.dryweight_con;
data.GA20=10^2./(data.A_smooth*data.GA20_molweight).*data.GA20_con.*data.dryweight_con;
data.GA8=10^2./(data.A_smooth*data.GA8_molweight).*data.GA8_con.*data.dryweight_con;
data.GA1=10^2./(data.A_smooth*data.GA1_molweight).*data.GA1_con.*data.dryweight_con;
data.GA29=10^2./(data.A_smooth*data.GA29_molweight).*data.GA29_con.*data.dryweight_con;

% Calculating errors doesn't account for errors in area measurements , or in dry weight
% sheet.

data.GA53_se=10^2./(data.A_smooth.*data.GA53_molweight).*data.GA53_con_se.*data.dryweight_con;
data.GA44_se=10^2./(data.A_smooth.*data.GA44_molweight).*data.GA44_con_se.*data.dryweight_con;
data.GA19_se=10^2./(data.A_smooth.*data.GA19_molweight).*data.GA19_con_se.*data.dryweight_con;
data.GA20_se=10^2./(data.A_smooth.*data.GA20_molweight).*data.GA20_con_se.*data.dryweight_con;
data.GA8_se=10^2./(data.A_smooth.*data.GA8_molweight).*data.GA8_con_se.*data.dryweight_con;
data.GA1_se=10^2./(data.A_smooth.*data.GA1_molweight).*data.GA1_con_se.*data.dryweight_con;
data.GA29_se=10^2./(data.A_smooth.*data.GA29_molweight).*data.GA29_con_se.*data.dryweight_con;

data.GA53_var=3*(data.GA53_se).^2;
data.GA44_var=3*(data.GA44_se).^2;
data.GA19_var=3*(data.GA19_se).^2;
data.GA20_var=3*(data.GA20_se).^2;
data.GA8_var=3*(data.GA8_se).^2;
data.GA1_var=3*(data.GA1_se).^2;
data.GA29_var=3*(data.GA29_se).^2;

data_cold.GA53=10^2./(data_cold.A_smooth.*data.GA53_molweight).*data_cold.GA53.*data_cold.dryweight;
data_cold.GA44=10^2./(data_cold.A_smooth.*data.GA44_molweight).*data_cold.GA44.*data_cold.dryweight;
data_cold.GA19=10^2./(data_cold.A_smooth.*data.GA19_molweight).*data_cold.GA19.*data_cold.dryweight;
data_cold.GA20=10^2./(data_cold.A_smooth.*data.GA20_molweight).*data_cold.GA20.*data_cold.dryweight;
data_cold.GA8=10^2./(data_cold.A_smooth.*data.GA8_molweight).*data_cold.GA8.*data_cold.dryweight;
data_cold.GA1=10^2./(data_cold.A_smooth.*data.GA1_molweight).*data_cold.GA1.*data_cold.dryweight;
data_cold.GA29=10^2./(data_cold.A_smooth.*data.GA29_molweight).*data_cold.GA29.*data_cold.dryweight;

data_cold.GA53_se=10^2./(data_cold.A_smooth.*data.GA53_molweight).*data_cold.GA53_se.*data_cold.dryweight;
data_cold.GA44_se=10^2./(data_cold.A_smooth.*data.GA44_molweight).*data_cold.GA44_se.*data_cold.dryweight;
data_cold.GA19_se=10^2./(data_cold.A_smooth.*data.GA19_molweight).*data_cold.GA19_se.*data_cold.dryweight;
data_cold.GA20_se=10^2./(data_cold.A_smooth.*data.GA20_molweight).*data_cold.GA20_se.*data_cold.dryweight;
data_cold.GA8_se=10^2./(data_cold.A_smooth.*data.GA8_molweight).*data_cold.GA8_se.*data_cold.dryweight;
data_cold.GA1_se=10^2./(data_cold.A_smooth.*data.GA1_molweight).*data_cold.GA1_se.*data_cold.dryweight;
data_cold.GA29_se=10^2./(data_cold.A_smooth.*data.GA29_molweight).*data_cold.GA29_se.*data_cold.dryweight;

data_cold.GA53_var=3*(data_cold.GA53_se).^2;
data_cold.GA44_var=3*(data_cold.GA44_se).^2;
data_cold.GA19_var=3*(data_cold.GA19_se).^2;
data_cold.GA20_var=3*(data_cold.GA20_se).^2;
data_cold.GA8_var=3*(data_cold.GA8_se).^2;
data_cold.GA1_var=3*(data_cold.GA1_se).^2;
data_cold.GA29_var=3*(data_cold.GA29_se).^2;

data_drought.GA53=10^2./(data_drought.A_smooth.*data.GA53_molweight).*data_drought.GA53.*data_drought.dryweight;
data_drought.GA44=10^2./(data_drought.A_smooth.*data.GA44_molweight).*data_drought.GA44.*data_drought.dryweight;
data_drought.GA19=10^2./(data_drought.A_smooth.*data.GA19_molweight).*data_drought.GA19.*data_drought.dryweight;
data_drought.GA20=10^2./(data_drought.A_smooth.*data.GA20_molweight).*data_drought.GA20.*data_drought.dryweight;
data_drought.GA8=10^2./(data_drought.A_smooth.*data.GA8_molweight).*data_drought.GA8.*data_drought.dryweight;
data_drought.GA1=10^2./(data_drought.A_smooth.*data.GA1_molweight).*data_drought.GA1.*data_drought.dryweight;
data_drought.GA29=10^2./(data_drought.A_smooth.*data.GA29_molweight).*data_drought.GA29.*data_drought.dryweight;

data_drought.GA53_se=10^2./(data_drought.A_smooth.*data.GA53_molweight).*data_drought.GA53_se.*data_drought.dryweight;
data_drought.GA44_se=10^2./(data_drought.A_smooth.*data.GA44_molweight).*data_drought.GA44_se.*data_drought.dryweight;
data_drought.GA19_se=10^2./(data_drought.A_smooth.*data.GA19_molweight).*data_drought.GA19_se.*data_drought.dryweight;
data_drought.GA20_se=10^2./(data_drought.A_smooth.*data.GA20_molweight).*data_drought.GA20_se.*data_drought.dryweight;
data_drought.GA8_se=10^2./(data_drought.A_smooth.*data.GA8_molweight).*data_drought.GA8_se.*data_drought.dryweight;
data_drought.GA1_se=10^2./(data_drought.A_smooth.*data.GA1_molweight).*data_drought.GA1_se.*data_drought.dryweight;
data_drought.GA29_se=10^2./(data_drought.A_smooth.*data.GA29_molweight).*data_drought.GA29_se.*data_drought.dryweight;

data_drought.GA53_var=3*(data_drought.GA53_se).^2;
data_drought.GA44_var=3*(data_drought.GA44_se).^2;
data_drought.GA19_var=3*(data_drought.GA19_se).^2;
data_drought.GA20_var=3*(data_drought.GA20_se).^2;
data_drought.GA8_var=3*(data_drought.GA8_se).^2;
data_drought.GA1_var=3*(data_drought.GA1_se).^2;
data_drought.GA29_var=3*(data_drought.GA29_se).^2;

% Read in enzyme data 
load  data_enzymes_3cases_summed.mat 
data.GA20ox=GA20ox_control;
data.GA3ox2=GA3ox2_control;
data.GA2ox=GA2ox_control;
data.pos_enz=10^4*pos_enz;

data_cold.GA20ox=GA20ox_cold;
data_cold.GA3ox2=GA3ox2_cold;
data_cold.GA2ox=GA2ox_cold;
data_cold.pos_enz=10^4*pos_enz;


data_drought.GA20ox=GA20ox_drought;
data_drought.GA3ox2=GA3ox2_drought;
data_drought.GA2ox=GA2ox_drought;
data_drought.pos_enz=10^4*pos_enz;


%save data_PCRenz_GAconc_nM_threecases_final_Ainterp_allenzpts.mat data data_cold data_drought



figure
title('Wild type GA concentrations in nM')
subplot(2,4,1)
hold on
errorbar(0.001*data.pos_GAs,data.GA53,data.GA53_se,'bs-','LineWidth',2)
%errorbar(data_conc.pos_GAs,data_conc.GA53_drought,data_conc.GA53_drought_se,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
%errorbar(data_conc.pos_GAs,data_conc.GA53_cold,data_conc.GA53_cold_se,'ro-','MarkerFaceColor',[1 0 0],'LineWidth',2)
%title('GA53')
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA53 (nM)','Interpreter','latex','FontSize',16)
box off

subplot(2,4,2)
hold on
errorbar(0.001*data.pos_GAs,data.GA44,data.GA44_se,'bs-','LineWidth',2)
%errorbar(data_conc.pos_GAs,data_conc.GA44_drought,data_conc.GA44_drought_se,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
%errorbar(data_conc.pos_GAs,data_conc.GA44_cold,data_conc.GA44_cold_se,'ro-','MarkerFaceColor',[1 0 0],'LineWidth',2)
%title('GA44')
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA44 (nM)','Interpreter','latex','FontSize',16)
box off

subplot(2,4,3)
hold on
errorbar(0.001*data.pos_GAs,data.GA19,data.GA19_se,'bs-','LineWidth',2)
%errorbar(data_conc.pos_GAs,data_conc.GA19_drought,data_conc.GA19_drought_se,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
%errorbar(data_conc.pos_GAs,data_conc.GA19_cold,data_conc.GA19_cold_se,'ro-','MarkerFaceColor',[1 0 0],'LineWidth',2)
%title('GA19')
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA19 (nM)','Interpreter','latex','FontSize',16)
box off

subplot(2,4,4)
hold on
errorbar(0.001*data.pos_GAs,data.GA20,data.GA20_se,'bs-','LineWidth',2)
%errorbar(data_conc.pos_GAs,data_conc.GA20_drought,data_conc.GA20_drought_se,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
%errorbar(data_conc.pos_GAs,data_conc.GA20_cold,data_conc.GA20_cold_se,'ro-','MarkerFaceColor',[1 0 0],'LineWidth',2)
%title('GA20')
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA20 (nM)','Interpreter','latex','FontSize',16)
box off

subplot(2,4,5)
hold on
errorbar(0.001*data.pos_GAs,data.GA1,data.GA1_se,'bs-','LineWidth',2)
%errorbar(data_conc.pos_GAs,data_conc.GA1_drought,data_conc.GA1_drought_se,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
%errorbar(data_conc.pos_GAs,data_conc.GA1_cold,data_conc.GA1_cold_se,'ro-','MarkerFaceColor',[1 0 0],'LineWidth',2)
%title('GA1')
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA1 (nM)','Interpreter','latex','FontSize',16)
box off

subplot(2,4,6)
hold on
errorbar(0.001*data.pos_GAs,data.GA29,data.GA29_se,'bs-','LineWidth',2)
%errorbar(data_conc.pos_GAs,data_conc.GA29_drought,data_conc.GA29_drought_se,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
%errorbar(data_conc.pos_GAs,data_conc.GA29_cold,data_conc.GA29_cold_se,'ro-','MarkerFaceColor',[1 0 0],'LineWidth',2)
%title('GA29')
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA29 (nM),','Interpreter','latex','FontSize',16)
box off

subplot(2,4,7)
hold on
errorbar(0.001*data.pos_GAs,data.GA8,data.GA8_se,'bs-','LineWidth',2)
%errorbar(pos_GAs,data_conc.GA8_drought,data_conc.GA8_drought_se,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
%errorbar(pos_GAs,data_conc.GA8_cold,data_conc.GA8_cold_se,'ro-','MarkerFaceColor',[1 0 0],'LineWidth',2)
%title('GA8')
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA8 (nM)','Interpreter','latex','FontSize',16)
box off

figure
title('GA concentrations in nM')
subplot(2,4,1)
hold on
plot(0.001*data.pos_GAs,data.GA53,'bs-','LineWidth',2)
plot(0.001*data_drought.pos_GAs,data_drought.GA53,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
plot(0.001*data_cold.pos_GAs,data_cold.GA53,'ro-','MarkerFaceColor',[1 0 0],'LineWidth',2)
title('GA53','Interpreter','latex','FontSize',18)
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA53 (nM)','Interpreter','latex','FontSize',16)
box off

subplot(2,4,2)
hold on
plot(0.001*data.pos_GAs,data.GA44,'bs-','LineWidth',2)
plot(0.001*data_drought.pos_GAs,data_drought.GA44,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
plot(0.001*data_cold.pos_GAs,data_cold.GA44,'ro-','MarkerFaceColor',[1 0 0],'LineWidth',2)
title('GA44','Interpreter','latex','FontSize',18)
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA44 (nM)','Interpreter','latex','FontSize',16)
box off

subplot(2,4,3)
hold on
plot(0.001*data.pos_GAs,data.GA19,'bs-','LineWidth',2)
plot(0.001*data_drought.pos_GAs,data_drought.GA19,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
plot(0.001*data_cold.pos_GAs,data_cold.GA19,'ro-','MarkerFaceColor',[1 0 0],'LineWidth',2)
title('GA19','Interpreter','latex','FontSize',18)
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA19 (nM)','Interpreter','latex','FontSize',16)
box off

subplot(2,4,4)
hold on
plot(0.001*data.pos_GAs,data.GA20,'bs-','LineWidth',2)
plot(0.001*data_drought.pos_GAs,data_drought.GA20,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
plot(0.001*data_cold.pos_GAs,data_cold.GA20,'ro-','MarkerFaceColor',[1 0 0],'LineWidth',2)
title('GA20','Interpreter','latex','FontSize',18)
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA20 (nM)','Interpreter','latex','FontSize',16)
box off

subplot(2,4,5)
hold on
plot(0.001*data.pos_GAs,data.GA1,'bs-','LineWidth',2)
plot(0.001*data_drought.pos_GAs,data_drought.GA1,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
plot(0.001*data_cold.pos_GAs,data_cold.GA1,'ro-','MarkerFaceColor',[1 0 0],'LineWidth',2)
title('GA1','Interpreter','latex','FontSize',18)
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA1 (nM)','Interpreter','latex','FontSize',16)
box off

subplot(2,4,6)
hold on
plot(0.001*data.pos_GAs,data.GA29,'bs-','LineWidth',2)
plot(0.001*data_drought.pos_GAs,data_drought.GA29,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
plot(0.001*data_cold.pos_GAs,data_cold.GA29,'ro-','MarkerFaceColor',[1 0 0],'LineWidth',2)
title('GA29','Interpreter','latex','FontSize',18)
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA29 (nM),','Interpreter','latex','FontSize',16)
box off

subplot(2,4,7)
hold on
plot(0.001*data.pos_GAs,data.GA8,'bs-','LineWidth',2)
plot(0.001*data_drought.pos_GAs,data_drought.GA8,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
plot(0.001*data_cold.pos_GAs,data_cold.GA8,'ro-','MarkerFaceColor',[1 0 0],'LineWidth',2)
title('GA8','Interpreter','latex','FontSize',18)
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA8 (nM)','Interpreter','latex','FontSize',16)
box off

legend1=legend('Control','Drought','Cold')
set(legend1,'Interpreter','latex','Location','SouthEast','FontSize',24)

figure
yyaxis left
errorbar(0.001*data.pos_GAs,data.GA1_con,data.GA1_con_se,'bs-','LineWidth',2)
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA1 measurements (ng/gDW)','Interpreter','latex','FontSize',16)
yyaxis right
plot(0.001*data.pos_GAs,data.GA1,'r*-','LineWidth',2)
ylabel('GA1 concentrations (nM)','Interpreter','latex','FontSize',16)
legend('GA1 measurements  (ng/gDW)','GA1 concentrations (nM)')
ylim([0,0.5])
box off

end

