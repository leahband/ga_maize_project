% Uses data on cell lengths and velocities to calculate RER and cell
% production rate. Adjusts values to assume that cell growth ceases at the
% position where the RER first reaches zero (see equation 2.38 in the Supplementary text)

% First loads data calculated from calculatingvelocity_droughtcold.m

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
%  Cold and drought
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear


load kinematicdata_3cases_veladjusted.mat  % Gives position, cell lengths, velocities and number of cells.
dx=position(2)-position(1); % [microns]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Control calculations:
%Calculate RER=dv/dx :
for j=2:N-1;
    RER_wt(j)=(velocity_wt(j+1)-velocity_wt(j-1))/(2*dx);
end
RER_wt(1)=1/(2*dx)*(-3*velocity_wt(1)+4*velocity_wt(2)-velocity_wt(3));
RER_wt(N)=1/(2*dx)*(3*velocity_wt(N)-4*velocity_wt(N-1)+velocity_wt(N-2));

% Calculate cell production rate P=dF/dx
for j=2:N-1;
    P_wt(j)=(F_wt(j+1)-F_wt(j-1))/(2*dx);
end
P_wt(1)=1/(2*dx)*(-3*F_wt(1)+4*F_wt(2)-F_wt(3));
P_wt(N)=1/(2*dx)*(3*F_wt(N)-4*F_wt(N-1)+F_wt(N-2));

d_wt=P_wt.*cell_length_wt';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Cold calculations:
%Calculate RER=dv/dx :
for j=2:N-1;
    RER_cold(j)=(velocity_cold(j+1)-velocity_cold(j-1))/(2*dx);
end
RER_cold(1)=1/(2*dx)*(-3*velocity_cold(1)+4*velocity_cold(2)-velocity_cold(3));
RER_cold(N)=1/(2*dx)*(3*velocity_cold(N)-4*velocity_cold(N-1)+velocity_cold(N-2));

% Calculate cell production rate P=dF/dx
for j=2:N-1;
    P_cold(j)=(F_cold(j+1)-F_cold(j-1))/(2*dx);
end
P_cold(1)=1/(2*dx)*(-3*F_cold(1)+4*F_cold(2)-F_cold(3));
P_cold(N)=1/(2*dx)*(3*F_cold(N)-4*F_cold(N-1)+F_cold(N-2));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Drought calculations:
% Calculate RER=dv/dx :
for j=2:N-1;
    RER_drought(j)=(velocity_drought(j+1)-velocity_drought(j-1))/(2*dx);
end
RER_drought(1)=1/(2*dx)*(-3*velocity_drought(1)+4*velocity_drought(2)-velocity_drought(3));
RER_drought(N)=1/(2*dx)*(3*velocity_drought(N)-4*velocity_drought(N-1)+velocity_drought(N-2));

% Calculate cell production rate P=dF/dx
for j=2:N-1;
    P_drought(j)=(F_drought(j+1)-F_drought(j-1))/(2*dx);
end
P_drought(1)=1/(2*dx)*(-3*F_drought(1)+4*F_drought(2)-F_drought(3));
P_drought(N)=1/(2*dx)*(3*F_drought(N)-4*F_drought(N-1)+F_drought(N-2));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot cell lengths, velocities, RER, F, and P:

positionmm=1/1000*position;

figure
subplot(3,3,1)
hold on 
plot(positionmm, cell_length_wt,'LineWidth',2)
plot(positionmm, cell_length_cold,'r','LineWidth',2)
plot(positionmm, cell_length_drought,'g','LineWidth',2)
hold on
xlabel('Distance, x, (mm)','Interpreter','latex')
ylabel('l(x), ($\mu$m)','Interpreter','latex')
title('Measured cell length, l(x)','Interpreter','latex')
box('off')
%set(gca,'FontSize',20)

subplot(3,3,2)
hold on 
plot(positionmm, velocity_wt,'LineWidth',2)
plot(positionmm, velocity_cold,'r','LineWidth',2)
plot(positionmm, velocity_drought,'g','LineWidth',2)
xlabel('Distance, x, (mm)','Interpreter','latex')
ylabel('v(x), ($\mu$m/h)','Interpreter','latex')
title('Measured velocity, v(x)','Interpreter','latex')
box('off')
%set(gca,'FontSize',20)

subplot(3,3,3)
hold on 
plot(positionmm, RER_wt,'LineWidth',2)
plot(positionmm, RER_cold,'r','LineWidth',2)
plot(positionmm, RER_drought,'g','LineWidth',2)
xlabel('Distance, x, (mm)','Interpreter','latex')
ylabel('RER(x), (1/h)','Interpreter','latex')
title('Relative Elongation Rate, RER=dv/dx','Interpreter','latex')
box('off')
%set(gca,'FontSize',20)

subplot(3,3,4)
hold on 
plot(positionmm, F_wt,'LineWidth',2)
plot(positionmm, F_cold,'r','LineWidth',2)
plot(positionmm, F_drought,'g','LineWidth',2)
xlabel('Distance, x, (mm)','Interpreter','latex')
ylabel('F(x), (1/h)','Interpreter','latex')
title('Cell flux, F=v/l','Interpreter','latex')
box('off')
%set(gca,'FontSize',20)

subplot(3,3,5)
hold on 
plot(positionmm, P_wt,'LineWidth',2)
plot(positionmm, P_cold,'r','LineWidth',2)
plot(positionmm, P_drought,'g','LineWidth',2)
xlabel('Distance, x, (mm)','Interpreter','latex')
ylabel('P(x), (cells/($\mu$m \, h)','Interpreter','latex')
title('Production rate, P=dF/dx','Interpreter','latex')
box('off')
l=legend('Control','Low temperature','Drought')
set(l,'interpreter','latex','Location','EastOutside','Box', 'off')
%set(gca,'FontSize',20)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Adjust data

% Once RER<0, assume we are in the mature zone, where RER=0, and velocities and cell-lengths are constant. 
for i=1:N
    if RER_wt(i)<10^(-8)
        RER_wt(i:end)=0;
        velocity_wt(i:end)=velocity_wt(i);
        cell_length_wt(i:end)=cell_length_wt(i);
    end
end
for i=1:N
    if RER_cold(i)<10^(-8)
        RER_cold(i:end)=0;
        velocity_cold(i:end)=velocity_cold(i);
        cell_length_cold(i:end)=cell_length_cold(i);
    end
end
for i=1:N
    if RER_drought(i)<10^(-8)
        RER_drought(i:end)=0;
        velocity_drought(i:end)=velocity_drought(i);
        cell_length_drought(i:end)=cell_length_drought(i);
    end
end



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Extend x domain into the mature zone.
% 
noextra=(100000-position(end))/dx;
position=1/1000*[position',position(end)+dx:dx:100000];

RER_wt=[RER_wt,zeros(1,noextra)];
d_wt=[d_wt,zeros(1,noextra)];
cell_length_wt=[cell_length_wt',cell_length_wt(end)*ones(1,noextra)];
velocity_wt=[velocity_wt',velocity_wt(end)*ones(1,noextra)];

RER_cold=[RER_cold,zeros(1,noextra)];
cell_length_cold=[cell_length_cold',cell_length_cold(end)*ones(1,noextra)];
velocity_cold=[velocity_cold',velocity_cold(end)*ones(1,noextra)];

RER_drought=[RER_drought,zeros(1,noextra)];
cell_length_drought=[cell_length_drought',cell_length_drought(end)*ones(1,noextra)];
velocity_drought=[velocity_drought',velocity_drought(end)*ones(1,noextra)];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot adjusted data cell lengths, velocities, RER, F, and P:
%figure
subplot(3,3,7)
hold on 
plot(position, cell_length_wt,'LineWidth',2)
plot(position, cell_length_cold,'r','LineWidth',2)
plot(position, cell_length_drought,'g','LineWidth',2)
hold on
xlabel('Distance, x, (mm)','Interpreter','latex')
ylabel('l(x), ($\mu$m)','Interpreter','latex')
title('Adjusted cell length, l(x)','Interpreter','latex')
box('off')
%set(gca,'FontSize',20)

subplot(3,3,8)
hold on 
plot(position, velocity_wt,'LineWidth',2)
plot(position, velocity_cold,'r','LineWidth',2)
plot(position, velocity_drought,'g','LineWidth',2)
xlabel('Distance, x, (mm)','Interpreter','latex')
ylabel('v(x), ($\mu$m/h)','Interpreter','latex')
title('Adjusted velocity, v(x)','Interpreter','latex')
box('off')
%set(gca,'FontSize',20)

subplot(3,3,9)
hold on 
plot(position, RER_wt,'LineWidth',2)
plot(position, RER_cold,'r','LineWidth',2)
plot(position, RER_drought,'g','LineWidth',2)
xlabel('Distance, x, (mm)','Interpreter','latex')
ylabel('RER(x), (1/h)','Interpreter','latex')
title('Adjusted Relative Elongation Rate, RER=dv/dx','Interpreter','latex')
box('off')
%set(gca,'FontSize',20)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot control data figure for presentation:
figure
subplot(1,4,1)
hold on 
plot(position, cell_length_wt,'b','LineWidth',2)
plot(17,0,'b.','MarkerSize',10,'LineWidth',2)
hold on
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('Cell length ($\mu$m)','Interpreter','latex','FontSize',10)
%title('Measured cell length, l(x)','Interpreter','latex')
box('off')
%xlim([0 120])
%set(gca,'FontSize',20)

subplot(1,4,2)
hold on 
plot(position, velocity_wt,'b','LineWidth',2)
plot(17,0,'b.','MarkerSize',10,'LineWidth',2)
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('Velocity ($\mu$m/h)','Interpreter','latex','FontSize',10)
%title('Measured velocity, v(x)','Interpreter','latex')
box('off')
%xlim([0 120])
%set(gca,'FontSize',20)

subplot(1,4,3)
hold on 
plot(position, RER_wt,'b','LineWidth',2)
plot(17,0,'b.','MarkerSize',10,'LineWidth',2)
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('RER (1/h)','Interpreter','latex','FontSize',10)
%title('Relative Elongation Rate, RER=dv/dx','Interpreter','latex')
box('off')
%xlim([0 120])

subplot(1,4,4)
hold on 
plot(position, d_wt,'b','LineWidth',2)
plot(17,0,'b.','MarkerSize',10,'LineWidth',2)
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('Cell division rate (1/h)','Interpreter','latex','FontSize',10)
%title('Relative Elongation Rate, RER=dv/dx','Interpreter','latex')
box('off')
%xlim([0 120])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot figure with three cases for presentation:
figure
subplot(1,3,1)
hold on 
plot(position, cell_length_wt,'LineWidth',2)
plot(position, cell_length_cold,'g','LineWidth',2)
plot(position, cell_length_drought,'r','LineWidth',2)
hold on
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('Cell length ($\mu$m)','Interpreter','latex','FontSize',16)
%title('Measured cell length, l(x)','Interpreter','latex')
box('off')
%xlim([0 120])
%set(gca,'FontSize',20)

subplot(1,3,2)
hold on 
plot(position, velocity_wt,'LineWidth',2)
plot(position, velocity_cold,'g','LineWidth',2)
plot(position, velocity_drought,'r','LineWidth',2)
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('Velocity ($\mu$m/h)','Interpreter','latex','FontSize',16)
%title('Measured velocity, v(x)','Interpreter','latex')
box('off')
%xlim([0 120])
%set(gca,'FontSize',20)

subplot(1,3,3)
hold on 
plot(position, RER_wt,'LineWidth',2)
plot(position, RER_cold,'g','LineWidth',2)
plot(position, RER_drought,'r','LineWidth',2)
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('RER (1/h)','Interpreter','latex','FontSize',16)
%title('Relative Elongation Rate, RER=dv/dx','Interpreter','latex')
box('off')
%xlim([0 120])





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
% Calculate boundaries of growth zones
%
meripos_wt=length(P_wt(P_wt>10^(-8)))
merisize_wt=50*(meripos_wt-1)  % Get minus 1 as position(1)=0;
mericelllength_wt=cell_length_wt(meripos_wt)  
EZpos_wt=length(RER_wt(RER_wt>10^(-8)))
EZsize_wt=50*(EZpos_wt-1)
finalcelllength_wt=cell_length_wt(EZpos_wt)
EZlength_wt=EZsize_wt-merisize_wt
EZ_celllengthincrease_wt=finalcelllength_wt-mericelllength_wt
EZ_relativecelllengthincrease_wt=EZ_celllengthincrease_wt/mericelllength_wt

meripos_cold=length(P_cold(P_cold>10^(-8)))
merisize_cold=50*(meripos_cold-1)  
mericelllength_cold=cell_length_cold(meripos_cold)
EZpos_cold=length(RER_cold(RER_cold>10^(-8)))
EZsize_cold=50*(EZpos_cold-1)
finalcelllength_cold=cell_length_cold(EZpos_cold)
EZlength_cold=EZsize_cold-merisize_cold
EZ_celllengthincrease_cold=finalcelllength_cold-mericelllength_cold

meripos_drought=length(P_drought(P_drought>10^(-8)))
merisize_drought=50*(meripos_drought-1)  
mericelllength_drought=cell_length_drought(meripos_drought)
EZpos_drought=length(RER_drought(RER_drought>10^(-8)))
EZsize_drought=50*(EZpos_drought-1)
finalcelllength_drought=cell_length_drought(EZpos_drought)
EZlength_drought=EZsize_drought-merisize_drought
EZ_celllengthincrease_drought=finalcelllength_drought-mericelllength_drought

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
% Save
%save kinematicdata_3cases_calc_velandmericorrected.mat







