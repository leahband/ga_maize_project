load enzymeactivitymeasurements_GA20withGA2oxandGA3ox_B104andB73.mat

figure
hold on
errorbar(x,B73_GA1,B73_GA1_se,'b','LineWidth',2)
errorbar(x,B73_GA29,B73_GA29_se,'r','LineWidth',2)
legend('GA_1','GA_{29}')
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)
ylabel('Conc (nM)','Interpreter','latex','FontSize',10)
box off
ylim([0 0.08])

figure
hold on
errorbar(x,B104_GA1,B104_GA1_se,'b','LineWidth',2)
errorbar(x,B104_GA29,B104_GA29_se,'r','LineWidth',2)
legend('GA_1','GA_{29}')
xlabel('Distance (mm)','Interpreter','latex','FontSize',10)

ylabel('Conc (nM)','Interpreter','latex','FontSize',10)
box off
ylim([0 0.08])

