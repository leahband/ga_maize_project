% 

load enzymeactivitydata_droughtcold.mat

figure
subplot(2,4,1)
hold on
errorbar(pos,GA53_control,GA53_control_se)
errorbar(pos,GA53_drought,GA53_drought_se,'g')
errorbar(pos,GA53_cold,GA53_cold_se,'r')
%legend1=legend('Control','Drought','Cold')
title('GA_{53} degradation rate','FontSize',16)
xlabel('Dist (mm)')

subplot(2,4,2)
hold on
errorbar(pos,GA44_control,GA44_control_se)
errorbar(pos,GA44_drought,GA44_drought_se,'g')
errorbar(pos,GA44_cold,GA44_cold_se,'r')
%legend1=legend('Control','Drought','Cold')
title('GA_{44} degradation rate','FontSize',16)
xlabel('Dist (mm)')

subplot(2,4,3)
hold on
errorbar(pos,GA19_control,GA19_control_se)
errorbar(pos,GA19_drought,GA19_drought_se,'g')
errorbar(pos,GA19_cold,GA19_cold_se,'r')
%legend1=legend('Control','Drought','Cold')
title('GA_{19} degradation rate','FontSize',16)
xlabel('Dist (mm)')

subplot(2,4,4)
hold on
errorbar(pos,GA20_control,GA20_control_se)
errorbar(pos,GA20_drought,GA20_drought_se,'g')
errorbar(pos,GA20_cold,GA20_cold_se,'r')
%legend1=legend('Control','Drought','Cold')
title('GA_{20} degradation rate','FontSize',16)
xlabel('Dist (mm)')

subplot(2,4,5)
hold on
errorbar(pos,GA1_control,GA1_control_se)
errorbar(pos,GA1_drought,GA1_drought_se,'g')
errorbar(pos,GA1_cold,GA1_cold_se,'r')
%legend1=legend('Control','Drought','Cold')
title('GA_{1} degradation rate','FontSize',16)
xlabel('Dist (mm)')

subplot(2,4,6)
hold on
errorbar(pos,GA29_control,GA29_control_se)
errorbar(pos,GA29_drought,GA29_drought_se,'g')
errorbar(pos,GA29_cold,GA29_cold_se,'r')
%legend1=legend('Control','Drought','Cold')
title('GA_{29} degradation rate','FontSize',16)
xlabel('Dist (mm)')

subplot(2,4,7)
hold on
errorbar(pos,GA8_control,GA8_control_se)
errorbar(pos,GA8_drought,GA8_drought_se,'g')
errorbar(pos,GA8_cold,GA8_cold_se,'r')
legend1=legend('Control','Drought','Cold')
title('GA_{8} degradation rate','FontSize',16)
xlabel('Dist (mm)')


% To calculate the rate constant, we need to divide metabolite levels with
% mRNA, but each have datapoints at different x values, so the following
% code uses linear interpolation to show data on new position vector,
% 'u_pos'

x=pos;
u_pos = [x(1):0.1:x(end)]';
[ks,k]=histc(u_pos,x) %histc(x,binranges) counts the number of values in x that are within each specified bin range. 
% k is the bin number of each x entry

% Puts final entry into final bin
n = length(x);
k(k == n) = n - 1;

t = (u_pos - x(k))./(x(k+1) - x(k));

y=GA19_control;
GA19_control_int = (1-t).*y(k) + t.*y(k+1);
y=GA19_control_se;
GA19_control_se_int = (1-t).*y(k) + t.*y(k+1);

y=GA19_cold;
GA19_cold_int = (1-t).*y(k) + t.*y(k+1);
y=GA19_cold_se;
GA19_cold_se_int = (1-t).*y(k) + t.*y(k+1);

y=GA19_drought;
GA19_drought_int = (1-t).*y(k) + t.*y(k+1);
y=GA19_drought_se;
GA19_drought_se_int = (1-t).*y(k) + t.*y(k+1);

y=GA44_control;
GA44_control_int = (1-t).*y(k) + t.*y(k+1);
y=GA44_control_se;
GA44_control_se_int = (1-t).*y(k) + t.*y(k+1);

y=GA44_cold;
GA44_cold_int = (1-t).*y(k) + t.*y(k+1);
y=GA44_cold_se;
GA44_cold_se_int = (1-t).*y(k) + t.*y(k+1);

y=GA44_drought;
GA44_drought_int = (1-t).*y(k) + t.*y(k+1);
y=GA44_drought_se;
GA44_drought_se_int = (1-t).*y(k) + t.*y(k+1);

y=GA53_control;
GA53_control_int = (1-t).*y(k) + t.*y(k+1);
y=GA53_control_se;
GA53_control_se_int = (1-t).*y(k) + t.*y(k+1);

y=GA53_cold;
GA53_cold_int = (1-t).*y(k) + t.*y(k+1);
y=GA53_cold_se;
GA53_cold_se_int = (1-t).*y(k) + t.*y(k+1);

y=GA53_drought;
GA53_drought_int = (1-t).*y(k) + t.*y(k+1);
y=GA53_drought_se;
GA53_drought_se_int = (1-t).*y(k) + t.*y(k+1);

y=GA1_control;
GA1_control_int = (1-t).*y(k) + t.*y(k+1);
y=GA1_control_se;
GA1_control_se_int = (1-t).*y(k) + t.*y(k+1);

y=GA1_cold;
GA1_cold_int = (1-t).*y(k) + t.*y(k+1);
y=GA1_cold_se;
GA1_cold_se_int = (1-t).*y(k) + t.*y(k+1);

y=GA1_drought;
GA1_drought_int = (1-t).*y(k) + t.*y(k+1);
y=GA1_drought_se;
GA1_drought_se_int = (1-t).*y(k) + t.*y(k+1);

y=GA8_control;
GA8_control_int = (1-t).*y(k) + t.*y(k+1);
y=GA8_control_se;
GA8_control_se_int = (1-t).*y(k) + t.*y(k+1);

y=GA8_cold;
GA8_cold_int = (1-t).*y(k) + t.*y(k+1);
y=GA8_cold_se;
GA8_cold_se_int = (1-t).*y(k) + t.*y(k+1);

y=GA8_drought;
GA8_drought_int = (1-t).*y(k) + t.*y(k+1);
y=GA8_drought_se;
GA8_drought_se_int = (1-t).*y(k) + t.*y(k+1);

y=GA20_control;
GA20_control_int = (1-t).*y(k) + t.*y(k+1);
y=GA20_control_se;
GA20_control_se_int = (1-t).*y(k) + t.*y(k+1);

y=GA20_cold;
GA20_cold_int = (1-t).*y(k) + t.*y(k+1);
y=GA20_cold_se;
GA20_cold_se_int = (1-t).*y(k) + t.*y(k+1);

y=GA20_drought;
GA20_drought_int = (1-t).*y(k) + t.*y(k+1);
y=GA20_drought_se;
GA20_drought_se_int = (1-t).*y(k) + t.*y(k+1);

y=GA29_control;
GA29_control_int = (1-t).*y(k) + t.*y(k+1);
y=GA29_control_se;
GA29_control_se_int = (1-t).*y(k) + t.*y(k+1);

y=GA29_cold;
GA29_cold_int = (1-t).*y(k) + t.*y(k+1);
y=GA29_cold_se;
GA29_cold_se_int = (1-t).*y(k) + t.*y(k+1);

y=GA29_drought;
GA29_drought_int = (1-t).*y(k) + t.*y(k+1);
y=GA29_drought_se;
GA29_drought_se_int = (1-t).*y(k) + t.*y(k+1);


load data_PCRenz_control.mat 
load data_PCRenz_cold.mat 
load data_PCRenz_drought.mat 

x=1/1000*data.pos_enz;
u = [x(1):0.1:x(end)]';
[ks,k]=histc(u,x)
n = length(x);
k(k == n) = n - 1;
t = (u - x(k))./(x(k+1) - x(k));

y=data.GA20ox;
GA20ox_control_int = (1-t).*y(k) + t.*y(k+1);
y=data.GA20ox_std;
GA20ox_control_std_int = (1-t).*y(k) + t.*y(k+1);

y=data.GA3ox2;
GA3ox_control_int = (1-t).*y(k) + t.*y(k+1);
y=data.GA3ox2_std;
GA3ox_control_std_int = (1-t).*y(k) + t.*y(k+1);

y=data.GA2ox;
GA2ox_control_int = (1-t).*y(k) + t.*y(k+1);
y=data.GA2ox_std;
GA2ox_control_std_int = (1-t).*y(k) + t.*y(k+1);

y=data_cold.GA20ox;
GA20ox_cold_int = (1-t).*y(k) + t.*y(k+1);
y=data_cold.GA20ox_std;
GA20ox_cold_std_int = (1-t).*y(k) + t.*y(k+1);

y=data_cold.GA3ox2;
GA3ox_cold_int = (1-t).*y(k) + t.*y(k+1);
y=data_cold.GA3ox2_std;
GA3ox_cold_std_int = (1-t).*y(k) + t.*y(k+1);

y=data_cold.GA2ox;
GA2ox_cold_int = (1-t).*y(k) + t.*y(k+1);
y=data_cold.GA2ox_std;
GA2ox_cold_std_int = (1-t).*y(k) + t.*y(k+1);

y=data_drought.GA20ox;
GA20ox_drought_int = (1-t).*y(k) + t.*y(k+1);
y=data_drought.GA20ox_std;
GA20ox_drought_std_int = (1-t).*y(k) + t.*y(k+1);

y=data_drought.GA3ox2;
GA3ox_drought_int = (1-t).*y(k) + t.*y(k+1);
y=data_drought.GA3ox2_std;
GA3ox_drought_std_int = (1-t).*y(k) + t.*y(k+1);

y=data_drought.GA2ox;
GA2ox_drought_int = (1-t).*y(k) + t.*y(k+1);
y=data_drought.GA2ox_std;
GA2ox_drought_std_int = (1-t).*y(k) + t.*y(k+1);

lambda53_control=GA53_control_int./GA20ox_control_int(26:end);
lambda53_control_std=sqrt( (GA53_control_int./GA20ox_control_int(26:end)).^2.*(3*GA53_control_se_int.^2./GA53_control_int.^2+GA20ox_control_std_int(26:end).^2./GA20ox_control_int(26:end).^2));
lambda53_cold=GA53_cold_int./GA20ox_cold_int(26:end);
lambda53_cold_std=sqrt( (GA53_cold_int./GA20ox_cold_int(26:end)).^2.*(3*GA53_cold_se_int.^2./GA53_cold_int.^2+GA20ox_cold_std_int(26:end).^2./GA20ox_cold_int(26:end).^2));
lambda53_drought=GA53_drought_int./GA20ox_drought_int(26:end);
lambda53_drought_std=sqrt( (GA53_drought_int./GA20ox_drought_int(26:end)).^2.*(3*GA53_drought_se_int.^2./GA53_drought_int.^2+GA20ox_drought_std_int(26:end).^2./GA20ox_drought_int(26:end).^2));


lambda44_control=GA44_control_int./GA20ox_control_int(26:end);
lambda44_control_std=sqrt( (GA44_control_int./GA20ox_control_int(26:end)).^2.*(3*GA44_control_se_int.^2./GA44_control_int.^2+GA20ox_control_std_int(26:end).^2./GA20ox_control_int(26:end).^2));
lambda44_cold=GA44_cold_int./GA20ox_cold_int(26:end);
lambda44_cold_std=sqrt( (GA44_cold_int./GA20ox_cold_int(26:end)).^2.*(3*GA44_cold_se_int.^2./GA44_cold_int.^2+GA20ox_cold_std_int(26:end).^2./GA20ox_cold_int(26:end).^2));
lambda44_drought=GA44_drought_int./GA20ox_drought_int(26:end);
lambda44_drought_std=sqrt( (GA44_drought_int./GA20ox_drought_int(26:end)).^2.*(3*GA44_drought_se_int.^2./GA44_drought_int.^2+GA20ox_drought_std_int(26:end).^2./GA20ox_drought_int(26:end).^2));


lambda19_control=GA19_control_int./GA20ox_control_int(26:end);
lambda19_control_std=sqrt( (GA19_control_int./GA20ox_control_int(26:end)).^2.*(3*GA19_control_se_int.^2./GA19_control_int.^2+GA20ox_control_std_int(26:end).^2./GA20ox_control_int(26:end).^2));
lambda19_cold=GA19_cold_int./GA20ox_cold_int(26:end);
lambda19_cold_std=sqrt( (GA19_cold_int./GA20ox_cold_int(26:end)).^2.*(3*GA19_cold_se_int.^2./GA19_cold_int.^2+GA20ox_cold_std_int(26:end).^2./GA20ox_cold_int(26:end).^2));
lambda19_drought=GA19_drought_int./GA20ox_drought_int(26:end);
lambda19_drought_std=sqrt( (GA19_drought_int./GA20ox_drought_int(26:end)).^2.*(3*GA19_drought_se_int.^2./GA19_drought_int.^2+GA20ox_drought_std_int(26:end).^2./GA20ox_drought_int(26:end).^2));

gamma1_control=GA1_control_int./GA2ox_control_int(26:end);
gamma1_control_std=sqrt( (GA1_control_int./GA2ox_control_int(26:end)).^2.*(3*GA1_control_se_int.^2./GA1_control_int.^2+GA2ox_control_std_int(26:end).^2./GA2ox_control_int(26:end).^2));
gamma1_cold=GA1_cold_int./GA2ox_cold_int(26:end);
gamma1_cold_std=sqrt( (GA1_cold_int./GA2ox_cold_int(26:end)).^2.*(3*GA1_cold_se_int.^2./GA1_cold_int.^2+GA2ox_cold_std_int(26:end).^2./GA2ox_cold_int(26:end).^2));
gamma1_drought=GA1_drought_int./GA2ox_drought_int(26:end);
gamma1_drought_std=sqrt( (GA1_drought_int./GA2ox_drought_int(26:end)).^2.*(3*GA1_drought_se_int.^2./GA1_drought_int.^2+GA2ox_drought_std_int(26:end).^2./GA2ox_drought_int(26:end).^2));


gamma29_control=GA29_control_int./GA2ox_control_int(26:end);
gamma29_control_std=sqrt( (GA29_control_int./GA2ox_control_int(26:end)).^2.*(3*GA29_control_se_int.^2./GA29_control_int.^2+GA2ox_control_std_int(26:end).^2./GA2ox_control_int(26:end).^2));
gamma29_cold=GA29_cold_int./GA2ox_cold_int(26:end);
gamma29_cold_std=sqrt( (GA29_cold_int./GA2ox_cold_int(26:end)).^2.*(3*GA29_cold_se_int.^2./GA29_cold_int.^2+GA2ox_cold_std_int(26:end).^2./GA2ox_cold_int(26:end).^2));
gamma29_drought=GA29_drought_int./GA2ox_drought_int(26:end);
gamma29_drought_std=sqrt( (GA29_drought_int./GA2ox_drought_int(26:end)).^2.*(3*GA29_drought_se_int.^2./GA29_drought_int.^2+GA2ox_drought_std_int(26:end).^2./GA2ox_drought_int(26:end).^2));


gamma8_control=GA8_control_int./GA2ox_control_int(26:end);
gamma8_control_std=sqrt((GA8_control_int./GA2ox_control_int(26:end)).^2.*(3*GA8_control_se_int.^2./GA8_control_int.^2+GA2ox_control_std_int(26:end).^2./GA2ox_control_int(26:end).^2));
gamma8_cold=GA8_cold_int./GA2ox_cold_int(26:end);
gamma8_cold_std=sqrt((GA8_cold_int./GA2ox_cold_int(26:end)).^2.*(3*GA8_cold_se_int.^2./GA8_cold_int.^2+GA2ox_cold_std_int(26:end).^2./GA2ox_cold_int(26:end).^2));
gamma8_drought=GA8_drought_int./GA2ox_drought_int(26:end);
gamma8_drought_std=sqrt((GA8_drought_int./GA2ox_drought_int(26:end)).^2.*(3*GA8_drought_se_int.^2./GA8_drought_int.^2+GA2ox_drought_std_int(26:end).^2./GA2ox_drought_int(26:end).^2));

figure
subplot(2,3,1)
hold on
plot(u_pos,lambda53_control)
plot(u_pos,lambda53_drought,'g')
plot(u_pos,lambda53_cold,'r')
errorbar(u_pos(1:100:end),lambda53_control(1:100:end),lambda53_control_std(1:100:end),'x')
errorbar(u_pos(1:100:end),lambda53_drought(1:100:end),lambda53_drought_std(1:100:end),'gx')
errorbar(u_pos(1:100:end),lambda53_cold(1:100:end),lambda53_cold_std(1:100:end),'rx')
legend1=legend('Control','Drought','Cold')
title('lambda_{53}')
xlabel('Dist (mm)')

subplot(2,3,2)
hold on
plot(u_pos,lambda44_control)
plot(u_pos,lambda44_drought,'g')
plot(u_pos,lambda44_cold,'r')
errorbar(u_pos(1:100:end),lambda44_control(1:100:end),lambda44_control_std(1:100:end),'x')
errorbar(u_pos(1:100:end),lambda44_drought(1:100:end),lambda44_drought_std(1:100:end),'gx')
errorbar(u_pos(1:100:end),lambda44_cold(1:100:end),lambda44_cold_std(1:100:end),'rx')
%legend1=legend('Control','Drought','Cold')
title('lambda_{44}')
xlabel('Dist (mm)')

subplot(2,3,3)
hold on
plot(u_pos,lambda19_control)
plot(u_pos,lambda19_drought,'g')
plot(u_pos,lambda19_cold,'r')
errorbar(u_pos(1:100:end),lambda19_control(1:100:end),lambda19_control_std(1:100:end),'x')
errorbar(u_pos(1:100:end),lambda19_drought(1:100:end),lambda19_drought_std(1:100:end),'gx')
errorbar(u_pos(1:100:end),lambda19_cold(1:100:end),lambda19_cold_std(1:100:end),'rx')
%legend1=legend('Control','Drought','Cold')
title('lambda_{19}')
xlabel('Dist (mm)')

subplot(2,3,4)
hold on
plot(u_pos,gamma1_control)
plot(u_pos,gamma1_drought,'g')
plot(u_pos,gamma1_cold,'r')
errorbar(u_pos(1:100:end),gamma1_control(1:100:end),gamma1_control_std(1:100:end),'x')
errorbar(u_pos(1:100:end),gamma1_drought(1:100:end),gamma1_drought_std(1:100:end),'gx')
errorbar(u_pos(1:100:end),gamma1_cold(1:100:end),gamma1_cold_std(1:100:end),'rx')
%legend1=legend('Control','Drought','Cold')
title('gamma_{1}')
xlabel('Dist (mm)')

subplot(2,3,5)
hold on
plot(u_pos,gamma29_control)
plot(u_pos,gamma29_drought,'g')
plot(u_pos,gamma29_cold,'r')
errorbar(u_pos(1:100:end),gamma29_control(1:100:end),gamma29_control_std(1:100:end),'x')
errorbar(u_pos(1:100:end),gamma29_drought(1:100:end),gamma29_drought_std(1:100:end),'gx')
errorbar(u_pos(1:100:end),gamma29_cold(1:100:end),gamma29_cold_std(1:100:end),'rx')
%legend1=legend('Control','Drought','Cold')
title('gamma_{29}')
xlabel('Dist (mm)')

subplot(2,3,6)
hold on
plot(u_pos,gamma8_control)
plot(u_pos,gamma8_drought,'g')
plot(u_pos,gamma8_cold,'r')
errorbar(u_pos(1:100:end),gamma8_control(1:100:end),gamma8_control_std(1:100:end),'x')
errorbar(u_pos(1:100:end),gamma8_drought(1:100:end),gamma8_drought_std(1:100:end),'gx')
errorbar(u_pos(1:100:end),gamma8_cold(1:100:end),gamma8_cold_std(1:100:end),'rx')
%legend1=legend('Control','Drought','Cold')
title('gamma_{8}')
xlabel('Dist (mm)')



