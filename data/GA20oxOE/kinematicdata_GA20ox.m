% Uses data on cell lengths and velocities to calculate RER and cell
% production rate. Adjusts values to assume that cell growth ceases at the
% position where the RER first reaches zero (see equation 2.38 in the Supplementary text)

% First load data file made in calculatingvelocity_GA20oxdata.m
clear
load kinematicdata_GA20ox.mat  
dx=50 % [microns]


position=10^(-3)*position; % (covert from microns to mm)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
% Wild type
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Calculate RER=dv/dx :
for j=2:N-1;
    RER_wt(j)=(velocity_wt(j+1)-velocity_wt(j-1))/(2*dx);
end
RER_wt(1)=1/(2*dx)*(-3*velocity_wt(1)+4*velocity_wt(2)-velocity_wt(3));
RER_wt(N)=1/(2*dx)*(3*velocity_wt(N)-4*velocity_wt(N-1)+velocity_wt(N-2));

% Calculate cell flux, F=v/l
F_wt=velocity_wt./cell_length_wt; % [1/time]

% Calculate cell production rate P=dF/dx
for j=2:N-1;
    P_wt(j)=(F_wt(j+1)-F_wt(j-1))/(2*dx);
end
P_wt(1)=1/(2*dx)*(-3*F_wt(1)+4*F_wt(2)-F_wt(3));
P_wt(N)=1/(2*dx)*(3*F_wt(N)-4*F_wt(N-1)+F_wt(N-2));

d_wt=P_wt.*cell_length_wt';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
% GA20ox
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Calculate RER=dv/dx :
for j=2:N-1;
    RER_GA20ox(j)=(velocity_GA20ox(j+1)-velocity_GA20ox(j-1))/(2*dx);
end
RER_GA20ox(1)=1/(2*dx)*(-3*velocity_GA20ox(1)+4*velocity_GA20ox(2)-velocity_GA20ox(3));
RER_GA20ox(N)=1/(2*dx)*(3*velocity_GA20ox(N)-4*velocity_GA20ox(N-1)+velocity_GA20ox(N-2));

% Calculate cell flux, F=v/l
F_GA20ox=velocity_GA20ox./cell_length_GA20ox; % [1/time]

% Calculate cell production rate P=dF/dx
for j=2:N-1;
    P_GA20ox(j)=(F_GA20ox(j+1)-F_GA20ox(j-1))/(2*dx);
end
P_GA20ox(1)=1/(2*dx)*(-3*F_GA20ox(1)+4*F_GA20ox(2)-F_GA20ox(3));
P_GA20ox(N)=1/(2*dx)*(3*F_GA20ox(N)-4*F_GA20ox(N-1)+F_GA20ox(N-2));

d_GA20ox=P_GA20ox.*cell_length_GA20ox';


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
% Plot cell lengths, velocities, RER, F, and P:
figure
subplot(3,3,1)
hold on
plot(position, cell_length_wt)
plot(position, cell_length_GA20ox,'r--')
hold on
xlabel('Position, x, (mm)','Interpreter','tex','FontSize',16)
ylabel('l(x), ($\mu$m)','Interpreter','latex','FontSize',16)
title('Measured cell length, l(x)','Interpreter','latex','FontSize',16)
box('off')
set(gca,'FontSize',14)

subplot(3,3,2)
hold on
plot(position, velocity_wt)
plot(position, velocity_GA20ox,'r--')
xlabel('Position, x, (mm)','Interpreter','latex','FontSize',16)
ylabel('v(x), ($\mu$/h)','Interpreter','latex','FontSize',16)
title('Measured velocity, v(x)','Interpreter','latex','FontSize',16)
box('off')
set(gca,'FontSize',14)

subplot(3,3,3)
hold on
plot(position, RER_wt)
plot(position, RER_GA20ox,'r--')
xlabel('Position, x, (mm)','Interpreter','latex','FontSize',16)
ylabel('RER(x), (1/h)','Interpreter','latex','FontSize',16)
title('Relative Elongation Rate, RER=dv/dx','Interpreter','latex','FontSize',16)
box('off')
set(gca,'FontSize',14)

subplot(3,3,4)
hold on
plot(position, F_wt)
plot(position, F_GA20ox,'r--')
xlabel('Position, x, (mm)','Interpreter','latex','FontSize',16)
ylabel('F(x), (1/h)','Interpreter','latex','FontSize',16)
title('Cell flux, F=v/l','Interpreter','latex','FontSize',16)
box('off')
set(gca,'FontSize',14)

subplot(3,3,5)
hold on
plot(position, P_wt)
plot(position, P_GA20ox,'r--')
xlabel('Position, x, (mm)','Interpreter','latex','FontSize',16)
ylabel('P(x), (cells/($\mu$h)','Interpreter','latex','FontSize',16)
title('Production rate, P=dF/dx','Interpreter','latex','FontSize',16)
box('off')
set(gca,'FontSize',14)

subplot(3,3,6)
hold on
plot(position, d_wt)
plot(position, d_GA20ox,'r--')
xlabel('Position, x, (mm)','Interpreter','latex','FontSize',16)
ylabel('d(x), (cells/(cell h)','Interpreter','latex','FontSize',16)
title('Division rate, d=Pl,','Interpreter','latex','FontSize',16)
box('off')
set(gca,'FontSize',14)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Adjusting data

for i=1:N
    if RER_wt(i)<10^(-6)
        i
        position(i)
        RER_wt(i:end)=0;
        cell_length_wt(i)
        velocity_wt(i:end)=velocity_wt(i);
        cell_length_wt(i:end)=cell_length_wt(i);
        break
    end
end
for i=1:N
    if RER_GA20ox(i)<10^(-6)
        RER_GA20ox(i:end)=0;
        velocity_GA20ox(i:end)=velocity_GA20ox(i);
        cell_length_GA20ox(i:end)=cell_length_GA20ox(i);
    end
end

% Remove negative production rates at the end of the meristem
for j=1:N;
    if P_wt(j)<10^(-6)
        P_wt(j)=0;
        d_wt(j)=0;
    end
    if P_GA20ox(j)<10^(-6)
        P_GA20ox(j)=0;
        d_GA20ox(j)=0;
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Extend x domain and set RER=0 for x in mature zone.

position_ext=[position',[position(end)+0.05:0.05:position(end)+30]];
RER_wt=[RER_wt,zeros(1,600)];
RER_GA20ox=[RER_GA20ox,zeros(1,600)];
d_wt=[d_wt,zeros(1,600)];
d_GA20ox=[d_GA20ox,zeros(1,600)];
cell_length_wt=[cell_length_wt',cell_length_wt(end)*ones(1,600)];
cell_length_GA20ox=[cell_length_GA20ox',cell_length_GA20ox(end)*ones(1,600)];
velocity_wt=[velocity_wt',velocity_wt(end)*ones(1,600)];
velocity_GA20ox=[velocity_GA20ox',velocity_GA20ox(end)*ones(1,600)];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot adjusted data cell lengths, velocities, RER, F, and P:
%figure
subplot(3,3,7)
hold on
plot(position_ext, cell_length_wt)
plot(position_ext, cell_length_GA20ox,'r--')
hold on
xlabel('Position, x, (mm)','Interpreter','latex','FontSize',16)
ylabel('l(x), ($\mu$ m)','Interpreter','latex','FontSize',16)
title('Adjusted cell length','Interpreter','latex','FontSize',16)
box('off')
set(gca,'FontSize',14)

subplot(3,3,8)
hold on
plot(position_ext, velocity_wt)
plot(position_ext, velocity_GA20ox,'r--')
xlabel('Position, x, (mm)','Interpreter','latex','FontSize',16)
ylabel('v(x), ($\mu$ m/h)','Interpreter','latex','FontSize',16)
title('Adjusted velocity','Interpreter','latex','FontSize',16)
box('off')
set(gca,'FontSize',14)

subplot(3,3,9)
hold on
plot(position_ext, RER_wt)
plot(position_ext, RER_GA20ox,'r--')
xlabel('Position, x, (mm)','Interpreter','latex','FontSize',16)
ylabel('RER(x), (1/h)','Interpreter','latex','FontSize',16)
title('Adjusted Relative Elongation Rate','Interpreter','latex','FontSize',16)
box('off')
set(gca,'FontSize',14)



position=position_ext;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
% Calculate boundaries of growth zones
%
merisize_wt=50*(length(P_wt(P_wt>10^(-6)))-1)  
mericelllength_wt=cell_length_wt(length(P_wt(P_wt>10^(-6)))-1)
EZsize_wt=50*(length(RER_wt(RER_wt>10^(-6)))-1)

merisize_GA20ox=50*(length(P_GA20ox(P_GA20ox>10^(-6)))-1) 
mericelllength_GA20ox=cell_length_GA20ox(length(P_GA20ox(P_GA20ox>10^(-6)))-1)
EZsize_GA20ox=50*(length(RER_GA20ox(RER_GA20ox>10^(-6)))-1)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% save

%save kinematicdata_GA20ox_wRER.mat
