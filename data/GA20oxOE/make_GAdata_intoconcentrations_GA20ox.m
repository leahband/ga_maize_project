% Forms 'data' from different mat files, adjusting GA data into
% concentrations.


function make_GAdata_intoconcentrations_GA20ox

load data_metabolitesandenzymes_GA20oxOE.mat

load kinematicdata_GA20ox_wRER.mat velocity_wt velocity_GA20ox RER_wt RER_GA20ox cell_length_wt cell_length_GA20ox merisize_wt merisize_GA20ox mericelllength_wt mericelllength_GA20ox position
data.RER=RER_wt;
data.cell_length=cell_length_wt;
data.vel=velocity_wt;
data.velocity=velocity_wt;
data.xm=merisize_wt;
data.lmf=mericelllength_wt;
data.position=10^3*position;

data_GA20ox.RER=RER_GA20ox;
data_GA20ox.cell_length=cell_length_GA20ox;
data_GA20ox.vel=velocity_GA20ox;
data_GA20ox.velocity=velocity_GA20ox;
data_GA20ox.xm=merisize_GA20ox;
data_GA20ox.lmf=mericelllength_GA20ox;
data_GA20ox.position=10^3*position;

data.pos_GAs=10^3*data.pos_GAs';
data.pos_enz=10^3*data.pos_enz;
data_GA20ox.pos_GAs=10^3*data_GA20ox.pos_GAs';
data_GA20ox.pos_enz=10^3*data_GA20ox.pos_enz;

figure

subplot(2,4,1)
hold on
errorbar(0.001*data.pos_GAs,data.GA53,data.GA53_se,'bs-','LineWidth',2)
errorbar(0.001*data_GA20ox.pos_GAs,data_GA20ox.GA53,data_GA20ox.GA53_se,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
title('GA53','Interpreter','latex','FontSize',18)
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA53 (ng/gDW)','Interpreter','latex','FontSize',16)
box off

subplot(2,4,2)
hold on
errorbar(0.001*data.pos_GAs,data.GA44,data.GA44_se,'bs-','LineWidth',2)
errorbar(0.001*data_GA20ox.pos_GAs,data_GA20ox.GA44,data_GA20ox.GA44_se,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
title('GA44','Interpreter','latex','FontSize',18)
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA44 (ng/gDW)','Interpreter','latex','FontSize',16)
box off

subplot(2,4,3)
hold on
errorbar(0.001*data.pos_GAs,data.GA19,data.GA19_se,'bs-','LineWidth',2)
errorbar(0.001*data_GA20ox.pos_GAs,data_GA20ox.GA19,data_GA20ox.GA19_se,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA19 (ng/gDW)','Interpreter','latex','FontSize',16)
title('GA19','Interpreter','latex','FontSize',18)
box off

subplot(2,4,4)
hold on
errorbar(0.001*data.pos_GAs,data.GA20,data.GA20_se,'bs-','LineWidth',2)
errorbar(0.001*data_GA20ox.pos_GAs,data_GA20ox.GA20,data_GA20ox.GA20_se,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA20 (ng/gDW)','Interpreter','latex','FontSize',16)
title('GA20','Interpreter','latex','FontSize',18)
box off

subplot(2,4,5)
hold on
errorbar(0.001*data.pos_GAs,data.GA1,data.GA1_se,'bs-','LineWidth',2)
errorbar(0.001*data_GA20ox.pos_GAs,data_GA20ox.GA1,data_GA20ox.GA1_se,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA1 (ng/gDW)','Interpreter','latex','FontSize',16)
title('GA1','Interpreter','latex','FontSize',18)
box off

subplot(2,4,6)
hold on
errorbar(0.001*data.pos_GAs,data.GA29,data.GA29_se,'bs-','LineWidth',2)
errorbar(0.001*data_GA20ox.pos_GAs,data_GA20ox.GA29,data_GA20ox.GA29_se,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA29 (ng/gDW),','Interpreter','latex','FontSize',16)
title('GA29','Interpreter','latex','FontSize',18)
box off

subplot(2,4,7)
hold on
errorbar(0.001*data.pos_GAs,data.GA8,data.GA8_se,'bs-','LineWidth',2)
errorbar(0.001*data_GA20ox.pos_GAs,data_GA20ox.GA8,data_GA20ox.GA8_se,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA8 (ng/gDW)','Interpreter','latex','FontSize',16)

title('GA8','Interpreter','latex','FontSize',18)
box off
legend1=legend('Wild type','AtGA20ox OE')
set(legend1,'Interpreter','latex','Location','SouthEast','FontSize',24)

dryweight=[0.019;0.019;0.01956;0.01876;0.01868;0.019;0.01968;0.02124;0.0223;0.02314;0.02568;0.025];
dryweight_pos=[2500;7500;12500;17500;25000;35000;45000;55000;65000;75000;85000;95000];
data.dryweight=interp1(dryweight_pos,dryweight,data.pos_GAs,'linear');

data.GA53_molweight=346.42;
data.GA20_molweight=331.39;
data.GA19_molweight=360.41;
data.GA44_molweight=362.42; %(Open Lactone form)
data.GA8_molweight=363.39;
data.GA1_molweight=347.39;
data.GA29_molweight=347.39;

load ./areas_GA20oxOE.mat data_areas
data.Ac=data_areas.B104wt;
data.Ac_se=data_areas.B104wt_se;
data.pos_Ac=data_areas.pos_Ac;

data_GA20ox.Ac=data_areas.GA20oxOE;
data_GA20ox.Ac_se=data_areas.GA20oxOE_se;
data_GA20ox.pos_Ac=data_areas.pos_Ac;


% Calculate leaf area at positions of GA measurements:
data.A_smooth=interp1(data.pos_Ac,data.Ac,data.pos_GAs,'pchip','extrap');
data_GA20ox.A_smooth=interp1(data_GA20ox.pos_Ac,data_GA20ox.Ac,data_GA20ox.pos_GAs,'pchip','extrap');


% Leaf area given in mm^2
% GA measurements * Dry Weight/(GA molecular weight* Leaf area) gives value in
% 10^(-7) M, multiply by 100 to giuve a value in nM.


data.GA53=10^2./(data.A_smooth*data.GA53_molweight).*data.GA53.*data.dryweight;
data.GA44=10^2./(data.A_smooth*data.GA44_molweight).*data.GA44.*data.dryweight;
data.GA19=10^2./(data.A_smooth*data.GA19_molweight).*data.GA19.*data.dryweight;
data.GA20=10^2./(data.A_smooth*data.GA20_molweight).*data.GA20.*data.dryweight;
data.GA8=10^2./(data.A_smooth*data.GA8_molweight).*data.GA8.*data.dryweight;
data.GA1=10^2./(data.A_smooth*data.GA1_molweight).*data.GA1.*data.dryweight;
data.GA29=10^2./(data.A_smooth*data.GA29_molweight).*data.GA29.*data.dryweight;

data.GA53_se=10^2./(data.A_smooth.*data.GA53_molweight).*data.GA53_se.*data.dryweight;
data.GA44_se=10^2./(data.A_smooth.*data.GA44_molweight).*data.GA44_se.*data.dryweight;
data.GA19_se=10^2./(data.A_smooth.*data.GA19_molweight).*data.GA19_se.*data.dryweight;
data.GA20_se=10^2./(data.A_smooth.*data.GA20_molweight).*data.GA20_se.*data.dryweight;
data.GA8_se=10^2./(data.A_smooth.*data.GA8_molweight).*data.GA8_se.*data.dryweight;
data.GA1_se=10^2./(data.A_smooth.*data.GA1_molweight).*data.GA1_se.*data.dryweight;
data.GA29_se=10^2./(data.A_smooth.*data.GA29_molweight).*data.GA29_se.*data.dryweight;

data.GA53_var=5*(data.GA53_se).^2;
data.GA44_var=5*(data.GA44_se).^2;
data.GA19_var=5*(data.GA19_se).^2;
data.GA20_var=5*(data.GA20_se).^2;
data.GA8_var=5*(data.GA8_se).^2;
data.GA1_var=5*(data.GA1_se).^2;
data.GA29_var=5*(data.GA29_se).^2;


data_GA20ox.GA53=10^2./(data_GA20ox.A_smooth*data.GA53_molweight).*data_GA20ox.GA53.*data.dryweight;
data_GA20ox.GA44=10^2./(data_GA20ox.A_smooth*data.GA44_molweight).*data_GA20ox.GA44.*data.dryweight;
data_GA20ox.GA19=10^2./(data_GA20ox.A_smooth*data.GA19_molweight).*data_GA20ox.GA19.*data.dryweight;
data_GA20ox.GA20=10^2./(data_GA20ox.A_smooth*data.GA20_molweight).*data_GA20ox.GA20.*data.dryweight;
data_GA20ox.GA8=10^2./(data_GA20ox.A_smooth*data.GA8_molweight).*data_GA20ox.GA8.*data.dryweight;
data_GA20ox.GA1=10^2./(data_GA20ox.A_smooth*data.GA1_molweight).*data_GA20ox.GA1.*data.dryweight;
data_GA20ox.GA29=10^2./(data_GA20ox.A_smooth*data.GA29_molweight).*data_GA20ox.GA29.*data.dryweight;


data_GA20ox.GA53_se=10^2./(data_GA20ox.A_smooth.*data.GA53_molweight).*data_GA20ox.GA53_se.*data.dryweight;
data_GA20ox.GA44_se=10^2./(data_GA20ox.A_smooth.*data.GA44_molweight).*data_GA20ox.GA44_se.*data.dryweight;
data_GA20ox.GA19_se=10^2./(data_GA20ox.A_smooth.*data.GA19_molweight).*data_GA20ox.GA19_se.*data.dryweight;
data_GA20ox.GA20_se=10^2./(data_GA20ox.A_smooth.*data.GA20_molweight).*data_GA20ox.GA20_se.*data.dryweight;
data_GA20ox.GA8_se=10^2./(data_GA20ox.A_smooth.*data.GA8_molweight).*data_GA20ox.GA8_se.*data.dryweight;
data_GA20ox.GA1_se=10^2./(data_GA20ox.A_smooth.*data.GA1_molweight).*data_GA20ox.GA1_se.*data.dryweight;
data_GA20ox.GA29_se=10^2./(data_GA20ox.A_smooth.*data.GA29_molweight).*data_GA20ox.GA29_se.*data.dryweight;

data_GA20ox.GA53_var=5*(data_GA20ox.GA53_se).^2;
data_GA20ox.GA44_var=5*(data_GA20ox.GA44_se).^2;
data_GA20ox.GA19_var=5*(data_GA20ox.GA19_se).^2;
data_GA20ox.GA20_var=5*(data_GA20ox.GA20_se).^2;
data_GA20ox.GA8_var=5*(data_GA20ox.GA8_se).^2;
data_GA20ox.GA1_var=5*(data_GA20ox.GA1_se).^2;
data_GA20ox.GA29_var=5*(data_GA20ox.GA29_se).^2;



%save data_GA20oxOE_GAconc_nM_GA20oxAdata.mat data data_GA20ox


figure

subplot(2,4,1)
hold on
plot(0.001*data.pos_GAs,data.GA53,'bs-','LineWidth',2)
plot(0.001*data_GA20ox.pos_GAs,data_GA20ox.GA53,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
title('GA53','Interpreter','latex','FontSize',18)
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA53 (nM)','Interpreter','latex','FontSize',16)
box off

subplot(2,4,2)
hold on
plot(0.001*data.pos_GAs,data.GA44,'bs-','LineWidth',2)
plot(0.001*data_GA20ox.pos_GAs,data_GA20ox.GA44,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
title('GA44','Interpreter','latex','FontSize',18)
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA44 (nM)','Interpreter','latex','FontSize',16)
box off

subplot(2,4,3)
hold on
plot(0.001*data.pos_GAs,data.GA19,'bs-','LineWidth',2)
plot(0.001*data_GA20ox.pos_GAs,data_GA20ox.GA19,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA19 (nM)','Interpreter','latex','FontSize',16)
title('GA19','Interpreter','latex','FontSize',18)

box off

subplot(2,4,4)
hold on
plot(0.001*data.pos_GAs,data.GA20,'bs-','LineWidth',2)
plot(0.001*data_GA20ox.pos_GAs,data_GA20ox.GA20,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA20 (nM)','Interpreter','latex','FontSize',16)
title('GA20','Interpreter','latex','FontSize',18)

box off

subplot(2,4,5)
hold on
plot(0.001*data.pos_GAs,data.GA1,'bs-','LineWidth',2)
plot(0.001*data_GA20ox.pos_GAs,data_GA20ox.GA1,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA1 (nM)','Interpreter','latex','FontSize',16)
title('GA1','Interpreter','latex','FontSize',18)

box off

subplot(2,4,6)
hold on
plot(0.001*data.pos_GAs,data.GA29,'bs-','LineWidth',2)
plot(0.001*data_GA20ox.pos_GAs,data_GA20ox.GA29,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA29 (nM),','Interpreter','latex','FontSize',16)
title('GA29','Interpreter','latex','FontSize',18)

box off

subplot(2,4,7)
hold on
plot(0.001*data.pos_GAs,data.GA8,'bs-','LineWidth',2)
plot(0.001*data_GA20ox.pos_GAs,data_GA20ox.GA8,'g^-','MarkerFaceColor',[0.49 1 0.63],'LineWidth',2)
xlabel('Distance (mm)','Interpreter','latex','FontSize',16)
ylabel('GA8 (nM)','Interpreter','latex','FontSize',16)
legend('Wild type','AtGA20ox over-expression')
title('GA8','Interpreter','latex','FontSize',18)

box off
legend1=legend('Wild type','AtGA20ox OE')
set(legend1,'Interpreter','latex','Location','SouthEast','FontSize',24)

end

