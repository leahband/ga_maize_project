% Data set provided distributions of cell lengths, 
% Here, we use these data to calculate the cell velocities.

clear
load  data_celllengths_GA20ox.mat

position=x; % In microns

N= size(cell_length_GA20ox,1);


% Suppl Table of Nelissen et al gives the meristem lengths to be G20ox=26.5, and
% wt=19.1; these values are based on DAPI staining and so gives the position where cell division ceases, rather than being based on the kinematics.
% We note that these values lead to a region where cell production is negative,
% as cell length increases faster than velocity, so in the kinematic
% analysis below, we take the meristem size to be 
% equal to the position where the cell production rate reaches zero. 

merisize_wt=11600; % In microns
merisize_GA20ox=18700; % In microns
prod_wt=17.1; % Values from Supplementary Table of Nelissen et al. 
prod_GA20ox=23.9;


for i=1:N
    if position(i)==merisize_wt;
        pos_meri_wt=i
    end
    if position(i)==merisize_GA20ox;
        pos_meri_GA20ox=i
    end
end

velocity_wt=zeros(N,1);
velocity_GA20ox=zeros(N,1);

velocity_wt(pos_meri_wt:end)=prod_wt*cell_length_wt(pos_meri_wt:end);
velocity_GA20ox(pos_meri_GA20ox:end)=prod_GA20ox*cell_length_GA20ox(pos_meri_GA20ox:end);

gradient_wt=velocity_wt(pos_meri_wt)./position(pos_meri_wt);
gradient_GA20ox=velocity_GA20ox(pos_meri_GA20ox)./position(pos_meri_GA20ox);

velocity_wt(1:pos_meri_wt)=gradient_wt*position(1:pos_meri_wt);
velocity_GA20ox(1:pos_meri_GA20ox)=gradient_GA20ox*position(1:pos_meri_GA20ox);

figure
plot(position, velocity_wt)
hold on 
plot(position, velocity_GA20ox,'r')

%save kinematicdata_GA20ox.mat
